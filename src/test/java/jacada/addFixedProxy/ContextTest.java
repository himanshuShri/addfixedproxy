package jacada.addFixedProxy;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import jacada.addFixedProxy.controller.ADTController;
import jacada.addFixedProxy.controller.MockController;

/**
 * Tests that the context has started.
 * @see https://spring.io/guides/gs/testing-web/
 * @author ndoan
 **/
@RunWith(SpringRunner.class)
@SpringBootTest()
public class ContextTest {

  @Autowired private ADTController adt;
  @Autowired private MockController mock;

  @Test
  public void controllersAreNotNull() throws Exception {
      assertThat(adt).isNotNull();
      assertThat(mock).isNotNull();
  }
}
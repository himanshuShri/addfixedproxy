package jacada.addFixedProxy;

import javax.validation.constraints.NotEmpty;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import lombok.Data;

@ConfigurationProperties(prefix = "proxy.service.test")
@Validated
@Data
public class TestProperties {

	@NotEmpty private String endpointPrefix;

	@NotEmpty private String id;
	@NotEmpty private String customerId;
	@NotEmpty private String sessionId;

	@NotEmpty private String tabId;
	@NotEmpty private String importantAction;
	@NotEmpty private String dataType;
	@NotEmpty private String contactId;
	@NotEmpty private String privilege;

	@NotEmpty private String postCode;
	@NotEmpty private String houseNumber;
	@NotEmpty private String houseLetter;
	@NotEmpty private String houseNumberExtn;
	@NotEmpty private String room;
}
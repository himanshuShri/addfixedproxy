package jacada.addFixedProxy.integration.mock;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import jacada.addFixedProxy.TestProperties;
import jacada.addFixedProxy.dto.adt.ADTUniversalDTO;

/**
 * Calls the service and checks various properties of the response.
 * @see https://spring.io/guides/gs/testing-web/
 * @author ndoan
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@EnableConfigurationProperties({ TestProperties.class })
@ActiveProfiles("test")
public class ADTTest {

	@Autowired private TestProperties properties;
  @Autowired private MockMvc mockMvc;
	@Autowired private ObjectMapper objMapper;

  private String emptyContent;
  private String epPrefix;

  @Before
  public void init() {
  	emptyContent= "{}";
  	epPrefix = properties.getEndpointPrefix();
  }

  @Test
  public void closeTab() throws Exception {

  	String operation = "closeTab";
  	String uri = String.format("/%s/%s?JSESSIONID=%s",
  			properties.getCustomerId(), properties.getTabId(), properties.getSessionId());
  	String errField = String.format("no %s errors for %s-%s-%s",
  			operation, properties.getCustomerId(), properties.getTabId(),
  			properties.getSessionId());

		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(
				String.format("%s/%s/%s", epPrefix, operation, uri)
		).
		contentType(MediaType.APPLICATION_JSON_UTF8).
		content(emptyContent);

    mockMvc.perform(
    		request
  	).
		andDo(
				print()
  	).
  	andExpect(
  			status().isOk()
  	).
  	andExpect(
  			jsonPath("$.error").value(errField)
  	).
  	andExpect(
  			jsonPath("$.status").value(true)
  	);
  }

  @Test
  public void showTab() throws Exception {

  	String operation = "showTab";
  	String uri = String.format("/%s/%s?JSESSIONID=%s",
  			properties.getCustomerId(), properties.getTabId(), properties.getSessionId());
  	String errField = String.format("no %s errors for %s-%s-%s",
  			operation, properties.getCustomerId(), properties.getTabId(),
  			properties.getSessionId());

		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(
				String.format("%s/%s/%s", epPrefix, operation, uri)
		).
		contentType(MediaType.APPLICATION_JSON_UTF8).
		content(emptyContent);

    mockMvc.perform(
    		request
  	).
		andDo(
				print()
  	).
  	andExpect(
  			status().isOk()
  	).
  	andExpect(
  			jsonPath("$.error").value(errField)
  	).
  	andExpect(
  			jsonPath("$.status").value(true)
  	);
  }

  @Test
  public void addImportantAction() throws Exception {

  	String operation = "addImportantAction";
  	String uri = String.format("/%s?JSESSIONID=%s",
  			properties.getCustomerId(), properties.getSessionId());
  	String errField = String.format("no %s errors for %s-%s-%s",
  			operation, properties.getImportantAction(), properties.getCustomerId(),
  			properties.getSessionId());

  	ADTUniversalDTO dto = new ADTUniversalDTO();
  	dto.setImportantAction(properties.getImportantAction());

		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(
				String.format("%s/%s/%s", epPrefix, operation, uri)
		).
		contentType(MediaType.APPLICATION_JSON_UTF8).
		content(objMapper.writeValueAsBytes(dto));

    mockMvc.perform(
    		request
  	).
		andDo(
				print()
  	).
  	andExpect(
  			status().isOk()
  	).
  	andExpect(
  			jsonPath("$.error").value(errField)
  	).
  	andExpect(
  			jsonPath("$.status").value(true)
  	);
  }

  @Test
  public void dataUpdated() throws Exception {

  	String operation = "dataUpdated";
  	String uri = String.format("/%s/%s/%s?JSESSIONID=%s",
  			properties.getCustomerId(), properties.getDataType(),
  			properties.getId(), properties.getSessionId());
  	String errField = String.format("no %s errors for %s-%s-%s-%s",
  			operation, properties.getCustomerId(), properties.getDataType(),
  			properties.getId(), properties.getSessionId());

		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(
				String.format("%s/%s/%s", epPrefix, operation, uri)
		).
		contentType(MediaType.APPLICATION_JSON_UTF8).
		content(emptyContent);

    mockMvc.perform(
    		request
  	).
		andDo(
				print()
  	).
  	andExpect(
  			status().isOk()
  	).
  	andExpect(
  			jsonPath("$.error").value(errField)
  	).
  	andExpect(
  			jsonPath("$.status").value(true)
  	);
  }

  @Test
  public void isPrivileged() throws Exception {

  	String operation = "isPrivileged";
  	String uri = String.format("/%s?JSESSIONID=%s",
  			properties.getPrivilege(), properties.getSessionId());
  	String errField = String.format("no %s errors for %s-%s",
  			operation, properties.getPrivilege(), properties.getSessionId());

  	MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get(
				String.format("%s/%s/%s", epPrefix, operation, uri)
		).
		contentType(MediaType.APPLICATION_JSON_UTF8);

    mockMvc.perform(
    		request
  	).
		andDo(
				print()
  	).
  	andExpect(
  			status().isOk()
  	).
  	andExpect(
  			jsonPath("$.error").value(errField)
  	).
  	andExpect(
  			jsonPath("$.status").value(true)
  	);
  }
}
package jacada.addFixedProxy.integration.mock;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import jacada.addFixedProxy.TestProperties;

/**
 * Calls the service and checks various properties of the response.
 * @see https://spring.io/guides/gs/testing-web/
 * @author ndoan
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@EnableConfigurationProperties({ TestProperties.class })
@ActiveProfiles("test")
@SuppressWarnings("unused")
public class AmdocsTest {

  @Autowired private TestProperties properties;

  @Autowired private MockMvc mockMvc;
  @Autowired private ObjectMapper objMapper;
  private String epPrefix;

  @Before
  public void init() {
  	epPrefix = properties.getEndpointPrefix();
  }
}
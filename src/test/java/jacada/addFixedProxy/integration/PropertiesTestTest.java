package jacada.addFixedProxy.integration;

import static org.hamcrest.CoreMatchers.startsWith;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

/**
 * Calls the service and checks various properties of the response.
 * @see https://spring.io/guides/gs/testing-web/
 * @author ndoan
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PropertiesTestTest {

  @Autowired private MockMvc mockMvc;

  @Test
  public void strMethod() throws Exception {

    mockMvc.perform(
    		get("/propertiesTest/str/1")
  	).
		andDo(
				print()
  	).
  	andExpect(
  			status().isOk()
  	).
  	andExpect(
  			content().string(startsWith("here is your id: 1"))
  	);
  }

  @Test
  public void webMethod() throws Exception {

    mockMvc.perform(
    		get("/propertiesTest/web/1")).
  	andDo(
  			print()
  	).
  	andExpect(
  			status().isOk()
  	).
  	andExpect(
  			content().string(startsWith("1 - "))
  	);
  }

  @Test
  public void dataMethod() throws Exception {

    mockMvc.perform(
    		get("/propertiesTest/data")
  	).
  	andDo(
  			print()
  	).
  	andExpect(
  			status().isOk()
  	).
  	andExpect(
  			jsonPath("$").value(Matchers.hasSize(4))
  	).
  	andExpect(
  			jsonPath("$[0].name").value("fred")
  	).
  	andExpect(
  			jsonPath("$[3].name").value("scooby")
  	);
  }
}
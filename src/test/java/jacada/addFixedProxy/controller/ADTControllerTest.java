package jacada.addFixedProxy.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import jacada.addFixedProxy.TestProperties;
import jacada.addFixedProxy.dto.proxy.adt.ProxyADTRequestDTO;
import jacada.addFixedProxy.dto.proxy.layer7.NetworkPathDetailsDTO;
import jacada.addFixedProxy.service.adt.ADTService;
import jacada.addFixedProxy.service.layer7.rest.Layer7Service;
import jacada.addFixedProxy.service.vanDyck.VanDyckService;

/**
 * Tests that the controller paths are as expected.
 * @see https://spring.io/guides/gs/testing-web/
 * @author ndoan
 **/
@RunWith(SpringRunner.class)
@WebMvcTest(ADTController.class)
@AutoConfigureMockMvc
@EnableConfigurationProperties({ TestProperties.class })
@ActiveProfiles("test")
public class ADTControllerTest {

	@Autowired private TestProperties properties;
	@Autowired private MockMvc mockMvc;
  @MockBean private Layer7Service amdocs;
  @MockBean private ADTService adt;
  @MockBean private VanDyckService vanDyck;
	@Autowired private ObjectMapper objMapper;

  @Test
  public void networkPathDetails() throws Exception {

  	NetworkPathDetailsDTO dto = new NetworkPathDetailsDTO();
  	dto.setPostCode(properties.getPostCode());
  	dto.setHouseNumber(properties.getHouseNumber());
  	dto.setHouseLetter(properties.getHouseLetter());
  	dto.setHouseNumberExtension(properties.getHouseNumberExtn());
  	dto.setRoom(properties.getRoom());

  	Map<String, Object> output = new HashMap<>();
  	output.put("postCode", properties.getPostCode());
  	output.put("status", 200);

  	when(amdocs.networkPathDetails(dto)).thenReturn(output);

  	MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/customer/networkPathDetails").
  			contentType(MediaType.APPLICATION_JSON_UTF8).
  			content(objMapper.writeValueAsBytes(dto));

  	this.mockMvc.perform(
  			request
  	).andDo(
  			print()
  	).
		andExpect(
				status().isOk()
		).
  	andExpect(
  			jsonPath("$.['postCode']").value(properties.getPostCode())
  			).
  	andExpect(
  			jsonPath("$.status").value(200)
  	);
  }

  @Test
  public void closeTab() throws Exception {

  	ProxyADTRequestDTO dto = new ProxyADTRequestDTO();
  	dto.setCustomerId(properties.getCustomerId());
  	dto.setTabId(properties.getTabId());
  	dto.setSessionId(properties.getSessionId());
  	String errField = String.format("no closeTab errors for %s-%s-%s",
  			properties.getCustomerId(), properties.getTabId(), properties.getSessionId());

  	Map<String, Object> output = new HashMap<>();
  	output.put("error", errField);
  	output.put("status", true);

  	when(adt.closeTab(dto)).thenReturn(output);

  	MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/closeTab").
  			contentType(MediaType.APPLICATION_JSON_UTF8).
  			content(objMapper.writeValueAsBytes(dto));

  	this.mockMvc.perform(
  			request
  	).andDo(
  			print()
  	).
		andExpect(
				status().isOk()
		).
  	andExpect(
  			jsonPath("$.error").value(errField)
  	).
  	andExpect(
  			jsonPath("$.status").value(true)
  	);
  }
}
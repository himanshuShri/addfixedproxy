package jacada.addFixedProxy.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jacada.addFixedProxy.UtilBeans.ExpiringComplexMap;
import jacada.addFixedProxy.UtilBeans.ExpiringComplexMap.CacheItem;
import jacada.addFixedProxy.dto.proxy.layer7.AddressDTO;
import jacada.addFixedProxy.dto.proxy.layer7.CPEDetailsDTO;
import jacada.addFixedProxy.dto.proxy.layer7.CreateAppointmentDTO;
import jacada.addFixedProxy.dto.proxy.layer7.CreateInteractionDTO;
import jacada.addFixedProxy.dto.proxy.layer7.CreateTicketDTO;
import jacada.addFixedProxy.dto.proxy.layer7.CreateTicketPhase2DTO;
import jacada.addFixedProxy.dto.proxy.layer7.GetRequestDTO;
import jacada.addFixedProxy.dto.proxy.layer7.GetRequestDTO.ID_TYPE;
import jacada.addFixedProxy.dto.proxy.layer7.NetworkPathDetailsDTO;
import jacada.addFixedProxy.dto.proxy.layer7.ReserveTimeslotsDTO;
import jacada.addFixedProxy.dto.proxy.layer7.SOMOrderDTO;
import jacada.addFixedProxy.dto.proxy.layer7.SubmitSwapDTO;
import jacada.addFixedProxy.dto.proxy.layer7.UpdateAccountDetailsDTO;
import jacada.addFixedProxy.dto.proxy.layer7.UpdateAppointmentDTO;
import jacada.addFixedProxy.dto.proxy.layer7.UpdateBillingArrangementDetailsDTO;
import jacada.addFixedProxy.dto.proxy.layer7.UpdateBillingCustomerDetailsDTO;
import jacada.addFixedProxy.dto.proxy.layer7.UpdateContactDTO;
import jacada.addFixedProxy.service.layer7.rest.Layer7Service;
import jacada.addFixedProxy.service.layer7.soap.AddressClient.AddressSearchVO;
import lombok.extern.slf4j.Slf4j;

//CLEAN swagger

@Slf4j
@RestController
@RequestMapping (
	value		= "/layer7",
	method		= RequestMethod.GET,
	consumes	= MediaType.APPLICATION_JSON_VALUE,
	produces	= MediaType.APPLICATION_JSON_VALUE
)
public class Layer7Controller {

	@Autowired private Layer7Service layer7;
	@Autowired private ExpiringComplexMap fifteenMinuteMap;

	// LAYER7 services
	@RequestMapping(
		value = "/address",
		method = RequestMethod.POST
	)
	public AddressSearchVO address(
		@RequestBody AddressDTO dto
	) {
		log.info("address requests are handled by amdocs");
		log.debug("request details: {}", dto);

		return layer7.address(dto);
	}

	@RequestMapping(
		value = "/customer/assignedProducts/{customerId}"
	)
	public Map<String, Object> assignedProducts(
		@PathVariable("customerId") String customerId
	) {
		log.info("assignedProducts requests are handled by amdocs");
		log.debug("request details: {}", customerId);

		GetRequestDTO dto = new GetRequestDTO(customerId, ID_TYPE.CUSTOMER_ID);
		return layer7.assignedProducts(dto);
	}

	@RequestMapping(
		value = "/product/details/product/{assignedProductIds}"
	)
	public Map<String, Object> assignedProductDetails_product(
		@PathVariable("assignedProductIds") String assignedProductIds
	) throws Exception {
		log.info("assignedProductDetails requests are handled by amdocs");
		log.debug("request details: {}", assignedProductIds);

		GetRequestDTO dto = new GetRequestDTO(assignedProductIds, ID_TYPE.ASSIGNED_PRODUCT_ID);
		return layer7.assignedProductDetails(dto);
	}

	@RequestMapping(
		value = "/product/details/customer/{customerId}"
	)
	public Map<String, Object> assignedProductDetails_customer(
		@PathVariable("customerId") String customerId
	) throws Exception {
		log.info("assignedProductDetails requests are handled by amdocs");
		log.debug("request details: {}", customerId);

		GetRequestDTO dto = new GetRequestDTO(customerId, ID_TYPE.CUSTOMER_ID);
		return layer7.assignedProductDetails(dto);
	}

	@RequestMapping(
		value = "/contact/{contactId}",
		method = RequestMethod.GET
	)
	public Map<String, Object> contactDetails(
		@PathVariable("contactId") String contactId
	) {
		log.info("contactDetails requests are handled by amdocs");
		log.debug("request details: {}", contactId);

		GetRequestDTO dto = new GetRequestDTO(contactId, ID_TYPE.CONTACT_ID);
		return layer7.contactDetails(dto);
	}

	@RequestMapping(
		value = "/appointment/{appointmentId}"
	)
	public Map<String, Object> appointment(
		@PathVariable("appointmentId") String appointmentId
	) {
		log.info("appointment lookup requests are handled by tibco");
		log.debug("request details: {}", appointmentId);

		GetRequestDTO dto = new GetRequestDTO(appointmentId, ID_TYPE.APPOINTMENT_ID);
		return layer7.appointment(dto);
	}

	@RequestMapping(
		value = "/appointment/create",
		method = RequestMethod.POST
	)
	public Map<String, Object> createAppointment(
		@RequestBody CreateAppointmentDTO dto
	) {
		log.info("createAppointment requests are handled by tibco");
		log.debug("request details: {}", dto);

		return layer7.createAppointment(dto);
	}

	@RequestMapping(
		value = "/appointment/update",
		method = RequestMethod.POST
	)
	public Map<String, Object> updateAppointment(
		@RequestBody UpdateAppointmentDTO dto
	) {
		log.debug("updateAppointment requests are handled by amdocs");
		log.debug("request details: {}", dto);

		return layer7.updateAppointment(dto);
	}

	@RequestMapping(
		value = "/product/swap/create/cpeSwap",
		method = RequestMethod.POST
	)
	public Map<String, Object> submitSwap(
		@RequestBody SubmitSwapDTO dto
	) {
		log.debug("submitSwap requests are handled by amdocs");
		log.debug("request details: {}", dto);

		return layer7.submitSwap(dto);
	}

	@RequestMapping(
		value = "/customer/interaction/create",
		method = RequestMethod.POST
	)
	public Map<String, Object> createInteraction(
		@RequestBody CreateInteractionDTO dto
	) {
		log.info("createInteraction requests are handled by amdocs");
		log.debug("request details: {}", dto);
		return layer7.createInteraction(dto);
	}

	@RequestMapping(
		value = "/case/create",
		method = RequestMethod.POST
	)
	public Map<String, Object> createCase(
		@RequestBody CreateTicketDTO dto
	) {
		log.info("createCase requests are handled by amdocs");
		log.debug("request details: {}", dto);
		return layer7.createTicket(dto);
	}

	@RequestMapping(
		value = "/ticket/create",
		method = RequestMethod.POST
	)
	public Map<String, Object> createTicket(
		@RequestBody CreateTicketPhase2DTO dto
	) {
		log.info("createTicket requests are handled by amdocs");
		log.debug("request details: {}", dto);
		return layer7.createTicket(dto);
	}

	@RequestMapping(
		value = "/customer/profile/{customerId}"
	)
	public Map<String, Object> customerProfile(
		@PathVariable("customerId") String customerId
	) {
		log.info("customerProfile requests are handled by amdocs");
		log.debug("request details: {}", customerId);

		GetRequestDTO dto = new GetRequestDTO(customerId, ID_TYPE.CUSTOMER_ID);
		return layer7.customerProfile(dto);
	}

	@RequestMapping(
		value = "/customer/filters/{customerId}"
	)
	public Map<String, Object> customerFilters(
		@PathVariable("customerId") String customerId
	) {
		log.info("filterCustomer requests are handled by K2View");
		log.debug("request details: {}", customerId);

		GetRequestDTO dto = new GetRequestDTO(customerId, ID_TYPE.CUSTOMER_ID);
		return layer7.customerFilters(dto);
	}

	@RequestMapping(
		value = "/customer/billingArrangementDetails/{customerId}"
	)
	public Map<String, Object> billingArrangementDetails(
		@PathVariable("customerId") String customerId
	) throws Exception {
		log.info("billingArrangementDetails requests are handled by amdocs");
		log.debug("request details: {}", customerId);

		GetRequestDTO dto = new GetRequestDTO(customerId, ID_TYPE.CUSTOMER_ID);
		return layer7.billingArrangementDetails(dto);
	}

	@RequestMapping(
		value = "/customer/billingEntities/{customerId}"
	)
	public Map<String, Object> billingEntities(
		@PathVariable("customerId") String customerId
	) {
		log.info("billingEntities requests are handled by amdocs");
		log.debug("request details: {}", customerId);

		GetRequestDTO dto = new GetRequestDTO(customerId, ID_TYPE.CUSTOMER_ID);
		return layer7.billingEntities(dto);
	}

	@RequestMapping(
		value = "/network/path",
		method = RequestMethod.POST
	)
	public Map<String, Object> networkPathDetails(
		@RequestBody NetworkPathDetailsDTO dto
	) {
		log.info("networkPathDetails requests are handled by amdocs");
		log.debug("request details: {}", dto);

		return layer7.networkPathDetails(dto);
	}

	@RequestMapping(
		value = "/appointment/timeslot/reserve",
		method = RequestMethod.POST
	)
	public Map<String, Object> reserveTimeslots(
		@RequestBody ReserveTimeslotsDTO dto
	) throws Exception {
		log.info("reserveTimeslots requests are handled by tibco");
		log.debug("request details: {}", dto);

		return layer7.reserveTimeslots(dto);
	}

	@RequestMapping(
		value = "/case/{caseId}"
	)
	public Map<String, Object> caseDetails(
		@PathVariable("caseId") String caseId
	) {
		log.info("ticketDetails requests are handled by amdocs");
		log.debug("request details: {}", caseId);

		GetRequestDTO dto = new GetRequestDTO(caseId, ID_TYPE.CASE_ID);
		return layer7.ticketDetails(dto);
	}

	@RequestMapping(
		value = "/cpe/details",
		method = RequestMethod.POST
	)
	public Map<String, Object> cpeDetails(
		@RequestBody CPEDetailsDTO dto
	) {
		log.info("cpeDetails requests are handled by isw");
		log.debug("request details: {}", dto);

		return layer7.cpeDetails(dto);
	}

	@RequestMapping(
		value = "/customer/account/update",
		method = RequestMethod.POST
	)
	public Map<String, Object> updateAccountDetails(
		@RequestBody UpdateAccountDetailsDTO dto
	) {
		log.info("updateAccountDetails requests are handled by amdocs");
		log.debug("request details: {}", dto);

		return layer7.updateAccountDetails(dto);
	}

	@RequestMapping(
		value = "/customer/billingCustomer/update",
		method = RequestMethod.POST
	)
	public Map<String, Object> updateBillingCustomerDetails(
		@RequestBody UpdateBillingCustomerDetailsDTO dto
	) {
		log.info("updateBillingCustomerDetails requests are handled by amdocs");
		log.debug("request details: {}", dto);

		return layer7.updateBillingCustomerDetails(dto);
	}

	@RequestMapping(
		value = "/customer/billingArrangement/update",
		method = RequestMethod.POST
	)
	public Map<String, Object> updateBillingArrangementDetails(
		@RequestBody UpdateBillingArrangementDetailsDTO dto
	) {
		log.info("updateBillingArrangementDetails requests are handled by amdocs");
		log.debug("request details: {}", dto);

		return layer7.updateBillingArrangementDetails(dto);
	}

	@RequestMapping(
		value = "/contact/update",
		method = RequestMethod.POST
	)
	public Map<String, Object> updateContact(
		@RequestBody UpdateContactDTO dto
	) {
		log.info("updateContact requests are handled by amdocs");
		log.debug("request details: {}", dto);

		return layer7.updateContact(dto);
	}

	@RequestMapping(
		value	= "/som/allLocks"
	)
	public Map<String, CacheItem> allLocks(
	) throws Exception {
		log.info("refreshEntitlements-allLocks requests are handled here");

		return fifteenMinuteMap.map();
	}

	@RequestMapping(
		value	= "/som/isLocked/{serviceGroupId}"
	)
	public Map<String, Object> isLocked(
		@PathVariable("serviceGroupId") String serviceGroupId
	) throws Exception {
		log.info("refreshEntitlements-isLocked requests are handled here");
		log.debug("request details: {}", serviceGroupId);

		CacheItem lock = fifteenMinuteMap.contains(serviceGroupId);
		Map<String, Object> output = new HashMap<String, Object>();

		// only call the service if this request has not been performed recently
		if(lock == null) {
			log.debug("This account is unlocked");
			output.put("locked", false);
		} else {
			log.debug("This account is locked");
			output.put("locked", true);
			output.put("lockedAt", lock.getAddedAt());
		}

		return output;
	}

	@RequestMapping(
		value	= "/som/lock/{serviceGroupId}",
		method	= RequestMethod.POST
	)
	public Map<String, Object> lock(
		@PathVariable("serviceGroupId") String serviceGroupId
	) throws Exception {
		log.info("refreshEntitlements-lock requests are handled here");
		log.debug("request details: {}", serviceGroupId);

		CacheItem lock = fifteenMinuteMap.add(serviceGroupId);
		Map<String, Object> output = new HashMap<String, Object>();

		// only call the service if this request has not been performed recently
		if(lock == null) {
			log.debug("This account is now locked");
			output.put("locked", true);
		} else {
			log.warn("This account was previously locked");
			output.put("locked", true);
			output.put("lockedAt", lock.getAddedAt());
		}

		return output;
	}

	@RequestMapping(
		value	= "/som/unlock/{serviceGroupId}",
		method	= RequestMethod.POST
	)
	public Map<String, Object> unlock(
		@PathVariable("serviceGroupId") String serviceGroupId
	) throws Exception {
		log.info("refreshEntitlements-unlock requests are handled here");
		log.debug("request details: {}", serviceGroupId);

		CacheItem lock = fifteenMinuteMap.remove(serviceGroupId);
		Map<String, Object> output = new HashMap<String, Object>();

		// only call the service if this request has not been performed recently
		if(lock == null) {
			log.warn("This account was already unlocked");
			output.put("locked", false);
		} else {
			log.debug("This account is unlocked");
			output.put("locked", false);
			output.put("lockedAt", lock.getAddedAt());
		}

		return output;
	}

	@RequestMapping(
		value	= "/som/refreshEntitlements",
		method	= RequestMethod.POST
	)
	public Map<String, Object> refreshEntitlements(
			@RequestBody SOMOrderDTO dto
	) throws Exception {
		log.info("refreshEntitlements requests are handled by amdocs");
		log.debug("request details: {}", dto);

		var output = layer7.refreshEntitlements(dto);
		log.info("refresh requested for {}", dto.getServiceGroupId());
		return output;
	}
}
package jacada.addFixedProxy.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;


import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import jacada.addFixedProxy.mongo.MongoInstance.COLLECTIONS;
import jacada.addFixedProxy.service.admin.AdminService;
import jacada.addFixedProxy.view.AdminView;
import lombok.extern.slf4j.Slf4j;

// CLEAN swagger

@Slf4j
@RestController
@RequestMapping (
	value		= "/admin",
	method		= RequestMethod.GET,
	produces	= MediaType.TEXT_HTML_VALUE
)
public class AdminController {

	@Autowired AdminView 	view;
	@Autowired AdminService service;

	@RequestMapping(
		value	= "/"
	)
	public String buildPage (
	) throws Exception {
        log.debug("building admin page");
        return view.buildPage();
    }

	@RequestMapping(
		method		= RequestMethod.POST,
		consumes	= MediaType.APPLICATION_JSON_VALUE,
		value		= "insert/{collection}"
	)
	public void insert(
		@PathVariable("collection") String collection,
		@RequestBody				String json
	) throws Exception {

		log.debug("inserting data [{}], {}",
				collection, json);

		service.insert(collection, json);
    }

	@RequestMapping(
		method		= RequestMethod.POST,
		consumes	= MediaType.APPLICATION_JSON_VALUE,
		value		= "update/{collection}/{id}"
	)
	public void update(
			@PathVariable("collection") String collection,
			@PathVariable("id") 		String id,
			@RequestBody				String json
	) throws Exception {

		log.debug("updating [{}], [{}], {}",
				collection, id, json);

		service.update(collection, id, json);
    }

	@RequestMapping(
		method		= RequestMethod.POST,
		consumes	= MediaType.MULTIPART_FORM_DATA_VALUE,
		value		= "/uploadAll"
	)
	public void uploadFiles(
		@RequestParam(name="ticketType1File",	required=false) MultipartFile ticketType1File,
		@RequestParam(name="ticketType2File",	required=false) MultipartFile ticketType2File,
		@RequestParam(name="ticketType3File",	required=false) MultipartFile ticketType3File,
		@RequestParam(name="flexAttrFile",		required=false) MultipartFile flexAttrFile
	) throws IOException {

		log.debug("uploading data files [{}], [{}], [{}], [{}]",
				ticketType1File, ticketType2File, ticketType3File, flexAttrFile);

		service.upload(COLLECTIONS.TICKET_TYPE_1, 		ticketType1File);
		service.upload(COLLECTIONS.TICKET_TYPE_2, 		ticketType2File);
		service.upload(COLLECTIONS.TICKET_TYPE_3, 		ticketType3File);
		service.upload(COLLECTIONS.FLEXIBLE_ATTRIBUTES,	flexAttrFile);
    }

	@RequestMapping(
		produces	= MediaType.APPLICATION_JSON_VALUE,
		value		= "findForDisplay"
	)
	public Map<String, Object> findForDisplay(
		@RequestParam(name = "ticketType1Display", required = false) String d1,
		@RequestParam(name = "ticketType2Display", required = false) String d2,
		@RequestParam(name = "ticketType3Display", required = false) String d3
	) throws IOException {

		log.debug("inverting display mapping for ticketType1Display {}, ticketType2Display {}, ticketType3Display {}", d1, d2, d3);
        return service.findForDisplay(d1, d2, d3);
    }

	@RequestMapping(
		produces	= MediaType.APPLICATION_JSON_VALUE,
		value		= "read/{collection}"
	)
	public List<Document> read(
		@PathVariable("collection") String collection,
		@RequestParam(name = "ticketType1", required = false) String tt1,
		@RequestParam(name = "ticketType2", required = false) String tt2,
		@RequestParam(name = "ticketType3", required = false) String tt3,
		@RequestParam(name = "parent", 		required = false) String parent
	) throws IOException {

		log.debug("reading existing {} records, ticketType1 {}, ticketType2 {}, ticketType3 {}, parent {}", collection, tt1, tt2, tt3, parent);
        return service.read(collection, tt1, tt2, tt3, parent);
    }


	@RequestMapping(
		method		= RequestMethod.POST,
		produces	= MediaType.APPLICATION_JSON_VALUE,
		value		= "delete/{collection}/{id}"
	)
	public void delete(
			@PathVariable("collection") String collection,
			@PathVariable("id") 		String id
	) throws IOException {

		log.debug("deleting {}.{} record", collection, id);
        service.delete(collection, id);
    }
}
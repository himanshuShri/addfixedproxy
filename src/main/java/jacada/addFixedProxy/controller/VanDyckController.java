package jacada.addFixedProxy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jacada.addFixedProxy.service.vanDyck.VanDyckService;
import lombok.extern.slf4j.Slf4j;

//CLEAN swagger

@Slf4j
@RestController
@RequestMapping (
	value		= "/vanDyck",
	method		= RequestMethod.GET,
	consumes	= MediaType.APPLICATION_JSON_VALUE,
	produces	= MediaType.APPLICATION_JSON_VALUE
)

public class VanDyckController {

	@Autowired private VanDyckService vanDyck;

	// VANDYCK services
	@RequestMapping(
		value = "/caseTypes"
	)
	public String supportedCases(
	) throws Exception {

		log.info("supportedCases requests are handled by vanDyck");
		return vanDyck.supportedCases();
	}

	@RequestMapping(
		value = "/case/createDynamics/{caseType}"
	)
	public String createCaseDynamics(
		@PathVariable("caseType") String caseType,
		@RequestParam("ADTSessionId") String ADTSessionId,
		@RequestParam("sessionId") String sessionId,
		@RequestParam(value="customerId", required=false, defaultValue="") String customerId
	) throws Exception {

		log.info("createCaseDynamics requests are handled by vanDyck");
		log.debug("request details: {} / {} / {} / {}", caseType, customerId, ADTSessionId, sessionId);
		return vanDyck.createCaseDynamics(caseType, customerId, ADTSessionId, sessionId);
	}
}
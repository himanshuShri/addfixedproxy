package jacada.addFixedProxy.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import jacada.addFixedProxy.service.adt.DiagnosticsService;
import jacada.addFixedProxy.view.DiagnosticsView;

// CLEAN swagger

@RestController
@RequestMapping (
	value		= "/diagnostics",
	method		= RequestMethod.GET,
	consumes	= MediaType.ALL_VALUE,			// these requests come in from a context-less browser
	produces	= MediaType.TEXT_HTML_VALUE
)
public class DiagnosticsController {

	@Autowired DiagnosticsService service;
	@Autowired DiagnosticsView view;

	// BUILD services
	@RequestMapping(
		value = "buildInfo"
	)
	public String buildInfo(
	) throws Exception {

		return view.buildInfoTable(service.buildInfo());
	}

	@RequestMapping(
		value = "properties/{type}"
	)
	public String properties(
		@PathVariable("type") String type
	) throws Exception {

		Map<String,Object> map = service.mapProperties(type);
		return view.hashTable(map);
	}

	@RequestMapping(
		value = "endpoints/{type}"
	)
	public String endpoints(
		@PathVariable("type") String type
	) throws Exception {

		Map<String,Object> map = service.endpoints(type);
		return view.hashTable(map);
	}

	@RequestMapping(
		value = "outputPaths/{type}"
	)
	public String outputPaths(
		@PathVariable("type") String type
	) throws Exception {

		Map<String,Object> map = service.outputPaths(type);
		return view.hashTable(map);
	}
}
package jacada.addFixedProxy.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import jacada.addFixedProxy.properties.InteractProperties;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping (
	value		= "/interact",
	method		= RequestMethod.GET,
	consumes	= MediaType.APPLICATION_JSON_VALUE,
	produces	= MediaType.APPLICATION_JSON_VALUE
)

public class InteractController {

	@Autowired private InteractProperties properties;

	/**
	 * createCase_shortUrl : 5UnidyD
	 */
	@RequestMapping(
		value = "createCase"
	)
	public ModelAndView createCase(
		ModelMap model,
		@RequestParam Map<String, String> requestParams
	) {
		log.debug("redirecting to createCase interaction");
		model.addAttribute("interaction", properties.getCreateCaseInteraction());
		model.addAttribute("accountId", properties.getAccountId());
		model.addAttribute("showTabs", properties.getShowTabs());
		model.addAttribute("showSearch", properties.getShowSearch());
		model.addAttribute("appkey", properties.getAppKey());
		model.addAttribute("Environment-Name", properties.getEnvironment());

		model.addAllAttributes(requestParams);
		return new ModelAndView("redirect:" + properties.getCreateCaseUrl(), model);
	}

	@RequestMapping(
		value = "loadShortUrl/{url}"
	)
	public ModelAndView loadUrl(
		@PathVariable("url") String url,
		ModelMap model
	) {

		log.debug("redirecting to [{}]", url);
		return new ModelAndView(String.format("redirect:%s%s", properties.getShortUrlPrefix(), url), model);
	}
}
package jacada.addFixedProxy.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientResponseException;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import jacada.addFixedProxy.dto.adt.ADTResponseDTO;
import jacada.addFixedProxy.dto.adt.ADTUniversalDTO;
import jacada.addFixedProxy.dto.layer7.ISWRequestDTO;
import jacada.addFixedProxy.dto.layer7.Layer7AssignedProductDetailsDTO;
import jacada.addFixedProxy.dto.layer7.Layer7AssignedProductsDTO;
import jacada.addFixedProxy.dto.layer7.Layer7BillingArrangementDetailsDTO;
import jacada.addFixedProxy.dto.layer7.Layer7CreateAppointmentDTO;
import jacada.addFixedProxy.dto.layer7.Layer7CreateInteractionDTO;
import jacada.addFixedProxy.dto.layer7.Layer7CreateTicketDTO;
import jacada.addFixedProxy.dto.layer7.Layer7ReserveTimeslotsDTO;
import jacada.addFixedProxy.dto.layer7.Layer7SubmitSwapDTO;
import jacada.addFixedProxy.dto.layer7.Layer7UpdateAccountDetailsDTO;
import jacada.addFixedProxy.dto.layer7.Layer7UpdateAppointmentDTO;
import jacada.addFixedProxy.dto.layer7.Layer7UpdateBillingArrangementDetailsDTO;
import jacada.addFixedProxy.dto.layer7.Layer7UpdateBillingCustomerDetailsDTO;
import jacada.addFixedProxy.dto.layer7.Layer7UpdateContactDTO;
import jacada.addFixedProxy.service.mock.rest.ADTMockService;
import jacada.addFixedProxy.service.mock.rest.Layer7MockService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping (
	value		= "/mock",
	method		= RequestMethod.GET,
	consumes	= MediaType.APPLICATION_JSON_VALUE,
	produces	= MediaType.APPLICATION_JSON_VALUE
)
public class MockController {

	@Autowired private Layer7MockService amdocs;
	@Autowired private ADTMockService adt;
	@Autowired private ObjectMapper objMapper;

	// JSON trailing comma
	@RequestMapping(
		value = "/json_comma/{mode}"
	)
	public String json_testing(
		@PathVariable("mode") int mode
	) {
		log.info("json_testing method, returning a trailing commma array");

		String data =
				mode == 1? "[1,2,3]"	:	// in tolerant mode, 1 & 2 produce the same output
				mode == 2? "[1,2,3,]"	:
				mode == 3? "[1,2,3,,]"	:	// this is always an error
				mode == 4? "{\"one\":1,\"two\":2,\"three\":3}"	:	// in tolerant mode, 4 & 5 produce the same
				mode == 5? "{\"one\":1,\"two\":2,\"three\":3,}"	:
						   "{\"one\":1,\"two\":2,\"three\":3,,}";	// this is always an error

		log.debug("JSON comma test, with value {}, output is -> {}", mode, data);
		DocumentContext doc = JsonPath.parse("{\"data\":" +data +"}");

		return doc.read("$.data");
	}

	// Layer7 services
	@RequestMapping(
		value		= "order/submit",
		method		= RequestMethod.POST,
		consumes	= MediaType.APPLICATION_XML_VALUE,
		produces	= MediaType.APPLICATION_XML_VALUE
	)
	public ResponseEntity<String> refreshEntitlements(
		@RequestBody String dto
	) throws Exception {

		log.info("refreshEntitlements {}", dto);
		return new ResponseEntity<> (amdocs.refreshEntitlements(dto), HttpStatus.OK);
	}

	@RequestMapping(
		value	= "GetNetworkPathDetails_1",
		method	= RequestMethod.POST
	)
	public ResponseEntity<String> networkPathDetails(
		@RequestBody ISWRequestDTO dto
	) throws Exception {
		log.info("Network Path Details request for {}", dto);
		return new ResponseEntity<> (amdocs.networkPathDetails(dto), HttpStatus.OK);
	}

	@RequestMapping(
		value	= "rest/ticket/details/{ticketId}"
	)
	public ResponseEntity<String> ticketDetails(
		@PathVariable("ticketId") String ticketId
	) throws Exception {

		log.info("Ticket details request for {}", ticketId);
		return new ResponseEntity<> (amdocs.ticketDetails(ticketId), HttpStatus.OK);
	}

	@RequestMapping(
		value	= "rest/customer/contact/details/{contactId}",
		method	= RequestMethod.POST
	)
	public ResponseEntity<String> contactDetails(
		@PathVariable("contactId") String contactId) throws Exception {

		log.info("Contact Details request for {}", contactId);
		return new ResponseEntity<> (amdocs.contactDetails(contactId), HttpStatus.OK);
	}

	@RequestMapping(
		value	= "rest/profile/customerId/{customerId}",
		method	= RequestMethod.POST
	)
	public ResponseEntity<String> billingEntities(
		@PathVariable("customerId") String customerId
	) throws Exception {

		log.info("Billing Entities request for {}", customerId);
		return new ResponseEntity<> (amdocs.billingEntities(customerId), HttpStatus.OK);
	}

	@RequestMapping(
		value	= "rest/customer/retrievebillingarrangements",
		method	= RequestMethod.POST
	)
	public ResponseEntity<String> billingArrangementDetails(
		@RequestBody Layer7BillingArrangementDetailsDTO dto
	) throws Exception {

		log.info("Billing Arrengement Details request for {}", dto);
		return new ResponseEntity<> (amdocs.billingArrangementDetails(dto), HttpStatus.OK);
	}

	@RequestMapping(
		value	= "rest/customer/customerProfile/{customerId}"
	)
	public ResponseEntity<String> customerProfile(
		@PathVariable("customerId") String customerId
	) throws Exception {

		log.info("Customer Profile request for {}", customerId);
		return new ResponseEntity<> (amdocs.customerProfile(customerId), HttpStatus.OK);
	}

	@RequestMapping(
		value	= "rest/customer/account/update/{accountId}",
		method	= RequestMethod.POST
	)
	public ResponseEntity<String> updateAccountDetails(
		@RequestBody Layer7UpdateAccountDetailsDTO dto,
		@PathVariable("accountId") String accountId
	) throws Exception {

		log.info("Update Account Details request for {} {}", accountId, dto);
		return new ResponseEntity<> (amdocs.updateAccountDetails(dto, accountId), HttpStatus.OK);
	}

	@RequestMapping(
		value	= "rest/billing-arrangement/update/{billingArrangementId}",
		method	= RequestMethod.POST
	)
	public ResponseEntity<String> updateBillingArrangementDetails(
		@RequestBody Layer7UpdateBillingArrangementDetailsDTO dto,
		@PathVariable("billingArrangementId") String billingArrangementId
	) throws Exception {

		log.info("Update Billing Arrangement Details request for {} {}", dto);
		return new ResponseEntity<> (amdocs.updateBillingArrangementDetails(dto), HttpStatus.OK);
	}

	@RequestMapping(
		value	= "rest/billing/updateBillingCustomer",
		method	= RequestMethod.POST
	)
	public ResponseEntity<String> updateBillingCustomerDetails(
		@RequestBody Layer7UpdateBillingCustomerDetailsDTO dto
	) throws Exception {

		log.info("Update Billing Customer Details request for {} {}", dto);
		return new ResponseEntity<> (amdocs.updateBillingCustomerDetails(dto), HttpStatus.OK);
	}

	@RequestMapping(
		value	= "rest/customer/contact/update/{contact_id}",
		method	= RequestMethod.POST
	)
	public ResponseEntity<String> updateContact(
		@RequestBody Layer7UpdateContactDTO	dto,
		@PathVariable("contact_id") String 	contact_id
	) throws Exception {
		log.info("Update Contact request for {} {}", contact_id, dto);
		return new ResponseEntity<> (amdocs.updateContact(dto, contact_id), HttpStatus.OK);
	}

	@RequestMapping(
		value	= "appointments/proposals",
		method	= RequestMethod.POST
	)
	public ResponseEntity<String> reserveTimeslots(
		@RequestBody Layer7ReserveTimeslotsDTO dto
	) throws Exception {

		log.info("Find timeslots request for {}", dto);
		return new ResponseEntity<> (amdocs.timseslots(dto), HttpStatus.OK);
	}

	@RequestMapping(
		value	= "workorders/byref/{appointment_id}"
	)
	public ResponseEntity<String> appointment(
		@PathVariable("appointment_id") String appointment_id
	) throws Exception {

		log.info("Lookup appointment request for {}", appointment_id);
		return new ResponseEntity<> (amdocs.appointment(appointment_id), HttpStatus.OK);
	}

	@RequestMapping(
		value	= "workorders",
		method	= RequestMethod.POST
	)
	public ResponseEntity<String> createAppointment(
		@RequestBody Layer7CreateAppointmentDTO dto
	) throws Exception {

		log.info("Create Appointment request for {}", dto);
		return new ResponseEntity<> (amdocs.createAppointment(dto), HttpStatus.OK);
	}

	@RequestMapping(
		value	= "workorders",
		method	= RequestMethod.PUT
	)
	public ResponseEntity<String> updateAppointment(
		@RequestBody Layer7UpdateAppointmentDTO dto
	) throws Exception {

		log.info("Update Appointment request for {}", dto);
		return new ResponseEntity<> (amdocs.updateAppointment(dto), HttpStatus.OK);
	}

	@RequestMapping(
		value	= "rest/customer/contact/interaction/create",
		method	= RequestMethod.POST
	)
	public ResponseEntity<String> createInteraction(
		@RequestBody Layer7CreateInteractionDTO dto
	) throws Exception {

		log.info("Create Interaction request for {}", dto);
		return new ResponseEntity<> (amdocs.createInteraction(dto), HttpStatus.OK);
	}

	@RequestMapping(
		value	= "rest/tickets/create",
		method	= RequestMethod.POST
	)
	public ResponseEntity<String> createTicket(
		@RequestBody Layer7CreateTicketDTO dto
	) throws Exception {

		log.info("Create Ticket request for {}", dto);
		return new ResponseEntity<> (amdocs.createTicket(dto), HttpStatus.OK);
	}

	@RequestMapping(
		value	= "rest/order/customerid/assignedproducts",
		method	= RequestMethod.POST
	)
	public ResponseEntity<String> assignedProducts(
		@RequestBody Layer7AssignedProductsDTO dto
	) throws Exception {

		log.info("Assigned products request for {}", dto);
		return new ResponseEntity<> (amdocs.assignedProducts(dto), HttpStatus.OK);
	}

	@RequestMapping(
		value	= "rest/product/assignedproductdetails",
		method	= RequestMethod.POST
	)
	public ResponseEntity<String> assignedProductDetails(
		@RequestBody Layer7AssignedProductDetailsDTO dto
	) throws Exception {

		log.info("Assigned product details request for {}", dto);
		return new ResponseEntity<> (amdocs.assignedProductDetails(dto), HttpStatus.OK);
	}

	@RequestMapping(
		value	= "rest/order/cpeSwap",
		method	= RequestMethod.POST
	)
	public ResponseEntity<String> submitSwap(
		@RequestBody Layer7SubmitSwapDTO dto
	) throws Exception {

		log.info("Submit swap request for {}", dto);
		return new ResponseEntity<> (amdocs.submitSwap(dto), HttpStatus.OK);
	}

	@RequestMapping(
		value	= "rest/customer/filters/{customerId}"
	)
	public ResponseEntity<String> customerFilters(
			@PathVariable("customerId") String customerId) throws Exception {

		log.info("Filtering customer with Id: {}", customerId);
		return new ResponseEntity<> (amdocs.customerFilters(customerId), HttpStatus.OK);
	}
	@RequestMapping(
		value	= "json/reply/GetCPEDetails_1",
		method	= RequestMethod.POST
	)
	public ResponseEntity<String> cpeDetails(
		@RequestBody ISWRequestDTO dto
	) throws Exception {

		log.info("Assigned products request for {}", dto);
		return new ResponseEntity<> (amdocs.cpeDetails(dto), HttpStatus.OK);
	}


	// ADT services
	@RequestMapping(
		value = "closeTab/{customerId}/{tabId}",
		method = RequestMethod.POST
	)
	public ResponseEntity<ADTResponseDTO> closeTab(
		@PathVariable("customerId") String customerId,
		@PathVariable("tabId") 		String tabId,
		@RequestParam("JSESSIONID") String sessionId
	) {

		log.info("Close tab request for {} {} {}", customerId, tabId, sessionId);
		return new ResponseEntity<> (adt.closeTab(customerId, tabId, sessionId), HttpStatus.OK);
	}

	@RequestMapping(
		value = "showTab/{customerId}/{tabId}",
		method = RequestMethod.POST
	)
	public ResponseEntity<ADTResponseDTO> showTab(
		@PathVariable("customerId") String customerId,
		@PathVariable("tabId") 		String tabId,
		@RequestParam("JSESSIONID") String sessionId
	) {

		log.info("Show tab request for {} {} {}", customerId, tabId, sessionId);
		return new ResponseEntity<> (adt.showTab(customerId, tabId, sessionId), HttpStatus.OK);
	}

	@RequestMapping(
		value = "addImportantAction/{customerId}",
		method = RequestMethod.POST
	)
	public ResponseEntity<ADTResponseDTO> addImportantAction(
		@RequestBody 				ADTUniversalDTO dto,
		@PathVariable("customerId") String customerId,
		@RequestParam("JSESSIONID") String sessionId
	) {

		log.info("Add important action request for {} {} {}", customerId, sessionId, dto);
		return new ResponseEntity<> (adt.addImportantAction(dto, customerId, sessionId), HttpStatus.OK);
	}

	@RequestMapping(
		value = "/dataUpdated/{customerId}/{dataType}/{id}",
		method = RequestMethod.POST
	)
	public ResponseEntity<ADTResponseDTO> dataUpdated(
		@PathVariable("customerId") String customerId,
		@PathVariable("dataType") 	String dataType,
		@PathVariable("id") 		String id,
		@RequestParam("JSESSIONID") String sessionId
	) {

		log.info("Data updated request for {} {} {} {}", customerId, dataType, id, sessionId);
		return new ResponseEntity<> (adt.dataUpdated(customerId, dataType, id, sessionId), HttpStatus.OK);
	}
	
	@RequestMapping(
		value = "/performAction/{actionName}/{customerId}",
		method = RequestMethod.POST
	)
	public ResponseEntity<ADTResponseDTO> performAction(
			@PathVariable("customerId") String customerId,
			@PathVariable("actionName") String actionName,
			@RequestParam("sessionId") String sessionId,
			@RequestParam("JSESSIONID") String adtSessionId
	) {

		log.info("Perform Action request for {} {} {} {}", customerId, actionName, sessionId, adtSessionId);
		return new ResponseEntity<> (adt.performAction(customerId, actionName, sessionId, adtSessionId), HttpStatus.OK);
	}

	@RequestMapping(
		value = "isPrivileged/{privilege}"
	)
	public ResponseEntity<ADTResponseDTO> isPrivileged(
		@PathVariable("privilege") 	String privilege,
		@RequestParam("JSESSIONID") String sessionId
	) {

		log.info("Is privileged request for {} {}", privilege, sessionId);
		return new ResponseEntity<> (adt.isPrivileged(privilege, sessionId), HttpStatus.OK);
	}

	@RequestMapping(
		value	= "rest/error/amdocs",
		method	= {RequestMethod.GET, RequestMethod.POST}
	)
	public ResponseEntity<String> throwAmdocsError(
	) throws Exception {

		log.info("Generating an amdocs http error");

		String response = null;
		ResponseEntity<String> responseEntity = null;

		try {
			response = amdocs.error();
			responseEntity = new ResponseEntity<> (response, HttpStatus.OK);
		} catch (RestClientResponseException ex) {
			String respObj = objMapper.writeValueAsString( objMapper.readValue(ex.getResponseBodyAsString(), new TypeReference<Map<String, Object>>() {}) );
			responseEntity = new ResponseEntity<> (respObj, HttpStatus.BAD_REQUEST);
		}

		return responseEntity;
	}

	@RequestMapping(
		value	= "rest/error/adt",
		method	= {RequestMethod.GET, RequestMethod.POST}
	)
	public ResponseEntity<ADTResponseDTO> throwADTError(
	) throws Exception {

		log.info("Generating an ADT http error");

		ADTResponseDTO response = null;
		ResponseEntity<ADTResponseDTO> responseEntity = null;

		try {
			response = adt.error();
			responseEntity = new ResponseEntity<> (response, HttpStatus.OK);
		} catch (RestClientResponseException ex) {
			ADTResponseDTO respObj = objMapper.readValue(ex.getResponseBodyAsString(), ADTResponseDTO.class);
			responseEntity = new ResponseEntity<> (respObj, HttpStatus.BAD_REQUEST);
		}

		return responseEntity;
	}
}
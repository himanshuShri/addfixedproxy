package jacada.addFixedProxy.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jacada.addFixedProxy.dto.proxy.adt.ProxyADTRequestDTO;
import jacada.addFixedProxy.service.adt.ADTService;
import lombok.extern.slf4j.Slf4j;

//CLEAN swagger

@Slf4j
@RestController
@RequestMapping (
	value		= "/adt",
	method		= RequestMethod.GET,
	consumes	= MediaType.APPLICATION_JSON_VALUE,
	produces	= MediaType.APPLICATION_JSON_VALUE
)
public class ADTController {

	@Autowired private ADTService adt;

	// ADT services
	@RequestMapping(
		value = "/tab/close",
		method = RequestMethod.POST
	)
	public Map<String, Object> closeTab(
			@RequestBody ProxyADTRequestDTO dto
	) {

		log.info("closeTab requests are handled by ADT");
		log.debug("request details: {}", dto);
		return adt.closeTab(dto);
	}

	@RequestMapping(
		value = "/tab/show",
		method = RequestMethod.POST
	)
	public Map<String, Object> showTab(
			@RequestBody ProxyADTRequestDTO dto
	) {

		log.info("showTab requests are handled by ADT");
		log.debug("request details: {}", dto);
		return adt.showTab(dto);
	}

	@RequestMapping(
		value = "/update/importantAction",
		method = RequestMethod.POST
	)
	public Map<String, Object> addImportantAction(
			@RequestBody ProxyADTRequestDTO dto
	) {

		log.info("Add important action requests are handled by ADT");
		log.debug("request details: {}", dto);
		return adt.addImportantAction(dto);
	}

	@RequestMapping(
		value = "/update/data",
		method = RequestMethod.POST
	)
	public Map<String, Object> dataUpdated(
			@RequestBody ProxyADTRequestDTO dto
	) {

		log.info("Data updated requests are handled by ADT");
		log.debug("request details: {}", dto);
		return adt.dataUpdated(dto);
	}

	@RequestMapping(
		value = "/privilege/check/{privilege}"
	)
	public Map<String, Object> isPrivileged(
		@PathVariable("privilege") String privilege,
		@RequestParam("sessionId") String sessionId,
		@RequestParam("JSESSIONID") String adtSessionId
	) {

		log.info("Privilege requests are handled by ADT");

		ProxyADTRequestDTO dto = new ProxyADTRequestDTO();
		dto.setPrivilege(privilege);
		dto.setSessionId(sessionId);
		dto.setAdtSessionId(adtSessionId);
		log.debug("request details: {}", dto);

		return adt.isPrivileged(dto);
	}

	@RequestMapping(
		value = "/appointment/cancel",
		method = RequestMethod.POST
	)
	public Map<String, Object> cancelAppointment(
		@RequestBody ProxyADTRequestDTO dto
	) {

		log.info("Appointment cancelation requests are handled by ADT");
		log.debug("request details: {}", dto);
		return adt.cancelAppointment(dto);
	}
	
	@RequestMapping(
			value = "/auditPoint/create",
			method = RequestMethod.POST
		)
		public Map<String, Object> createAuditPoint(
			@RequestBody ProxyADTRequestDTO dto
		) {

			log.info("Audit point creation requests are handled by ADT");
			log.debug("request details: {}", dto);
			return adt.createAuditPoint(dto);
		}

}
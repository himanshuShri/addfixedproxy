package jacada.addFixedProxy.dto;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;

/**
 * Utility class to ease the considerable pain of dealing with xml namespaces.
 * Copied and adapted from http://www.kdgregory.com/index.php?page=xml.xpath
 * @author ndoan
 */
public class SimpleXMLNamespaceContext implements NamespaceContext {

	private String _prefix;
	private String _namespaceUri;
	private List<String> _prefixes;

	public SimpleXMLNamespaceContext(String prefix, String uri) {

		_prefix = prefix;
		_namespaceUri = uri;
		_prefixes = Arrays.asList(_prefix);
	}

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Iterator getPrefixes(String uri) {

		if (uri == null)
			throw new IllegalArgumentException("nsURI may not be null");
		else if (_namespaceUri.equals(uri))
			return _prefixes.iterator();
		else if (XMLConstants.XML_NS_URI.equals(uri))
			return Arrays.asList(XMLConstants.XML_NS_PREFIX).iterator();
		else if (XMLConstants.XMLNS_ATTRIBUTE_NS_URI.equals(uri))
			return Arrays.asList(XMLConstants.XMLNS_ATTRIBUTE).iterator();
		else
			return Collections.emptyList().iterator();
	}

	@Override
	public String getPrefix(String uri) {

		if (uri == null)
			throw new IllegalArgumentException("nsURI may not be null");
		else if (_namespaceUri.equals(uri))
			return _prefix;
		else if (XMLConstants.XML_NS_URI.equals(uri))
			return XMLConstants.XML_NS_PREFIX;
		else if (XMLConstants.XMLNS_ATTRIBUTE_NS_URI.equals(uri))
			return XMLConstants.XMLNS_ATTRIBUTE;
		else
			return null;
	}

	@Override
	public String getNamespaceURI(String prefix) {

		if (prefix == null)
			throw new IllegalArgumentException("prefix may not be null");
		else if (_prefix.equals(prefix))
			return _namespaceUri;
		else if (XMLConstants.XML_NS_PREFIX.equals(prefix))
			return XMLConstants.XML_NS_URI;
		else if (XMLConstants.XMLNS_ATTRIBUTE.equals(prefix))
			return XMLConstants.XMLNS_ATTRIBUTE_NS_URI;
		else
			return null;
	}
}
package jacada.addFixedProxy.dto.proxy.adt;

import jacada.addFixedProxy.dto.RequestDTO;
import jacada.addFixedProxy.dto.proxy.adt.ADTProxySharedTypes.Parameter;
import lombok.Data;

/**
 * All the ADT requests use the same inputs
 * @author ndoan
 *
 */
@Data
public class ProxyADTRequestDTO implements RequestDTO {

	private String accountId;
	private String agentId;
	private String businessTransactionId;
	private String sessionId;
	private String adtSessionId;
	private String customerId;
	private String contactId;
	private String tabId;
	private String id;
	private Parameter[] parameters;

	private String customerType;
	private String dataType;
	private String version;
	private String importantAction;
	private String privilege;

	private static final String CUSTOMER_ID_TPL		= "[customerId]";
	private static final String SESSION_ID_TPL		= "[sessionId]";
	private static final String ADT_SESSION_ID_TPL	= "[ADTSessionId]";
	private static final String TAB_ID_TPL			= "[tabId]";
	private static final String DATA_TYPE_TPL		= "[dataType]";
	private static final String ID_TPL				= "[id]";
	private static final String PRIVILEGE_TPL		= "[privilege]";

	public String generateUrl(String ep) {

		String url = ep;

		url = url.contains(CUSTOMER_ID_TPL)		?	url.replace(CUSTOMER_ID_TPL, 	customerId)		: url;
		url = url.contains(TAB_ID_TPL)			?	url.replace(TAB_ID_TPL, 		tabId) 			: url;
		url = url.contains(SESSION_ID_TPL)		?	url.replace(SESSION_ID_TPL, 	sessionId) 		: url;
		url = url.contains(ADT_SESSION_ID_TPL)	?	url.replace(ADT_SESSION_ID_TPL,	adtSessionId)	: url;
		url = url.contains(DATA_TYPE_TPL)		?	url.replace(DATA_TYPE_TPL, 		dataType)		: url;
		url = url.contains(ID_TPL)				?	url.replace(ID_TPL, 			id)				: url;
		url = url.contains(PRIVILEGE_TPL)		?	url.replace(PRIVILEGE_TPL, 		privilege)		: url;

		return url;
	}
}
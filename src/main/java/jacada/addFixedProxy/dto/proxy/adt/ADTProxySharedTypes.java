package jacada.addFixedProxy.dto.proxy.adt;

import lombok.Data;

@Data
public class ADTProxySharedTypes {

	@Data
	public static class Parameter {

		private String name;
		private String value;
	}
}
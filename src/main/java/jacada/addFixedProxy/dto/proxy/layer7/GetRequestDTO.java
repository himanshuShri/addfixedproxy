package jacada.addFixedProxy.dto.proxy.layer7;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

/**
 * This object allows the url generation to be extracted away from the service class,
 * mostly for the benefit of simple GET requests where there is no DTO.
 * It assumes that only 1 id field is used and that there is no url templating when
 * this id is null.
 * @author ndoan
 */
@Data
@Getter(AccessLevel.NONE)
@RequiredArgsConstructor
@NoArgsConstructor(force = true)
@EqualsAndHashCode(callSuper = false)

public class GetRequestDTO extends ProxyLayer7RequestDTO {

	private static final String APPOINTMENT_ID_TPL	= "[appointmentId]";
	private static final String CUSTOMER_ID_TPL		= "[customerId]";
	private static final String CONTACT_ID_TPL		= "[contactId]";

	private final String id;
	private final ID_TYPE type;

	@Override
	public String generateUrl(String ep) {

    String url = ep;

    url = url.contains(CUSTOMER_ID_TPL)     ?       url.replace(CUSTOMER_ID_TPL, id)        : url;
    url = url.contains(CONTACT_ID_TPL)      ?       url.replace(CONTACT_ID_TPL, id)         : url;
    url = url.contains(APPOINTMENT_ID_TPL)  ?       url.replace(APPOINTMENT_ID_TPL, id)     : url;

    return url;
	}

	public String id(ID_TYPE t) {
		return type.equals(t) ? id : null;
	}

	public static enum ID_TYPE {
		APPOINTMENT_ID,
		BILLING_ARRANGEMENT_ID,
		CASE_ID,
		CONTACT_ID,
		CUSTOMER_ID,
		ASSIGNED_PRODUCT_ID
	}
}
package jacada.addFixedProxy.dto.proxy.layer7;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
public class UpdateAppointmentDTO extends AppointmentDTO {

	private String oldAppointmentId;
}
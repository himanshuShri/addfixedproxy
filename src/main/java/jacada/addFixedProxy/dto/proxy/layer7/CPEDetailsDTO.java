package jacada.addFixedProxy.dto.proxy.layer7;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CPEDetailsDTO extends ProxyLayer7ISWRequestDTO {

	private String newSerialNumber;
	private String oldSerialNumber;
}
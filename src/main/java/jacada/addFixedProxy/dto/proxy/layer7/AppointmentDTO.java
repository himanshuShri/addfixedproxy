package jacada.addFixedProxy.dto.proxy.layer7;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public abstract class AppointmentDTO extends ProxyLayer7RequestDTO {

	private String caseCategory;
	private String caseType;
	private String caseSubType;
	private String workOrderPriority;

	private String customerId;
	private String appointmentId;
	private String availableStartDate;
	private String availableFinishDate;
	private String agentId;
	private String note;
	private String alternativePhoneNo;
	private String businessTransactionId;
}
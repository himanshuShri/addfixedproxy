package jacada.addFixedProxy.dto.proxy.layer7;

import java.util.List;

import jacada.addFixedProxy.dto.proxy.layer7.Layer7ProxySharedTypes.Product;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
public class CreateAppointmentDTO extends AppointmentDTO {

	// not part of updateAppintment interface
	private List<Product> products;
}
package jacada.addFixedProxy.dto.proxy.layer7;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class ReserveTimeslotsDTO extends ProxyLayer7RequestDTO {

	private String channel;
	private String code;
	private String customerId;
	private int numberOfProposals;
	private int daysToOffset;
}
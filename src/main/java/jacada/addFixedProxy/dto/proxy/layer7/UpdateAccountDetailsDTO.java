package jacada.addFixedProxy.dto.proxy.layer7;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class UpdateAccountDetailsDTO extends ProxyLayer7RequestDTO {

	private String accountId;
	private String accountName;
	private String kvkNo;

	@Override
	public String generateUrl(String ep) {
		return ep.replace("[accountId]", accountId);
	}
}
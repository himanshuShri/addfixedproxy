package jacada.addFixedProxy.dto.proxy.layer7;

import jacada.addFixedProxy.dto.proxy.layer7.Layer7ProxySharedTypes.AddressDetails;
import lombok.Data;

@Data
public class AddressDTO  {
	private String businessTransactionId;
	private AddressDetails address;
}
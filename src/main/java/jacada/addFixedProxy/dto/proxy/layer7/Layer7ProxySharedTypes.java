package jacada.addFixedProxy.dto.proxy.layer7;

import lombok.Data;

@Data
public class Layer7ProxySharedTypes {

	@Data
	public static class Product {

		private String productName;
		private String installationDate;
		private String productType;
	}

	@Data
	public static class Notification {

		private String type;
		private String value;
	}

	@Data
	public static class AddressDetails {

		private String streetName;
		private String houseNumber;
		private String zipCode;
		private String city;
		private String country;
		private String flat;
		private String houseNumberExtension;
		private String roomNumber;
	}
}
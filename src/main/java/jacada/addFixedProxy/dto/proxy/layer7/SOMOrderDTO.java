package jacada.addFixedProxy.dto.proxy.layer7;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class SOMOrderDTO extends ProxyLayer7RequestDTO {
	private String businessTransactionId;
	private String serviceGroupId;
}
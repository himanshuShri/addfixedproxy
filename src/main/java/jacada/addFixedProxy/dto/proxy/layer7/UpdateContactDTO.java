package jacada.addFixedProxy.dto.proxy.layer7;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class UpdateContactDTO extends ProxyLayer7RequestDTO {
	private String contactId;
	private String firstName;
	private String lastName;
	private String email;
	private String middleName;
	private String title;
	private String dob;

	private String country;
	private String street;
	private String city;
	private String postCode;
	private String houseNumber;

	private String facebook;
	private String twitter;
	private String linkedIn;
	private String privatePhone;
	private String officePhone;
	private String mobilePhone;

	// TODO find out which of these are supported and remove the rest
	// at least mobilePhoneAddressId looks to be problematic
	private int facebookAddressId;
	private int twitterAddressId;
	private int linkedInAddressId;
	private int privatePhoneAddressId;
	private int officePhoneAddressId;
	private int mobilePhoneAddressId;

	@Override
	public String generateUrl(String ep) {
		return ep.replace("[contactId]", contactId);
	}
}
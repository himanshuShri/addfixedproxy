package jacada.addFixedProxy.dto.proxy.layer7;


import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class NetworkPathDetailsDTO extends ProxyLayer7ISWRequestDTO {

	private String houseNumberExtension;
	private String room;
	private String houseNumber;
	private String houseLetter;
	private String postCode;
}
package jacada.addFixedProxy.dto.proxy.layer7;

import java.util.List;

import jacada.addFixedProxy.dto.proxy.layer7.Layer7ProxySharedTypes.Notification;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper=false)
public class CreateTicketDTO extends ProxyLayer7RequestDTO {

	private String channel;
	private String accountId;
	private String ticketType1;
	private String ticketType2;
	private String ticketType3;
	private String priority;
	private String title;
	private String note;
	private Notification notification;
	private boolean createInteractionFlag;
	private String contactId;
	private List<FlexibleAttribute> flexibleAttributes;

	@Data
	@NoArgsConstructor
	public static class FlexibleAttribute {

		private String key;
		private String value;
	}
}
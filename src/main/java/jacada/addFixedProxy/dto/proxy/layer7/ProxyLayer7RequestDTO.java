package jacada.addFixedProxy.dto.proxy.layer7;

import jacada.addFixedProxy.dto.RequestDTO;

public class ProxyLayer7RequestDTO implements RequestDTO {

	public String generateUrl(String ep) {
		return ep;
	}
}
package jacada.addFixedProxy.dto.proxy.layer7;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class UpdateBillingCustomerDetailsDTO extends ProxyLayer7RequestDTO {

	private String billingCustomerId;
	private String contactId;
	private VatDetails vatDetails;

	@Data
	public class VatDetails {

		private String vatCountry;
		private String vatIdNumber;
		private boolean vatValid;
	}
}
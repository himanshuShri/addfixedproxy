package jacada.addFixedProxy.dto.proxy.layer7;

import jacada.addFixedProxy.dto.proxy.layer7.Layer7ProxySharedTypes.AddressDetails;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class UpdateBillingArrangementDetailsDTO extends ProxyLayer7RequestDTO {

	private String billingArrangementId;
	private String billingArrangementName;
	private String customerId;
	private AddressDetails physicalAddress;
	private String emailAddress;
	private boolean cash;
	private PayMeans payMeans;
	private UpdateType updateType;
	private String deliveryMethod;

	@Override
	public String generateUrl(String ep) {
		return ep.replace("[billingArrangementId]", billingArrangementId);
	}

	@Data
	public class PayMeans {
		private String bankAccountName;
		private String bankAccountNumber;
		private String contactId;
	}

	public enum UpdateType {
		ADDRESS,
		DELIVERY,
		PAY_METHOD,
		PAY_ACCOUNT;
	}
}
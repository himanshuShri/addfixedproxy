package jacada.addFixedProxy.dto.proxy.layer7;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class CreateInteractionDTO extends ProxyLayer7RequestDTO {

	private String contactId;
	private String interactionTitle;
	private String reason1;
	private String reason2;
	private String notes;
	private String direction;
	private String linkedEntityId;
	private String linkedEntityType;
	private String channel;
	private String media;
	private String interactionType;
	private String ci_result;
}
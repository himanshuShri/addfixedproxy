package jacada.addFixedProxy.dto.proxy.layer7;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class ProxyLayer7ISWRequestDTO extends ProxyLayer7RequestDTO {

	private String businessTransactionId;
}
package jacada.addFixedProxy.dto.proxy.layer7;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class SubmitSwapDTO extends ProxyLayer7RequestDTO {

	private String assignedComponentId;
	private String reasonCode;
	private String reasonText;
	private String svcRequiredDate;
	private String oldSerialNo;
	private String newSerialNo;
	private String targetLogicalEquipmentId;
	private String oldSmartCardSerialNo;
	private String newSmartCardSerialNo;
	private String returnMethod;
	private String deliveryMethod;
	private String deliveryAddressId;
}
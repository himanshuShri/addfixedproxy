package jacada.addFixedProxy.dto.layer7;

import jacada.addFixedProxy.dto.proxy.layer7.GetRequestDTO;
import jacada.addFixedProxy.dto.proxy.layer7.GetRequestDTO.ID_TYPE;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Layer7AssignedProductsDTO implements Layer7RequestDTO {

	private String customerID;

	public Layer7AssignedProductsDTO(GetRequestDTO incoming) {

		customerID = incoming.id(ID_TYPE.CUSTOMER_ID);
	}
}
package jacada.addFixedProxy.dto.layer7;

import jacada.addFixedProxy.Utils;
import jacada.addFixedProxy.dto.proxy.layer7.SOMOrderDTO;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Layer7SOMOrderDTO implements Layer7XmlRequestDTO {

	private static String REQUEST_TEMPLATE = null;
	private String body;

	public String body() {

		return body;
	}

	public Layer7SOMOrderDTO(SOMOrderDTO incoming) throws Exception {

		/* parser approach */

		if (REQUEST_TEMPLATE == null) {
			log.debug("Loading the SOM request template");
			REQUEST_TEMPLATE = Utils.RESOURCE_STRING("/som/SomOrderTemplate.xml");
		}

		String externalKey 	= incoming.getBusinessTransactionId();
		String serviceGroup = incoming.getServiceGroupId();

		body = REQUEST_TEMPLATE.
				replace("$_EXTERNAL_ID_$", 		externalKey).
				replace("$_SERVICE_GROUP_ID_$", serviceGroup);


		/* text processing approach */

		/* programmatic approach

		String externalKey 	= incoming.getBusinessTransactionId();
		String customerKey 	= incoming.getServiceGroupId();

		order = new Order();
		order.getOrderKey().setExternalKey(externalKey);
		order.getCustomer().getCustomerKey().setExternalKey(customerKey);

		var refreshEntitlements = new Item();
		refreshEntitlements.setCharacteristic(orderScenario);
		var characteristicValue = new CharacteristicValue();
		characteristicValue.setValueDetail(DTVRefreshEntitlements);
		var value = refreshEntitlements.getValue();
		value.add(characteristicValue);
		order.getCharacteristics().getItem().add(refreshEntitlements);
		*/


/*
		File inputFile = new File(
        	getClass().getClassLoader().getResource("som/SomOrderTemplate.xml").getFile()
        );

        JAXBContext jaxbContext = JAXBContext.newInstance(Order.class);

        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        order= (Order) jaxbUnmarshaller.unmarshal(inputFile);

		order.getOrderKey().setExternalKey(externalKey);
		order.getCustomer().getCustomerKey().setExternalKey(customerKey);

		var refreshEntitlements = new Item();
		refreshEntitlements.setCharacteristic(orderScenario);
		var characteristicValue = new CharacteristicValue();
		characteristicValue.setValueDetail(DTVRefreshEntitlements);
		var value = refreshEntitlements.getValue();
		value.add(characteristicValue);
		order.getCharacteristics().getItem().add(refreshEntitlements);
*/
	}
}
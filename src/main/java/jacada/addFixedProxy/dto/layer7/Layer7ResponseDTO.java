package jacada.addFixedProxy.dto.layer7;

import java.util.ArrayList;
import java.util.Map;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class Layer7ResponseDTO {

	private int status;
	private ArrayList<Map<String, Object>> data 	= new ArrayList<>();
	private ArrayList<Map<String, Object>> error	= new ArrayList<>();

	private static Layer7ResponseDTO createResponseShell() {
		Layer7ResponseDTO response = new Layer7ResponseDTO();
		response.setData(new ArrayList<Map<String, Object>>());
		response.setError(new ArrayList<Map<String, Object>>());

		return response;
	}


	// unfortunately, amdocs doesn't respect its own return types on calls that
	// contracts out so we need to manually reconstruct an amdocs response here.
	// don't use this - instead make sure that the tinned response is sent back literally
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Layer7ResponseDTO convert(Object o) {

		Layer7ResponseDTO response = null;
		ReturnType z = ReturnType.valueOf(o.getClass().getSimpleName());
		switch (z) {

		// this is the default scenario where there really is a proper response
		case AmdocsResponseDTO:
			log.info("AMDOCS RESPONSE TYPE");
			response = (Layer7ResponseDTO) o;
			break;

		// here is the surrogate case where the response is modelled as a map
		// try to guess from the status code how it should look
		case LinkedHashMap:
			Map map = (Map) o;
			Object status = map.get("statusCode");
			int statusCode = status==null? 0 :Integer.valueOf((String) status).intValue();

			response = createResponseShell();
			response.setStatus(statusCode);

			if(statusCode == 0) {
				log.info(String.format("Caught what looks like either a reserve timeslots or network path details response (assuming success) - status [%d]", statusCode));
				response.getData().add(map);

			} else if(statusCode < 400) {
				log.info(String.format("Caught what looks like either a create or update appointment success response - status [%d]", statusCode));
				response.getData().add(map);

			} else {
				log.info(String.format("Caught what looks like either a create or update appointment failure response - status [%d]", statusCode));
				response.getError().add(map);
			}

			break;

		// no idea how we came to be here, so log the error and return something empty to avoid the NPE
		default:
			log.error(String.format("No mapping known for type - ", o.getClass().getSimpleName()));
			response =createResponseShell();
			break;
		}

		return response;
	}

	enum ReturnType {
		AmdocsResponseDTO, LinkedHashMap
	}
}
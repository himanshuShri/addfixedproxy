package jacada.addFixedProxy.dto.layer7;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.jayway.jsonpath.TypeRef;

import jacada.addFixedProxy.UtilBeans;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
@NoArgsConstructor
public class Layer7SharedTypes {

	public static TypeRef<List<String>> StringArrTypeRef = new TypeRef<List<String>>() {};

	public static String formatAsISODateTime(String dt) throws ParseException {
		return transformDate(dt, UtilBeans.ISO_T_DATE_FORMATTER);
	}

	private static String transformDate(String dt, DateFormat as) throws ParseException {

		return dt == null? null: as.format(UtilBeans.US_DATETIME_FORMATTER.parse(dt));
	}

	@Data
	@NoArgsConstructor
	public static class ProductWrapper {

		private List<Product> product = new ArrayList<>();

		public ProductWrapper(List<jacada.addFixedProxy.dto.proxy.layer7.Layer7ProxySharedTypes.Product> products) {

			for (jacada.addFixedProxy.dto.proxy.layer7.Layer7ProxySharedTypes.Product prod : products) {
				product.add(new Product(prod));
			}
		}
	}

	@Data
	@NoArgsConstructor
	public static class InstalledProductWrapper {
		private List<Product> InstalledProduct = new ArrayList<>();
	}

	@Data
	@NoArgsConstructor
	public static class Product {

		private String productName;
		private String installationDate;
		private String productType;

		public Product(jacada.addFixedProxy.dto.proxy.layer7.Layer7ProxySharedTypes.Product p) {

			try {
				installationDate = Layer7SharedTypes.formatAsISODateTime(p.getInstallationDate());
			} catch (ParseException e) {
				log.error("Could not parse date - {}", e.getMessage());
				e.printStackTrace();
			}
			productName = p.getProductName();
			productType = p.getProductType();
		}

		public Product(String name, String date) {

			productName = name;
			try {
				installationDate = Layer7SharedTypes.formatAsISODateTime(date);
			} catch (Throwable e) {
				log.error("null date found, using null");
			}
			;
		}
	}

	@Data
	@NoArgsConstructor
	public static class CPEInfoWrapper {
		private List<CPEInfo> cpeInfo = new ArrayList<>();
	}

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class CPEInfo {

		private String type;
		private String macAddress;
	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Notification {

		private String type;
		private String value;

		public Notification(jacada.addFixedProxy.dto.proxy.layer7.Layer7ProxySharedTypes.Notification notif) {

			type = notif.getType();
			value = notif.getValue();
		}

		public static Notification DEFAULT() {
			return new Notification("None", "");
		}
	}

	@Data
	@NoArgsConstructor
	public static class AddressDetails {

		private String streetname;
		private String housenumber;
		private String zip_code;
		private String city;
		private String country;
		private String flat;

		public AddressDetails(jacada.addFixedProxy.dto.proxy.layer7.Layer7ProxySharedTypes.AddressDetails addr) {

			streetname = addr.getStreetName();
			housenumber = addr.getHouseNumber();
			zip_code = addr.getZipCode();
			city = addr.getCity();
			country = addr.getCountry();
			flat = addr.getFlat();
		}
	}

	@Data
	@NoArgsConstructor
	public static class BillingAddress {

		private String streetName;
		private String houseNumber;
		private String zipCode;
		private String city;
		private String country;
		private int addressId;
		private String timeZone;
		private String resident;
		private String state;
	}
}
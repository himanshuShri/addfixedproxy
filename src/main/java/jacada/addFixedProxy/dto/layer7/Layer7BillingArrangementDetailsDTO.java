package jacada.addFixedProxy.dto.layer7;

import jacada.addFixedProxy.dto.proxy.layer7.GetRequestDTO;
import jacada.addFixedProxy.dto.proxy.layer7.GetRequestDTO.ID_TYPE;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Layer7BillingArrangementDetailsDTO implements Layer7RequestDTO {

	private String billingArrangementIDs;

	public Layer7BillingArrangementDetailsDTO(GetRequestDTO incoming) {

		billingArrangementIDs = incoming.id(ID_TYPE.BILLING_ARRANGEMENT_ID);
	}
}
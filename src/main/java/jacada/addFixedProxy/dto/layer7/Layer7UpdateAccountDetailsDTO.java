package jacada.addFixedProxy.dto.layer7;

import jacada.addFixedProxy.dto.proxy.layer7.UpdateAccountDetailsDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Layer7UpdateAccountDetailsDTO implements Layer7RequestDTO {

	private accountDetails accountDetails;
	public Layer7UpdateAccountDetailsDTO(UpdateAccountDetailsDTO incoming) {

		accountDetails = new accountDetails();
		accountDetails.setKvkNo(incoming.getKvkNo());
		accountDetails.setAccountName(incoming.getAccountName());
	}
}

@Data
class accountDetails {

	private String kvkNo;
	private String accountName;
}
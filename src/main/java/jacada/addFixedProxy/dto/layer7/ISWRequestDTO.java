package jacada.addFixedProxy.dto.layer7;

import javax.xml.datatype.XMLGregorianCalendar;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
public class ISWRequestDTO implements Layer7RequestDTO {

	// this does much the same as @JsonProperty, but it prevents the wrong name being published
  @Getter(onMethod=@__({@JsonProperty("Header")})) 	private ISWRequestHeader Header;
  @Getter(onMethod=@__({@JsonProperty("Body")}))	protected Object Body;

	public ISWRequestDTO() {
		Header = new ISWRequestHeader();
	}

	@Data
	@NoArgsConstructor
	public static class ISWRequestHeader {
		@Getter(onMethod=@__({@JsonProperty("BusinessTransactionID")})) private String BusinessTransactionID;
		@Getter(onMethod=@__({@JsonProperty("SentTimestamp")})) 				private XMLGregorianCalendar SentTimestamp;
		@Getter(onMethod=@__({@JsonProperty("SourceContext")})) 				private ISWRequestHeaderContext SourceContext;
	}

	@Data
	@NoArgsConstructor
	public static class ISWRequestHeaderContext {
		@Getter(onMethod=@__({@JsonProperty("Host")})) 				private String Host;
		@Getter(onMethod=@__({@JsonProperty("Application")})) private String Application;
	}
}
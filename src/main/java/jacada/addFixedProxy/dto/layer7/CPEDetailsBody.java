package jacada.addFixedProxy.dto.layer7;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

import jacada.addFixedProxy.dto.proxy.layer7.CPEDetailsDTO;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CPEDetailsBody {

	@Getter(onMethod = @__({ @JsonProperty("CPESerialNumberList") }))
	private List<CPEDetailsBodySerialNumber> CPESerialNumberList;

	public CPEDetailsBody(CPEDetailsDTO dto) {

		String oldSN = dto.getOldSerialNumber();
		String newSN = dto.getNewSerialNumber();

		CPESerialNumberList = new ArrayList<>();
		CPESerialNumberList.add(new CPEDetailsBodySerialNumber(oldSN));

		// only add the new serial number if it's non-null as amdocs
		// will take the blank and try to verify it.
		if(! StringUtils.isEmpty(newSN)) {
			CPESerialNumberList.add(new CPEDetailsBodySerialNumber(newSN));
		}
	}

	@Data
	@NoArgsConstructor
	public static class CPEDetailsBodySerialNumber {

		@Getter(onMethod = @__({ @JsonProperty("CPESerialNumber") }))
		private String CPESerialNumber;

		public CPEDetailsBodySerialNumber(String s) {
			CPESerialNumber = s;
		}
	}
}

package jacada.addFixedProxy.dto.layer7;

import jacada.addFixedProxy.dto.proxy.layer7.ReserveTimeslotsDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Layer7ReserveTimeslotsDTO implements Layer7RequestDTO {

	private String 	channel;
	private String 	workorderType;
	private String 	desireStartDate;
	private int 	maxProposal;
	private String 	streetName;
	private String 	houseFlatNumber;
	private String 	houseFlatExt;
	private String 	city;
	private String 	postCode;

	public Layer7ReserveTimeslotsDTO(ReserveTimeslotsDTO incoming) {

		channel 		= incoming.getChannel();
		workorderType 	= incoming.getCode();
		maxProposal 	= incoming.getNumberOfProposals();
	}
}
package jacada.addFixedProxy.dto.layer7;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import jacada.addFixedProxy.dto.layer7.Layer7SharedTypes.AddressDetails;
import jacada.addFixedProxy.dto.proxy.layer7.UpdateBillingArrangementDetailsDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties({ "electronic_DELIVERY_INDICATOR" }) // this name is case sensitive
public class Layer7UpdateBillingArrangementDetailsDTO implements Layer7RequestDTO {

	private final String ELECTRONIC_DELIVERY_INDICATOR = "E";

	private String billing_customer_id;
	private String billing_arrangement_id;
	private String bill_delivery;
	private String name;
	private AddressDetails address;
	private String email_address_for_bill;
	private PayMeans pay_mean;

	// prevents jackson marshalling 'cash' as 'isCash' and subsequently failing
	// the internal value is now arbitrary and the value is also serialised as 'isCash'
	@JsonProperty("isCash")	private boolean cash;

	public Layer7UpdateBillingArrangementDetailsDTO(UpdateBillingArrangementDetailsDTO incoming) {

		// these fields are always needed
		billing_customer_id = incoming.getCustomerId();
		billing_arrangement_id = incoming.getBillingArrangementId();

		// selectively add pieces of the request
		switch(incoming.getUpdateType()) {

		  case DELIVERY:
		  	String deliveryMethod = incoming.getDeliveryMethod();
		  	name = incoming.getBillingArrangementName();

		  	bill_delivery = "P";
		  	if(deliveryMethod.startsWith(ELECTRONIC_DELIVERY_INDICATOR)) {
		  		bill_delivery = "EB";
		  		email_address_for_bill = incoming.getEmailAddress();
		  	}
				break;

		  case ADDRESS:
				address = new AddressDetails(incoming.getPhysicalAddress());
		  	break;

		  case PAY_METHOD:
		  	cash = incoming.isCash();
		  	if(!cash) {
		  		pay_mean = new PayMeans(incoming.getPayMeans());
		  	}
		  	break;

		  case PAY_ACCOUNT:
	  		pay_mean = new PayMeans(incoming.getPayMeans());
	  		break;

			default:
				throw new IllegalArgumentException("Unrecognised update Type - "+ incoming.getUpdateType());
		};
	}

	@Data
	@NoArgsConstructor
	public class PayMeans {

		private String bank_account_name;
		private String bank_account_number;
		private String contact_id;

		public PayMeans(jacada.addFixedProxy.dto.proxy.layer7.UpdateBillingArrangementDetailsDTO.PayMeans pm) {

			bank_account_name = pm.getBankAccountName();
			bank_account_number = pm.getBankAccountNumber();
			contact_id = pm.getContactId();
		}
	}
}
package jacada.addFixedProxy.dto.layer7;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonInclude;

import jacada.addFixedProxy.dto.proxy.layer7.SubmitSwapDTO;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Data
@NoArgsConstructor
@Slf4j
//BSL uses bizarre XOR logic which demands optional fields are not sent at all.
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Layer7SubmitSwapDTO implements Layer7RequestDTO {

	private List<EquipmentSwapDetails> equipment_swap_details = new ArrayList<>();
	private String service_required_date;
	private String reason_code;
	private String reason_text;
	private DeliveryDetails delivery_details;
	private ReturnDetails return_details;

	public Layer7SubmitSwapDTO(SubmitSwapDTO incoming) {

		service_required_date 	= incoming.getSvcRequiredDate();
		reason_code 			= incoming.getReasonCode();
		reason_text 			= incoming.getReasonText();
		return_details 			= new ReturnDetails(incoming);
		delivery_details 		= DeliveryDetails.CreateDeliveryDetails(incoming);
		equipment_swap_details.add(new EquipmentSwapDetails(incoming));
}

	@Data
	@NoArgsConstructor
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	public static class EquipmentSwapDetails {

		private String assignedComponentId;
		private String oldSerialNo;
		private String newSerialNo;
		private String targetLogicalEquipmentId;
		private SmartCardSwapDetails smartCardSwapDetails;

		public EquipmentSwapDetails(SubmitSwapDTO incoming) {
			assignedComponentId 			= incoming.getAssignedComponentId();
			oldSerialNo						= incoming.getOldSerialNo();
			var newSN						= incoming.getNewSerialNo();
			var targetLEI					= incoming.getTargetLogicalEquipmentId();

			if (StringUtils.isEmpty(newSN)) {
				log.debug("newSerialNo was not supplied so setting targetLogicalEquipmentId [{}]", targetLEI);
				targetLogicalEquipmentId 	= targetLEI;
			} else {
				newSerialNo					= newSN;
				log.debug("newSerialNo [{}] was supplied so adding newSerialNo and removing targetLogicalEquipmentId",
						newSN);
			}

			if(!StringUtils.isEmpty(incoming.getOldSmartCardSerialNo()))
				smartCardSwapDetails 	= new SmartCardSwapDetails(incoming);
		}
	}

	@Data
	@NoArgsConstructor
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	public static class SmartCardSwapDetails {

		private String oldSmartCardSerialNo;
		private String newSmartCardSerialNo;

		public SmartCardSwapDetails(SubmitSwapDTO incoming) {
			oldSmartCardSerialNo = incoming.getOldSmartCardSerialNo();
			newSmartCardSerialNo = incoming.getNewSmartCardSerialNo();
		}
	}

	@Data
	@NoArgsConstructor
	public static class DeliveryDetails {

		private String deliveryMethod;
		private String deliveryAddressId;

		public DeliveryDetails(SubmitSwapDTO incoming) {
			deliveryMethod 		= incoming.getDeliveryMethod();
			deliveryAddressId 	= incoming.getDeliveryAddressId();
		}

		public static DeliveryDetails CreateDeliveryDetails(SubmitSwapDTO incoming) {

			DeliveryDetails details = null;
			String method = incoming.getDeliveryMethod();
			if (StringUtils.isEmpty(method)) {
				log.debug("No DeliveryMethod specified, skipping this element");
			} else {
				log.debug("DeliveryMethod specified as {}", method);
				details = new DeliveryDetails(incoming);
			}

			return details;
		}
	}

	@Data
	@NoArgsConstructor
	public static class ReturnDetails {

		private String returnMethod;

		public ReturnDetails(SubmitSwapDTO incoming) {
			returnMethod = incoming.getReturnMethod();
		}
	}
}

package jacada.addFixedProxy.dto.layer7;

import org.springframework.util.StringUtils;

import jacada.addFixedProxy.dto.proxy.layer7.UpdateAppointmentDTO;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.AppointmentDetails.AppointmentDetailsVO;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.StaticData.StaticDataVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class Layer7UpdateAppointmentDTO extends Layer7AppointmentDTO {

	// not part of createAppointment interface
	private String oldAppointmentId;
	private String oldAvailableStartDate;
	private String oldAvailableFinishDate;
	private String oldAppointmentStatus;
	private String appCancellationDate;
	private String caseId;

	public Layer7UpdateAppointmentDTO(UpdateAppointmentDTO incoming) {

		super(incoming);
		oldAppointmentId = incoming.getOldAppointmentId();
	}

	@Override
	public void consume(StaticDataVO staticVO) {

		super.consume(staticVO);
		appCancellationDate = staticVO.getToday();
		oldAppointmentStatus = staticVO.getCxlStatus();
	}

	// this consumer is specific to the update class
	public void consume(AppointmentDetailsVO apptDetailsVO) {

		oldAvailableStartDate = apptDetailsVO.getAvailableStartDate();
		oldAvailableFinishDate = apptDetailsVO.getAvailableFinishDate();
		caseId = apptDetailsVO.getCaseId();

		setCaseTitle(apptDetailsVO.getCaseTitle());
		setCaseType(apptDetailsVO.getCaseType());

		setHouseFlatNumber(apptDetailsVO.getHouseFlatNumber());
		setHouseFlatExt(apptDetailsVO.getHouseFlatExt());
		setStreetName(apptDetailsVO.getStreetName());
		setPostCode(apptDetailsVO.getPostCode());
		setCity(apptDetailsVO.getCity());

		setContactName(apptDetailsVO.getContactName());
		setContactPhoneNumber(apptDetailsVO.getContactPhoneNumber());

		setCustomerName(apptDetailsVO.getCustomerName());
		setCusMobileNumber(apptDetailsVO.getCusMobileNumber());

		if(StringUtils.isEmpty(getAlternatePhoneNo())) {
			setAlternatePhoneNo(apptDetailsVO.getAlternatePhoneNo());
		}

		setProducts(apptDetailsVO.getProducts());
		setInstalledProducts(apptDetailsVO.getInstalledProducts());
		setCpeInfo(apptDetailsVO.getCpeInfo());

		setWorkorderType(apptDetailsVO.getWorkorderType());
		setWorkorderPriority(apptDetailsVO.getWorkorderPriority());
		setHub(apptDetailsVO.getHub());
		setNode(apptDetailsVO.getNode());
		setGroupAmplifier(apptDetailsVO.getGroupAmplifier());
		setEndAmplififier(apptDetailsVO.getEndAmplifier());
		setMultitap(apptDetailsVO.getMultitap());
		setTapPosition(apptDetailsVO.getTapPosition());
		setCMTS(apptDetailsVO.getCMTS());
		setWebcard(apptDetailsVO.getWebcard());
	}
}
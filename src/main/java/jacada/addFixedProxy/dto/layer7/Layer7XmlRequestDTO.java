package jacada.addFixedProxy.dto.layer7;

// this is a marker interface, used to provide an Amdocs base generic type for XML requests
public interface Layer7XmlRequestDTO extends Layer7RequestDTO {
	String body();
}
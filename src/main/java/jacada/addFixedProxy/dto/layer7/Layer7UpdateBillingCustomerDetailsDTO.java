package jacada.addFixedProxy.dto.layer7;

import com.fasterxml.jackson.annotation.JsonProperty;

import jacada.addFixedProxy.dto.proxy.layer7.UpdateBillingCustomerDetailsDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Layer7UpdateBillingCustomerDetailsDTO implements Layer7RequestDTO {

	private String bc_id;
	private String contact_id;
	private boolean update_bc_address = false;
	private boolean update_fa_address = false;
	private boolean update_ba_address = false;
	private VatDetails vat_details;

	public Layer7UpdateBillingCustomerDetailsDTO(UpdateBillingCustomerDetailsDTO incoming) {

		bc_id = incoming.getBillingCustomerId();
		contact_id = incoming.getContactId();

		jacada.addFixedProxy.dto.proxy.layer7.UpdateBillingCustomerDetailsDTO.VatDetails incomingVD = incoming.getVatDetails();
		vat_details = new VatDetails();
		vat_details.setVat_country(incomingVD.getVatCountry());
		vat_details.setVatId_number(incomingVD.getVatIdNumber());
		vat_details.setVat_valid(incomingVD.isVatValid());
	}

	@Data
	@NoArgsConstructor
	public class VatDetails {

		private String vat_country;
		private String vatId_number;
	  @JsonProperty("is_vat_valid")	private boolean vat_valid;
	}
}
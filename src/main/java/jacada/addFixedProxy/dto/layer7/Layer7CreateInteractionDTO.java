package jacada.addFixedProxy.dto.layer7;

import jacada.addFixedProxy.dto.proxy.layer7.CreateInteractionDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Layer7CreateInteractionDTO implements Layer7RequestDTO {

	private String contact_id;
	private String interaction_title;
	private String notes;
	private String reason_1;
	private String reason_2;
	private String direction;
	private String linked_entity_id;
	private String linked_entity_type;
	private String channel;
	private String media;
	private String result;
	private String interaction_type;

	public Layer7CreateInteractionDTO(CreateInteractionDTO incoming) {

		contact_id 			= incoming.getContactId();
		interaction_title 	= incoming.getInteractionTitle();
		reason_1 			= incoming.getReason1();
		reason_2 			= incoming.getReason2();
		notes 				= incoming.getNotes();
		direction 			= incoming.getDirection();
		linked_entity_id 	= incoming.getLinkedEntityId();
		linked_entity_type 	= incoming.getLinkedEntityType();
		channel 			= incoming.getChannel();
		media 				= incoming.getMedia();
		interaction_type 	= incoming.getInteractionType();
		result 				= incoming.getCi_result();
	}
}
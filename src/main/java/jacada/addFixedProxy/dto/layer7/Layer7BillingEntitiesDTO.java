package jacada.addFixedProxy.dto.layer7;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Layer7BillingEntitiesDTO implements Layer7RequestDTO {

	private boolean isfabalreq = false;
}
package jacada.addFixedProxy.dto.layer7;

import com.fasterxml.jackson.annotation.JsonProperty;

import jacada.addFixedProxy.dto.proxy.layer7.NetworkPathDetailsDTO;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class NetworkPathDetailsBody {

	@Getter(onMethod = @__({ @JsonProperty("Address") }))
	private NetworkPhysicalAddress Address = null;

	public NetworkPathDetailsBody(NetworkPathDetailsDTO dto) {
		Address  = new NetworkPhysicalAddress(dto);
	}

	@Data
	@NoArgsConstructor
	public static class NetworkPhysicalAddress {

		@Getter(onMethod = @__({ @JsonProperty("HouseNumberExtension") }))
		private String HouseNumberExtension;

		@Getter(onMethod = @__({ @JsonProperty("Room") }))
		private String Room;

		@Getter(onMethod = @__({ @JsonProperty("HouseNumber") }))
		private String HouseNumber;

		@Getter(onMethod = @__({ @JsonProperty("HouseLetter") }))
		private String HouseLetter;

		@Getter(onMethod = @__({ @JsonProperty("PostalCode") }))
		private String PostalCode;

		public NetworkPhysicalAddress(NetworkPathDetailsDTO dto) {

			HouseNumberExtension = "";//dto.getHouseNumberExtension();
			Room = "";//dto.getRoom();
			HouseNumber = dto.getHouseNumber();
			HouseLetter = dto.getHouseLetter();
			PostalCode = dto.getPostCode();
		}
	}
}
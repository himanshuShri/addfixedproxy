package jacada.addFixedProxy.dto.layer7;

import java.util.Arrays;
import java.util.List;

import jacada.addFixedProxy.dto.proxy.layer7.GetRequestDTO;
import jacada.addFixedProxy.dto.proxy.layer7.GetRequestDTO.ID_TYPE;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Layer7AssignedProductDetailsDTO implements Layer7RequestDTO {

	private List<String> assignedProductIDList;

	public Layer7AssignedProductDetailsDTO(GetRequestDTO incoming) {

		assignedProductIDList = Arrays.asList(incoming.id(ID_TYPE.ASSIGNED_PRODUCT_ID).split(","));
	}
}
package jacada.addFixedProxy.dto.layer7;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jacada.addFixedProxy.dto.layer7.Layer7SharedTypes.CPEInfo;
import jacada.addFixedProxy.dto.layer7.Layer7SharedTypes.CPEInfoWrapper;
import jacada.addFixedProxy.dto.layer7.Layer7SharedTypes.InstalledProductWrapper;
import jacada.addFixedProxy.dto.layer7.Layer7SharedTypes.Notification;
import jacada.addFixedProxy.dto.layer7.Layer7SharedTypes.Product;
import jacada.addFixedProxy.dto.layer7.Layer7SharedTypes.ProductWrapper;
import jacada.addFixedProxy.dto.proxy.layer7.AppointmentDTO;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.AssignedProductDetails.AssignedProductDetailsVO;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.AssignedProducts.AssignedProductsVO;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.CustomerProfile.CustomerProfileVO;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.NetworkPathDetails.NetworkPathDetailsVO;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.StaticData.StaticDataVO;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
@NoArgsConstructor
public abstract class Layer7AppointmentDTO implements Layer7RequestDTO {

	private String appointmentId;
	private String businessTransactionId;
	private String availableStartDate;
	private String availableFinishDate;
	private String caseTitle;
	private String caseCategory;
	private String caseType;
	private String caseSubType;
	private String houseFlatNumber;
	private String houseFlatExt;
	private String streetName;
	private String postCode;
	private String city;
	private String contactId;
	private String contactName;
	private String contactPhoneNumber;
	private String customerName;
	private String cusMobileNumber;
	private String alternatePhoneNo;
	private ProductWrapper products;
	private InstalledProductWrapper installedProducts = new InstalledProductWrapper();
	private CPEInfoWrapper cpeInfo = new CPEInfoWrapper();
	private String workorderType;
	private String workorderPriority;
	private String customerId;
	private String note;
	private String hub;
	private String node;
	private String groupAmplifier;
	private String endAmplififier;
	private String multitap;
	private String tapPosition;
	private String CMTS;
	private String webcard;
	private String agentID;
	private String channel;
	private List<Notification> notification = new ArrayList<>();

	// this is used to shadow the customerId and won't be sent ultimately
	@JsonIgnore private String accountId;

	public Layer7AppointmentDTO(AppointmentDTO incoming) {

		caseCategory 			= incoming.getCaseCategory();
		caseType				= incoming.getCaseType();
		caseSubType 			= incoming.getCaseSubType();
		workorderType 			= incoming.getCaseType();
		workorderPriority		= incoming.getWorkOrderPriority();

		businessTransactionId = incoming.getBusinessTransactionId();

		appointmentId 			= incoming.getAppointmentId();
		availableStartDate 		= incoming.getAvailableStartDate();
		availableFinishDate 	= incoming.getAvailableFinishDate();

		alternatePhoneNo 		= incoming.getAlternativePhoneNo();
		customerId 				= incoming.getCustomerId();
		agentID 				= incoming.getAgentId();
		note 					= incoming.getNote();

		// finally add the default notification
		notification.add(Notification.DEFAULT());
	}

	public void consume(StaticDataVO staticVO) {

		channel = staticVO.getChannel();
	}

	public void consume(CustomerProfileVO custProfVO) {

		houseFlatNumber 		= custProfVO.getHouseNumber();
		houseFlatExt 			= custProfVO.getFlat();
		streetName 				= custProfVO.getStreet();
		postCode 				= custProfVO.getPostCode();
		city 					= custProfVO.getCity();

		contactId 				= custProfVO.getContactId();
		contactName 			= custProfVO.getFullName();
		contactPhoneNumber 		= custProfVO.getPhone();
		cusMobileNumber 		= custProfVO.getMobile();
		customerName 			= custProfVO.getFullName();

		accountId 				= custProfVO.getAccountId();
	}

	public void consume(AssignedProductsVO assProdVO) {

		// both arrays will be the same size, although data may be null/missing
		int len = assProdVO.getDates().size();

		List<Product> installedProds = installedProducts.getInstalledProduct();
		for(int i=0; i<len; i++) {
			installedProds.add(new Product(assProdVO.getNames().get(i), assProdVO.getDates().get(i)));
		}
	}

	public void consume(AssignedProductDetailsVO assProdDetVO) {

		// these could be different array sizes
		int macsLen 			= assProdDetVO.getTvMacs().size();
		int brandsLen 			= assProdDetVO.getTvBrands().size();
		int loopLen 			= macsLen;
		List<CPEInfo> cpeWrapper = cpeInfo.getCpeInfo();

		if(macsLen != brandsLen) {
			log.error("Inconsistent CPE Info data found, including all in the request");
			for (String mac   : assProdDetVO.getTvMacs())   log.error("macAddress - {}", mac);
			for (String brand : assProdDetVO.getTvBrands()) log.error("brand - {}", brand);
			loopLen = brandsLen > macsLen ? brandsLen : macsLen;
		}

		for(int i=0; i<loopLen; i++) {
			String mac 	 = i >= macsLen   ? "" : assProdDetVO.getTvMacs().get(i);
			String brand = i >= brandsLen ? "" : assProdDetVO.getTvBrands().get(i);
			cpeWrapper.add(new CPEInfo(brand, mac));
		}
	}

	public void consume(NetworkPathDetailsVO networkPathVO) {

		hub 			= networkPathVO.getHub();
		node 			= networkPathVO.getNode();
		groupAmplifier 	= networkPathVO.getGrpAmplifier();
		endAmplififier 	= networkPathVO.getEndAmplifier();
		multitap 		= networkPathVO.getMultiTap();
		tapPosition 	= networkPathVO.getTapPosn();
	}
}
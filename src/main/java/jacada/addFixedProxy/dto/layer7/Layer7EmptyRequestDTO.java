package jacada.addFixedProxy.dto.layer7;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Layer7EmptyRequestDTO implements Layer7RequestDTO {

	// without this, jackson won't create a body at all
	// which will cause the exchange to fail
	private String dummy;
}

package jacada.addFixedProxy.dto.layer7;

import jacada.addFixedProxy.dto.layer7.Layer7SharedTypes.ProductWrapper;
import jacada.addFixedProxy.dto.proxy.layer7.CreateAppointmentDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class Layer7CreateAppointmentDTO extends Layer7AppointmentDTO {

	public Layer7CreateAppointmentDTO(CreateAppointmentDTO incoming) {
		super(incoming);

		setAlternatePhoneNo(incoming.getAlternativePhoneNo());
		setProducts(new ProductWrapper(incoming.getProducts()));
	}
}
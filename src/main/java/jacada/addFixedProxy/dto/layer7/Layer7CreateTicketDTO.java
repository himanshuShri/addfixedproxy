package jacada.addFixedProxy.dto.layer7;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;

import jacada.addFixedProxy.dto.layer7.Layer7SharedTypes.Notification;
import jacada.addFixedProxy.dto.proxy.layer7.CreateTicketDTO;
import jacada.addFixedProxy.dto.proxy.layer7.CreateTicketPhase2DTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Layer7CreateTicketDTO implements Layer7RequestDTO {

	private String channel;
	private String account_id;
	private String ticket_type_1;
	private String ticket_type_2;
	private String ticket_type_3;
	private String priority;
	private String title;
	private String note;
	private boolean create_interaction_flag;
	private String contact_id;
	private Notification notification;
	private List<FlexibleAttribute> flexible_attributes = new ArrayList<>();

	public Layer7CreateTicketDTO(CreateTicketPhase2DTO incoming) {

		channel 				= incoming.getChannel();
		account_id 				= incoming.getAccountId();
		ticket_type_1 			= incoming.getTicketType1();
		ticket_type_2 			= incoming.getTicketType2();
		ticket_type_3 			= incoming.getTicketType3();
		priority 				= incoming.getPriority();
		title 					= incoming.getTitle();
		note 					= incoming.getNote();
		create_interaction_flag = incoming.isCreateInteractionFlag();
		contact_id 				= incoming.getContactId();

		notification = Notification.DEFAULT();

		String flexAttrStr = incoming.getFlexibleAttributes();
		if(StringUtils.hasText(flexAttrStr)) {
			String[] attrs = flexAttrStr.split("--,--");
			for(String flex: attrs) {
				String[] kv = flex.split("__:__", 2);
				flexible_attributes.add(new FlexibleAttribute(kv[0], kv[1]));
			}
		}
	}
	public Layer7CreateTicketDTO(CreateTicketDTO incoming) {

		channel 				= incoming.getChannel();
		account_id 				= incoming.getAccountId();
		ticket_type_1 			= incoming.getTicketType1();
		ticket_type_2 			= incoming.getTicketType2();
		ticket_type_3 			= incoming.getTicketType3();
		priority 				= incoming.getPriority();
		title 					= incoming.getTitle();
		note 					= incoming.getNote();
		create_interaction_flag = incoming.isCreateInteractionFlag();
		contact_id 				= incoming.getContactId();

		notification = new Notification(incoming.getNotification());

		List<jacada.addFixedProxy.dto.proxy.layer7.CreateTicketDTO.FlexibleAttribute> flexs = incoming.getFlexibleAttributes();
		for (jacada.addFixedProxy.dto.proxy.layer7.CreateTicketDTO.FlexibleAttribute flex : flexs) {
			flexible_attributes.add(new FlexibleAttribute(flex));
		}
	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class FlexibleAttribute {

		private String key;
		private String value;

		public FlexibleAttribute(jacada.addFixedProxy.dto.proxy.layer7.CreateTicketDTO.FlexibleAttribute flex) {

			key 	= flex.getKey();
			value 	= flex.getValue();
		}
	}
}
package jacada.addFixedProxy.dto.layer7;

import jacada.addFixedProxy.dto.RequestDTO;

// this is a marker interface, used to provide an Amdocs base generic type for JSON requests
public interface Layer7RequestDTO extends RequestDTO {}
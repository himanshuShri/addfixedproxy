package jacada.addFixedProxy.dto.layer7;

import java.util.ArrayList;
import java.util.List;

import jacada.addFixedProxy.dto.layer7.Layer7SharedTypes.AddressDetails;
import jacada.addFixedProxy.dto.proxy.layer7.UpdateContactDTO;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
@NoArgsConstructor
public class Layer7UpdateContactDTO implements Layer7RequestDTO {

	private String contact_id;
	private ContactDetails contact_details = new ContactDetails();
	private AddressDetails address_details;

	public Layer7UpdateContactDTO(UpdateContactDTO incoming) {

		address_details = new AddressDetails();
		contact_id = incoming.getContactId();

		address_details.setCountry(incoming.getCountry());
		address_details.setStreetname(incoming.getStreet());
		address_details.setCity(incoming.getCity());
		address_details.setZip_code(incoming.getPostCode());
		address_details.setHousenumber(incoming.getHouseNumber());

		contact_details.first_name 		= incoming.getFirstName();
		contact_details.last_name 		= incoming.getLastName();
		contact_details.middle_name 	= incoming.getMiddleName();
		contact_details.email_address 	= incoming.getEmail();
		contact_details.salutation 		= incoming.getTitle();
		contact_details.primary_phone 	= incoming.getPrivatePhone();

		contact_details.date_of_birth 	= incoming.getDob();

		// we check for zero here as we should only be updating records and never creating them
		// the value is not checked and passing a blank will trigger a delete on the back end
		int privatePhoneId = incoming.getPrivatePhoneAddressId();
		if (privatePhoneId != 0) {
			log.debug("Adding private phone to request");
			contact_details.alternative_contact_methods.add(new AlternativeContactMethod(
						ContactDetails.PRIVATEPHONE, incoming.getPrivatePhone(), privatePhoneId)
			);
		}

		int officePhoneId = incoming.getOfficePhoneAddressId();
		if (officePhoneId != 0) {
			log.debug("Adding work phone to request");
			contact_details.alternative_contact_methods.add(new AlternativeContactMethod(
						ContactDetails.WORKPHONE, incoming.getOfficePhone(), officePhoneId)
			);
		}

		int mobilePhoneId = incoming.getMobilePhoneAddressId();
		if (mobilePhoneId != 0) {
			log.debug("Adding mobile phone to request");
			contact_details.alternative_contact_methods.add(new AlternativeContactMethod(
						ContactDetails.MOBILE, incoming.getMobilePhone(), mobilePhoneId)
			);
		}

		int twitterId = incoming.getTwitterAddressId();
		if (twitterId != 0) {
			log.debug("Adding twitter to request");
			contact_details.alternative_contact_methods.add(new AlternativeContactMethod(
						ContactDetails.TWITTER, incoming.getTwitter(), twitterId)
			);
		}

		int facebookId = incoming.getFacebookAddressId();
		if (facebookId != 0) {
			log.debug("Adding facebook to request");
			contact_details.alternative_contact_methods.add(new AlternativeContactMethod(
						ContactDetails.FACEBOOK, incoming.getFacebook(), facebookId)
			);
		}

		int linkedInId = incoming.getLinkedInAddressId();
		if (linkedInId != 0) {
			log.debug("Adding linkedIn to request");
			contact_details.alternative_contact_methods.add(new AlternativeContactMethod(
						ContactDetails.LINKEDIN, incoming.getLinkedIn(), linkedInId)
			);
		}
	}

	@Data
	public static class ContactDetails {

		public static String PRIVATEPHONE 	= "T";
		public static String WORKPHONE 		= "Work Phone";
		public static String MOBILE			= "M";
		public static String FACEBOOK 		= "Facebook";
		public static String TWITTER 		= "Twitter";
		public static String LINKEDIN 		= "LinkedIn";

		private String first_name;
		private String last_name;
		private String email_address;
		private String middle_name;
		private String salutation;
		private String primary_phone;
		private String date_of_birth;
		private List<AlternativeContactMethod> alternative_contact_methods;

		public ContactDetails() {
			alternative_contact_methods = new ArrayList<>();
		}
	}

	@Data
	@NoArgsConstructor
	public static class AlternativeContactMethod {

		private String type;
		private String value;
		private String addressId;

		// negative values indicate create, so add a blank addressId to the request
		public AlternativeContactMethod(String t, String v, int a) {
			type 	= t;
			value = v;
			addressId = a > 0 ? String.valueOf(a) : "";
		}
	}
}
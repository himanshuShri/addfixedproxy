package jacada.addFixedProxy.dto.adt;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class ADTPrivilegedResponseDTO extends ADTResponseDTO {

	private boolean privileged;
}
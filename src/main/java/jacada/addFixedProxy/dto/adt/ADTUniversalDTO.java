package jacada.addFixedProxy.dto.adt;

import jacada.addFixedProxy.dto.RequestDTO;
import jacada.addFixedProxy.dto.proxy.adt.ADTProxySharedTypes.Parameter;
import jacada.addFixedProxy.dto.proxy.adt.ProxyADTRequestDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ADTUniversalDTO implements RequestDTO {

	private String 			importantAction;
	private Parameter[] parameters;

	public ADTUniversalDTO(ProxyADTRequestDTO incoming) {

		importantAction = incoming.getImportantAction();
		parameters		= incoming.getParameters();
	}
}
package jacada.addFixedProxy.dto.adt;

import lombok.Data;

@Data
public class ADTResponseDTO {

	private boolean status;
	private String error;
}
package jacada.addFixedProxy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import jacada.addFixedProxy.properties.ADTProperties;
import jacada.addFixedProxy.properties.Layer7Properties;
import lombok.extern.slf4j.Slf4j;

/**
 * Create a series of VendorResponseMapper beans, 1 per service vendor.
 * Spring can't do all the work for us here because it can't uniquely
 * identify the ServiceProperties instance it needs for each bean.
 * @author ndoan
 **/
@Slf4j
@Configuration
public class ResponseMapperFactory {

	@Autowired Layer7Properties l7Props;
	@Autowired ADTProperties adtProps;

	@Bean
	public VendorResponseMapper<Layer7Properties> layer7RM() throws Exception {
		log.debug("Creating layer7 response mapper bean");
		return new VendorResponseMapper<>(l7Props);
	}

	@Bean
	public VendorResponseMapper<ADTProperties> adtRM() throws Exception {
		log.debug("Creating adt response mapper bean");
		return new VendorResponseMapper<>(adtProps);
	}
}
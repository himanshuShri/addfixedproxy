package jacada.addFixedProxy.service.vanDyck;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import jacada.addFixedProxy.Utils;
import jacada.addFixedProxy.dto.proxy.adt.ProxyADTRequestDTO;
import jacada.addFixedProxy.properties.VanDyckProperties;
import jacada.addFixedProxy.properties.VanDyckProperties.Case;
import jacada.addFixedProxy.service.adt.dataFactory.Privilege;
import jacada.addFixedProxy.service.adt.dataFactory.Privilege.PrivilegeVO;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class VanDyckService {

	@Autowired Privilege 			privilegeFactory;
	@Autowired VanDyckProperties	properties;
	@Autowired ObjectMapper			mapper;

	// tokenise by ':'
	private Pattern rulePattern 		= Pattern.compile("([^:]+):(.+)");
	private String PROPERTY_RULE 		= "PROPERTY";
	private String PERMISSION_RULE 		= "PERMISSION";
	private String NOT_PERMISSION_RULE 	= "NOT_PERMISSION";

	public String supportedCases() throws JsonProcessingException {

		log.debug("Handling supportedCases");
		return mapper.writeValueAsString(properties.getSupportedCases());
	}

	public String createCaseDynamics(
			String caseType,		String customerId,
			String ADTSessionId,	String sessionId) throws Exception {

		log.debug("Handling createCaseDynamics");
		String tin 			= String.format("/vanDyck/%s.json", caseType);
		String structure 	= Utils.RESOURCE_STRING(tin);
		log.info("Found file [{}]", tin);

		DocumentContext structureDoc 			= JsonPath.parse(structure);
		List<Map<String, String>> completions 	= structureDoc.read("$.completions");
		Case caseInstance 						= properties.findCase(caseType);
		Map<String, String> propertiesMap 		= caseInstance.getProperties();

		for (Map<String, String> completion : completions) {

			String field	= completion.get("field");
			String rule 	= completion.get("rule");
			log.info("completing document [{}], setting [{}] to the value of [{}]", tin, field, rule);

			Matcher m = rulePattern.matcher(rule);
			if (m.matches()) {
				String ruleType = m.group(1);
				String ruleArg	= m.group(2);
				log.info("completing target [{}] with [{}] and [{}]", field, ruleType, ruleArg);

				if (PERMISSION_RULE.equals(ruleType) || NOT_PERMISSION_RULE.equals(ruleType)) {

					boolean isPrivileged = false;
					if (PERMISSION_RULE.equals(ruleType)) {
						isPrivileged = checkPermission(ruleArg, customerId, ADTSessionId, sessionId);
					} else if (NOT_PERMISSION_RULE.equals(ruleType)) {
						isPrivileged = !checkPermission(ruleArg, customerId, ADTSessionId, sessionId);
					}
					structureDoc.set(field, isPrivileged);

				} else if(PROPERTY_RULE.equals(ruleType)) {

					String propertyVal = propertiesMap.get(ruleArg);
					log.debug("properties map: {} -> {}", ruleArg, propertyVal);
					structureDoc.set(field, propertyVal);

				} else {

					log.error("Ignoring unrecognised rule [{}]", ruleType);

				}
			} else {
				log.error("unable to parse rule [{}]", rule);
			}
		}

		return structureDoc.jsonString();
	}

	private boolean checkPermission(String permission, String customerId, String ADTSessionId, String sessionId) {

		ProxyADTRequestDTO udDTO = new ProxyADTRequestDTO();
		udDTO.setCustomerId(customerId);
		udDTO.setSessionId(sessionId);
		udDTO.setAdtSessionId(ADTSessionId);
		udDTO.setPrivilege(permission);

		PrivilegeVO vo = privilegeFactory.callForVO(udDTO);
		boolean isPrivileged = vo.isGranted();

		log.info("Privilege returned as [{}]", isPrivileged);
		return isPrivileged;
	}
}
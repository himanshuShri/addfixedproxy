package jacada.addFixedProxy.service;

import java.util.Map;

/**
 * Any properties class that describes a service must expose
 * a method returning the output paths for each request type.
 * @author ndoan
 *
 */
public interface ServiceProperties {

	public Map<String, String> outputPaths(String requestType);
}
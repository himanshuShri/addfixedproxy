package jacada.addFixedProxy.service.admin;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * This is a single use class, create a new one to parse each file.
 *
 * @author ndoan
 *
 */
@Slf4j
public class ColumnFilter {

	private List<Integer> excludedCols = new ArrayList<Integer>();

	@Getter
	private List<List<String>> acceptedData = new ArrayList<List<String>>();
	@Getter
	private List<String> acceptedHeaders = new ArrayList<String>();

	public void acceptHeaders(List<String> hdrs) {
		acceptedHeaders =  hdrs;
	}

	public void acceptRow(List<String> cols) {

		List<String> data = new ArrayList<String>();
		for (int i = 0; i < cols.size(); i++) {
			String col = cols.get(i);
			log.trace("Column: {} -> {}", i, col);

			if (!excludedCols.contains(i)) {
				data.add(col);
			} else {
				log.warn("unknown column {} has been skipped", col);
			}
		}

		acceptedData.add(data);
	}
}
package jacada.addFixedProxy.service.admin;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import jacada.addFixedProxy.mongo.MongoInstance.COLLECTIONS;
import jacada.addFixedProxy.properties.AdminProperties;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * Uses a ColumnFilter instance to perform column level checking and then
 * composes the filtered output into a list of maps. This approach means looping
 * over the file rows twice but it gives a better separation than doing
 * filtering and composing simultaneously and the application is not time
 * critical.
 */
@Slf4j
@Component
@Setter
public class DelimitedFileParser {

	@Autowired
	private AdminProperties properties;
	private CSVParser parser = null;

	@PostConstruct
	public void init() {
		parser = new CSVParserBuilder().withSeparator(properties.getIncomingFileDelimeter()).build();
	}

	public List<Document> parse(String fileType, InputStream stream) throws IOException {

		InputStreamReader fr = new InputStreamReader(stream, StandardCharsets.UTF_8);
		final CSVReader reader = new CSVReaderBuilder(fr).withCSVParser(parser).build();

		ColumnFilter columnFilter = new ColumnFilter();
		Iterator<String[]> it = reader.iterator();

		if (COLLECTIONS.FLEXIBLE_ATTRIBUTES.name().equalsIgnoreCase(fileType)) {
			createFlexAttributeRecords(it, columnFilter);
		} else {
			createOtherRecords(it, columnFilter);
		}
		reader.close();

		List<Document> docs = new ArrayList<Document>();
		docs = createDocument(columnFilter);

		return docs;
	}

	private void createFlexAttributeRecords(Iterator<String[]> it, ColumnFilter columnFilter) {
		String typeValue = "", defValue = "";
		int typeColIndex = 0, defValColIndex = 0;
		long headerCount = 0, recordCount = 0;
		boolean validFile = true;
		boolean first = true;
		while (it.hasNext()) {
			List<String> cols = new ArrayList<String>();
			Collections.addAll(cols, it.next());
			if (first) {
				first = false;
				if (cols.contains("Type") && cols.contains("Default Value")) {
					headerCount = cols.size();
					typeColIndex = cols.indexOf("Type");
					defValColIndex = cols.indexOf("Default Value");
					columnFilter.acceptHeaders(cols);
				} else {
					log.warn("Header {} does not contain either Type or Default Value", cols);
					validFile = false;
					first = false;
					break;
				}

			} else if (validFile) {
				recordCount = cols.size();
				if (headerCount != recordCount) {
					log.warn("Record {} does not match the header count", cols);
				} else {
					typeValue = cols.get(typeColIndex);
					defValue = cols.get(defValColIndex);
					boolean result = true;
					if (!StringUtils.isEmpty(defValue.trim())) {
						try {
							checkDataType(typeValue, defValue);
						} catch (Exception e) {
							result = false;
							log.debug("Default Value {} does not match the given Type {} ", defValue, typeValue);
						}
					}
					if (result) {
						columnFilter.acceptRow(cols);
					}
				}
			}
		}
	}

	private void checkDataType(String typeValue, String defValColIndex) throws ParseException, NumberFormatException {
		switch (typeValue.toUpperCase()) {
		case "STRING":
			break;
		case "DATETIME":
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
			df.parse(defValColIndex);
			break;

		case "LONG":
			Long.parseLong(defValColIndex);
			break;

		case "DOUBLE":
			Double.parseDouble(defValColIndex);
			break;

		default:
			break;
		}

	}

	private void createOtherRecords(Iterator<String[]> it, ColumnFilter columnFilter) {

		boolean first = true;
		long headerCount = 0, recordCount = 0;
		while (it.hasNext()) {
			List<String> cols = new ArrayList<String>();
			Collections.addAll(cols, it.next());
			if (first) {
				first = false;
				headerCount = cols.size();
				columnFilter.acceptHeaders(cols);
			} else {
				recordCount = cols.size();
				if (headerCount != recordCount) {
					log.warn("Record {} does not match the header count", cols);
				} else {
					columnFilter.acceptRow(cols);
				}
			}
		}
	}

	private List<Document> createDocument(ColumnFilter columnFilter) {

		// build the filtered output into documents.
		List<String> headers = columnFilter.getAcceptedHeaders();
		List<List<String>> data = columnFilter.getAcceptedData();
		List<Document> docs = new ArrayList<Document>();
		if (headers.size() > 0) {
			for (List<String> row : data) {
				Document doc = new Document();
				Iterator<String> hdrIt = headers.iterator();
				for (String val : row) {
					doc.put(hdrIt.next(), val);
				}
				log.trace("completed document {}", doc);
				docs.add(doc);
			}

		} else {
			docs = null;
		}
		return docs;

	}

}
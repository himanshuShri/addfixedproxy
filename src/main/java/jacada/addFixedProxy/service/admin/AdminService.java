package jacada.addFixedProxy.service.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import jacada.addFixedProxy.mongo.MongoInstance;
import jacada.addFixedProxy.mongo.MongoInstance.COLLECTIONS;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AdminService {

	@Autowired
	DelimitedFileParser parser;
	@Autowired
	MongoInstance mongo;

	private final String ALL = "***";

	public void upload(COLLECTIONS collection, MultipartFile file) throws IOException {

		log.debug("Handling upload - {}", collection);
		if (file != null) {
			log.debug("{} file: name: {}, type: {}, size: {}B", collection, file.getOriginalFilename(),
					file.getContentType(), file.getSize());

			var data = parser.parse(collection.name(), file.getInputStream());

			if (data != null) {
				var deleted = mongo.deleteAll(collection);
				log.debug("Removed {} existing entries from {}", deleted, collection);

				mongo.insertAll(collection, data);
			} else {
				log.debug("No valid record to insert");
			}

		} else
			log.debug("null file received");
	}

	public void insert(String collection, String json) throws Exception {

		log.debug("Handling insert {}");
		var c = COLLECTIONS.valueOf(collection);
		mongo.insertOne(c, json);
	}

	public void update(String collection, String id, String json) throws Exception {

		log.debug("Handling update {}");
		var c = COLLECTIONS.valueOf(collection);
		mongo.updateOne(c, id, json);
	}

	/**
	 * Read, admitting the special string "***" signifying, select all
	 *
	 * @param collection
	 * @param tt1
	 * @param tt2
	 * @param tt3
	 * @return
	 */
	public List<Document> read(String collection, String tt1, String tt2, String tt3, String parent) {

		Document filter = new Document();

		if(StringUtils.hasText(tt1) 	&& !ALL.equals(tt1)) 	filter.put("ticketType1",	tt1);
		if(StringUtils.hasText(tt2) 	&& !ALL.equals(tt2)) 	filter.put("ticketType2",	tt2);
		if(StringUtils.hasText(tt3) 	&& !ALL.equals(tt3)) 	filter.put("ticketType3",	tt3);
		if(StringUtils.hasText(parent)	&& !ALL.equals(parent)) filter.put("parent",		parent);

		return _read(COLLECTIONS.valueOf(collection), filter);
	}

	/**
	 * Read the displayMappings view, admitting the special string "***" signifying, select all
	 *
	 * @param d1
	 * @param d2
	 * @param d3
	 * @return
	 */
	public Map<String, Object> findForDisplay(String d1, String d2, String d3) {

		Document filter = new Document();

		if(StringUtils.hasText(d1) && !ALL.equals(d1)) filter.put("display_ticketType1",	d1);
		if(StringUtils.hasText(d2) && !ALL.equals(d2)) filter.put("display_ticketType2",	d2);
		if(StringUtils.hasText(d3) && !ALL.equals(d3)) filter.put("display_ticketType3",	d3);

		List<Document> docs = _read(COLLECTIONS.DISPLAY_MAPPING, filter);

		Map<String, Object> output = new HashMap<>();
		output.put("data", docs);
		output.put("length", docs == null? 0: docs.size());

		return output;
	}

	private List<Document> _read(COLLECTIONS c, Document filter) {

		log.debug("Handling collection read - {} - {}", c, filter);
		return mongo.find(c, filter).into(new ArrayList<>());
	}

	public long delete(String collection, String id) {

		log.debug("Handling delete - {}.{}", collection, id);
		var c = COLLECTIONS.valueOf(collection);
		return mongo.deleteOne(c, id);
	}
}
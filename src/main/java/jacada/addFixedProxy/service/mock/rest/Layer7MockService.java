package jacada.addFixedProxy.service.mock.rest;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import com.fasterxml.jackson.databind.ObjectMapper;

import jacada.addFixedProxy.Utils;
import jacada.addFixedProxy.dto.layer7.ISWRequestDTO;
import jacada.addFixedProxy.dto.layer7.Layer7AssignedProductDetailsDTO;
import jacada.addFixedProxy.dto.layer7.Layer7AssignedProductsDTO;
import jacada.addFixedProxy.dto.layer7.Layer7BillingArrangementDetailsDTO;
import jacada.addFixedProxy.dto.layer7.Layer7CreateAppointmentDTO;
import jacada.addFixedProxy.dto.layer7.Layer7CreateInteractionDTO;
import jacada.addFixedProxy.dto.layer7.Layer7CreateTicketDTO;
import jacada.addFixedProxy.dto.layer7.Layer7ReserveTimeslotsDTO;
import jacada.addFixedProxy.dto.layer7.Layer7ResponseDTO;
import jacada.addFixedProxy.dto.layer7.Layer7SubmitSwapDTO;
import jacada.addFixedProxy.dto.layer7.Layer7UpdateAccountDetailsDTO;
import jacada.addFixedProxy.dto.layer7.Layer7UpdateAppointmentDTO;
import jacada.addFixedProxy.dto.layer7.Layer7UpdateBillingArrangementDetailsDTO;
import jacada.addFixedProxy.dto.layer7.Layer7UpdateBillingCustomerDetailsDTO;
import jacada.addFixedProxy.dto.layer7.Layer7UpdateContactDTO;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class Layer7MockService {

	@Autowired private ObjectMapper mapper;

	public String contactDetails(String contactId) throws Exception {
					return tinnedJSONResponse("Amdocs/getContactDetails");
	}

	public String customerProfile(String customerId) throws Exception {
		return tinnedJSONResponse("Amdocs/getCustomerProfile");
	}

	public String billingEntities(String customerId) throws Exception {
		return tinnedJSONResponse("Amdocs/getBillingEntities");
	}

	public String billingArrangementDetails(Layer7BillingArrangementDetailsDTO dto) throws Exception {
		return tinnedJSONResponse("Amdocs/getBillingArrangementDetails");
	}

	public String updateBillingArrangementDetails(Layer7UpdateBillingArrangementDetailsDTO dto) throws Exception {
		return tinnedJSONResponse("Amdocs/updateBillingArrangementDetails");
	}

	public String updateBillingCustomerDetails(Layer7UpdateBillingCustomerDetailsDTO dto) throws Exception {
		return tinnedJSONResponse("Amdocs/updateBillingCustomerDetails");
	}

	public String updateAccountDetails(Layer7UpdateAccountDetailsDTO dto, String accountId) throws Exception {
		return tinnedJSONResponse("Amdocs/updateAccountDetails");
	}

	public String updateContact(Layer7UpdateContactDTO dto, String contactId) throws Exception {
		return tinnedJSONResponse("Amdocs/updateContact");
	}

	public String createInteraction(Layer7CreateInteractionDTO dto) throws Exception {
		return tinnedJSONResponse("Amdocs/createInteraction");
	}

	public String createTicket(Layer7CreateTicketDTO dto) throws Exception {
		return tinnedJSONResponse("Amdocs/createTicket");
	}

	public String appointment(String appointmentId) throws Exception {
		return tinnedJSONResponse("Tibco/appointment");
	}

	public String createAppointment(Layer7CreateAppointmentDTO dto) throws Exception {
		return tinnedJSONResponse("Tibco/createAppointment");
	}

	public String updateAppointment(Layer7UpdateAppointmentDTO dto) throws Exception {
		return tinnedJSONResponse("Tibco/rescheduleAppointment");
	}

	public String timseslots(Layer7ReserveTimeslotsDTO dto) throws Exception {
		return tinnedJSONResponse("Tibco/reserveTimeslots");
	}

	public String assignedProducts(Layer7AssignedProductsDTO dto) throws Exception {
		return tinnedJSONResponse("Amdocs/searchAssignedProducts");
	}

	public String assignedProductDetails(Layer7AssignedProductDetailsDTO dto) throws Exception {
		return tinnedJSONResponse("Amdocs/getAssignedProductDetails_B2OSmartcard");
	}

	public String submitSwap(Layer7SubmitSwapDTO dto) throws Exception {
		return tinnedJSONResponse("Amdocs/submitSwap");
	}

	public String networkPathDetails(ISWRequestDTO dto) throws Exception {
		return tinnedJSONResponse("ISW/networkPathDetails");
	}

	public String ticketDetails(String ticketId) throws Exception {
		return tinnedJSONResponse("Amdocs/getTicketDetails");
	}

	public String cpeDetails(ISWRequestDTO dto) throws Exception {
		return tinnedJSONResponse("ISW/CPEDetails");
	}

	public String customerFilters(String customerId) throws Exception {
		return tinnedJSONResponse("K2View/customerFilters");
	}

	public String refreshEntitlements(String dto) throws Exception {
		return tinnedXMLResponse("SOM/refreshEntitlements");
	}

	private String tinnedJSONResponse(String tin) throws Exception {
		return Utils.RESOURCE_STRING(String.format("/layer7ExampleResponses/%s.json", tin));
	}

	private String tinnedXMLResponse(String tin) throws Exception {
		return Utils.RESOURCE_STRING(String.format("/layer7ExampleResponses/%s.xml", tin));
	}

	public String error() throws Exception {

		Layer7ResponseDTO resp = new Layer7ResponseDTO();
		resp.setStatus(99);

		ArrayList<Map<String,Object>> errs = resp.getError();
		Map<String,Object> err_details  = new HashMap<>();
		err_details.put("code", "ERR-99");
		err_details.put("message", "99 errors");
		errs.add(err_details);

		String body = mapper.writeValueAsString(resp);
		log.error("MOCKING ERR RESPONSE: {}", body);

		HttpClientErrorException ex = new HttpClientErrorException(
			HttpStatus.BAD_REQUEST,
			"bad news I'm afraid",
			body.getBytes(),
			Charset.defaultCharset()
		);
	  throw ex;
	}
}
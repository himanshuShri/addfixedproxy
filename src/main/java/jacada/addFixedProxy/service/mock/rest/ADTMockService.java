package jacada.addFixedProxy.service.mock.rest;

import java.nio.charset.Charset;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import com.fasterxml.jackson.databind.ObjectMapper;

import jacada.addFixedProxy.dto.adt.ADTPrivilegedResponseDTO;
import jacada.addFixedProxy.dto.adt.ADTResponseDTO;
import jacada.addFixedProxy.dto.adt.ADTUniversalDTO;
import lombok.val;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ADTMockService {

	@Autowired private ObjectMapper mapper;

	public ADTResponseDTO showTab(String customerId, String tabId, String sessionId) {

		val resp = new ADTResponseDTO();
		resp.setStatus(true);
		resp.setError(String.format("no showTab errors for %s-%s-%s", customerId, tabId, sessionId));

		return resp;
	}

	public ADTResponseDTO closeTab(String customerId, String tabId, String sessionId) {

		val resp = new ADTResponseDTO();
		resp.setStatus(true);
		resp.setError(String.format("no closeTab errors for %s-%s-%s", customerId, tabId, sessionId));

		return resp;
	}

	public ADTResponseDTO addImportantAction(ADTUniversalDTO dto, String customerId, String sessionId) {

		val resp = new ADTResponseDTO();
		resp.setStatus(true);
		resp.setError(String.format("no addImportantAction errors for %s-%s-%s", dto.getImportantAction(), customerId, sessionId));

		return resp;
	}

	public ADTResponseDTO dataUpdated(String customerId, String dataType, String id, String sessionId) {

		val resp = new ADTResponseDTO();
		resp.setStatus(true);
		resp.setError(String.format("no dataUpdated errors for %s-%s-%s-%s", customerId, dataType, id, sessionId));

		return resp;
	}

	public ADTResponseDTO isPrivileged(String privilege, String sessionId) {

		val resp = new ADTPrivilegedResponseDTO();
		resp.setStatus(true);
		resp.setError(String.format("no isPrivileged errors for %s-%s", privilege, sessionId));
		resp.setPrivileged(false);

		return resp;
	}

	public ADTResponseDTO performAction(String customerId, String actionName, String sessionId, String adtSessionId) {

		val resp = new ADTResponseDTO();
		resp.setStatus(true);
		resp.setError(String.format("no performAction errors for %s-%s-%s-%s", customerId, actionName, sessionId, adtSessionId));

		return resp;
	}

	public ADTResponseDTO error() throws Exception {

		ADTResponseDTO resp = new ADTResponseDTO();
		resp.setStatus(false);
		resp.setError("message, 99 errors");

		String body = mapper.writeValueAsString(resp);
		log.error("MOCKING ERR RESPONSE: {}", body);

		HttpClientErrorException ex = new HttpClientErrorException(
			HttpStatus.BAD_REQUEST,
			"very bad news I'm afraid",
			body.getBytes(),
			Charset.defaultCharset()
		);
	  throw ex;
	}
	
}
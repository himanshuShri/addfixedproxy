
package jacada.addFixedProxy.service.mock.soap;

import com.spatial_eye.xy.soap.Address;
import com.spatial_eye.xy.soap.ArrayOfaddress;
import com.spatial_eye.xy.soap.FindAddress1;
import com.spatial_eye.xy.soap.FindAddress1Response;
import com.spatial_eye.xy.soap.FindAddress1ResponseType;
import com.spatial_eye.xy.soap.ISyncReply;
import com.spatial_eye.xy.soap.ObjectFactory;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AddressServiceImpl implements ISyncReply {

	@Override
	public FindAddress1Response findAddress1(FindAddress1 par) {

		log.info("incoming as {}", par);
		ObjectFactory factory 			= new ObjectFactory();
		FindAddress1Response response	= factory.createFindAddress1Response();
		FindAddress1ResponseType body	= factory.createFindAddress1ResponseType();
		body.setResultCode(0);

		Address a = new Address();
		a.setPostalCode(factory.createAddressPostalCode("DL34 1QX"));
		a.setHouseNumber(27);
		a.setHouseNumberExtension(factory.createAddressHouseNumberExtension("a"));
		a.setStreet("Maison Dieu");
		a.setCity("Wellington");
//		a.setBagID(0L);

		ArrayOfaddress aA = factory.createArrayOfaddress();
		aA.getAddress().add(a);

		body.setMatchingAddresses(factory.createFindAddress1ResponseTypeMatchingAddresses(aA));

		response.setBody(body);
		return response;
	}
}
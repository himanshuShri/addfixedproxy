
package jacada.addFixedProxy.service.mock.soap;

import io.spring.guides.gs_producing_web_service.CountriesPort;
import io.spring.guides.gs_producing_web_service.Country;
import io.spring.guides.gs_producing_web_service.Currency;
import io.spring.guides.gs_producing_web_service.GetCountryRequest;
import io.spring.guides.gs_producing_web_service.GetCountryResponse;
import io.spring.guides.gs_producing_web_service.ObjectFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CountryServiceImpl implements CountriesPort {

	@Override
	public GetCountryResponse getCountry(GetCountryRequest request) {

		ObjectFactory factory = new ObjectFactory();
		GetCountryResponse response = factory.createGetCountryResponse();
		Country responseCountry = factory.createCountry();

		String requestCountryName = request.getName();
		responseCountry.setName(requestCountryName);

		if("Poland".equals(requestCountryName)) {

			log.info("recognised Poland");
			responseCountry.setCapital("Warsaw");
			responseCountry.setCurrency(Currency.PLN);
			responseCountry.setPopulation(50000000);
		} else if("Sweden".equals(requestCountryName)) {

			log.info("recognised Sweden");
			responseCountry.setCapital("Stockholm");
			responseCountry.setCurrency(Currency.EUR);
			responseCountry.setPopulation(20000000);
		} else {
			log.info("unrecognised country {}", requestCountryName);
		}

		response.setCountry(responseCountry);
		return response;
	}
}
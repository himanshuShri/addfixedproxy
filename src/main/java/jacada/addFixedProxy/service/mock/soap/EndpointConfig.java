package jacada.addFixedProxy.service.mock.soap;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Pattern;

import javax.xml.ws.Endpoint;

import org.apache.cxf.Bus;
import org.apache.cxf.binding.soap.Soap12;
import org.apache.cxf.binding.soap.SoapBindingConfiguration;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import jacada.addFixedProxy.properties.Layer7Properties;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class EndpointConfig {

	@Autowired private Layer7Properties l7Props;
	@Autowired private Bus bus;
	private Pattern svcNameRegex = Pattern.compile("^.*/");

	@Bean
	public Endpoint addressEndpoint() throws MalformedURLException {

		EndpointImpl endpoint = new EndpointImpl(bus, new AddressServiceImpl());

		URL url 		= new URL(l7Props.getAddressPortal());
		String protocol = url.getProtocol();
		String host 	= url.getHost();
		int port 		= url.getPort();
		String path 	= url.getPath();

		log.debug("URL created: "	+ url);
		log.debug("protocol: " 		+ protocol);
		log.debug("host: " 			+ host);
		log.debug("port: " 			+ port);
		log.debug("path: " 			+ path);

		// set up server to expect soap12 requests
		SoapBindingConfiguration config = new SoapBindingConfiguration();
		config.setVersion(Soap12.getInstance());
		endpoint.setBindingConfig(config);

		String svcName = svcNameRegex.matcher(path).replaceFirst("/");
		log.trace("Publishing address mock service on {}", svcName);
		endpoint.publish(svcName);

		return endpoint;
	}
}
package jacada.addFixedProxy.service.adt;

import java.util.HashMap;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;

@Slf4j
/**
 * ADT request/response is always JSON
 */
public enum ADTRequestTypes {
	CLOSE_TAB,
	SHOW_TAB,
	DATA_UPDATED,
	ADD_IMPORTANT_ACTION,
	IS_PRIVILEGED,
	CANCEL_APPOINTMENT,
	CREATE_AUDIT_POINT;

	static Map<String, Map<String, String>> outputPaths = new HashMap<> ();

	public static void paths(String key, Map<String, String> value) {
		outputPaths.put(key, value);
	}

	// return a null safe copy which can be added to
	public static Map<String, String> paths(String key) {

		Map<String, String> specificPaths = outputPaths.get(key);
		log.debug("Found these specific paths:{}", specificPaths);
		return specificPaths == null?
				new HashMap<>():
				new HashMap<>(specificPaths);
	}
}
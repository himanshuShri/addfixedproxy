package jacada.addFixedProxy.service.adt;

import java.lang.reflect.Method;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jacada.addFixedProxy.controller.ADTController;
import jacada.addFixedProxy.properties.ADTProperties;
import jacada.addFixedProxy.properties.InteractProperties;
import jacada.addFixedProxy.properties.Layer7Properties;
import jacada.addFixedProxy.properties.MongoProperties;
import jacada.addFixedProxy.properties.VanDyckProperties;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DiagnosticsService {

	@Autowired private ADTProperties		adtProperties;
	@Autowired private Layer7Properties		layer7Properties;
	@Autowired private MongoProperties		mongoProperties;
	@Autowired private InteractProperties 	interactProperties;
	@Autowired private VanDyckProperties	vandyckProperties;

	private final String ADT_TYPE 			= "adt";
	private final String INTERACT_TYPE 		= "interact";
	private final String LAYER7_TYPE		= "layer7";
	private final String MONGO_TYPE 		= "mongo";
	private final String VANDYCK_TYPE 		= "vandyck";
	private final String ENDPOINT_SUFFIX	= "EP";
	private final String PORTAL_SUFFIX		= "Portal";
	private final String OUTPUT_SUFFIX		= "OutputPaths";


	public List<String> buildInfo() throws Exception {

		log.debug("reading build info file");

		URL resource = ADTController.class.getResource("/META-INF/build-info.properties");
		return Files.readAllLines(Paths.get(resource.toURI()));
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> mapProperties(String type) throws Exception {

		log.debug("looking up properties for {}", type);

		Object[] lookup = typeMethods(type);
		if (lookup[0] != null) {
			return (Map<String, Object>) lookup[0];
		}

		Map<String, Object> resp = new HashMap<>();
		Method[] methods = (Method[]) lookup[1];
		Object instance = lookup[2];

		for (Method m: methods) {
			String name = m.getName();

			if (name.startsWith("get") && !"getClass".equals(name)) {
				log.trace("calling {}", name);
				Object value = m.invoke(instance);
				resp.put(propertyNameFromAccessor(name), value);
			}
		}

		return resp;
	}

	public Map<String, Object> endpoints(String type) throws Exception {

		Map<String, Object> all = mapProperties(type);
		Map<String, Object> resp = new HashMap<>();

		for(String k: all.keySet()) {
			if (k.endsWith(ENDPOINT_SUFFIX) || k.endsWith(PORTAL_SUFFIX)) {
				resp.put(k, all.get(k));
			}
		}

		return resp;
	}

	public Map<String, Object> outputPaths(String type) throws Exception {

		Map<String, Object> all = mapProperties(type);
		Map<String, Object> resp = new HashMap<>();

		for(String k: all.keySet()) {
			if (k.endsWith(OUTPUT_SUFFIX)) {
				resp.put(k, all.get(k));
			}
		}

		return resp;
	}

	/**
	 * @return 3 items in an array
	 * [0] - an error map, if this exists then abort and return it
	 * [1] - an array of method objects
	 * [2] - a properties instance
	 */
	private Object[] typeMethods(String type) {

		Class<?> c = null;
		Object instance = null;
		Object[] compositeResponse = new Object[3];

		switch (type) {
			case ADT_TYPE:
				c = ADTProperties.class;
				instance = adtProperties;
			break;
			case LAYER7_TYPE:
				c = Layer7Properties.class;
				instance = layer7Properties;
			break;
			case MONGO_TYPE:
				c = MongoProperties.class;
				instance = mongoProperties;
			break;
			case INTERACT_TYPE:
				c = InteractProperties.class;
				instance = interactProperties;
			break;
			case VANDYCK_TYPE:
				c = VanDyckProperties.class;
				instance = vandyckProperties;
			break;
			default:
				Map<String, Object> err = new HashMap<>();
				err.put("error", String.format("Properties type [%s] not found", type));
				err.put("supported types",
						String.format("%s, %s, %s, %s %s", ADT_TYPE, LAYER7_TYPE, INTERACT_TYPE, MONGO_TYPE, VANDYCK_TYPE));

				compositeResponse[0] = err;
				return compositeResponse;
		}

		compositeResponse[1] = c.getMethods();
		compositeResponse[2] = instance;
		return compositeResponse;
	}

	private String propertyNameFromAccessor(String name) {

		name = name.substring(3, name.length());
		char c = name.charAt(0);

		return String.format("%c%s", Character.toLowerCase(c), name.substring(1, name.length()));
	}
}
package jacada.addFixedProxy.service.adt;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import jacada.addFixedProxy.dto.adt.ADTUniversalDTO;
import jacada.addFixedProxy.dto.proxy.adt.ProxyADTRequestDTO;
import jacada.addFixedProxy.properties.ADTProperties;
import jacada.addFixedProxy.service.RestServiceDAO;
import jacada.addFixedProxy.service.VendorResponseMapper;
import jacada.addFixedProxy.service.adt.dataFactory.Privilege;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ADTService {

	@Autowired private ADTRequestBuilder reqBuilder;
	@Autowired private ADTProperties properties;
	@Autowired private VendorResponseMapper<ADTProperties> responseMapper;
	@Autowired Privilege privilegeFactory;
	@Autowired private RestServiceDAO dao;

	public Map<String, Object> closeTab(ProxyADTRequestDTO dto) {

		String url = dto.generateUrl(properties.getCloseTabEP());
		HttpEntity<ADTUniversalDTO> request = reqBuilder.buildRequest(new ADTUniversalDTO(dto));

		log.debug("Handling closeTab");
		String adtResp = dao.call(url, HttpMethod.POST, request);
		return responseMapper.mapResponse(ADTRequestTypes.CLOSE_TAB, adtResp);
	}

	public Map<String, Object> showTab(ProxyADTRequestDTO dto) {

		String url = dto.generateUrl(properties.getShowTabEP());
		HttpEntity<ADTUniversalDTO> request = reqBuilder.buildRequest(new ADTUniversalDTO(dto));

		log.debug("Handling showTab");
		String adtResp = dao.call(url, HttpMethod.POST, request);
		return responseMapper.mapResponse(ADTRequestTypes.SHOW_TAB, adtResp);
	}

	public Map<String, Object> addImportantAction(ProxyADTRequestDTO dto) {

		String url = dto.generateUrl(properties.getAddImportantActionEP());
		HttpEntity<ADTUniversalDTO> request = reqBuilder.buildRequest(new ADTUniversalDTO(dto));

		log.debug("Handling addImportantAction");
		String adtResp = dao.call(url, HttpMethod.POST, request);
		return responseMapper.mapResponse(ADTRequestTypes.ADD_IMPORTANT_ACTION, adtResp);
	}

	public Map<String, Object> dataUpdated(ProxyADTRequestDTO dto) {

		String url = dto.generateUrl(properties.getDataUpdatedEP());
		HttpEntity<ADTUniversalDTO> request = reqBuilder.buildRequest(new ADTUniversalDTO(dto));

		log.debug("Handling dataUpdated");
		String adtResp = dao.call(url, HttpMethod.POST, request);
		return responseMapper.mapResponse(ADTRequestTypes.DATA_UPDATED, adtResp);
	}

	public Map<String, Object> isPrivileged(ProxyADTRequestDTO dto) {

		String adtResp = privilegeFactory.call(dto);
		return responseMapper.mapResponse(ADTRequestTypes.IS_PRIVILEGED, adtResp);
	}

	public Map<String, Object> cancelAppointment(ProxyADTRequestDTO dto) {

		String url = dto.generateUrl(properties.getCancelAppointmentEP());
		HttpEntity<ADTUniversalDTO> request = reqBuilder.buildRequest(new ADTUniversalDTO(dto));

		log.debug("Handling cancelAppointment");
		String adtResp = dao.call(url, HttpMethod.POST, request);
		return responseMapper.mapResponse(ADTRequestTypes.CANCEL_APPOINTMENT, adtResp);
	}
	
	public Map<String, Object> createAuditPoint(ProxyADTRequestDTO dto) {

		String url = dto.generateUrl(properties.getCreateAuditPointEP());
		HttpEntity<ADTUniversalDTO> request = reqBuilder.buildRequest(new ADTUniversalDTO(dto));

		log.debug("Handling createAuditPoint");
		String adtResp = dao.call(url, HttpMethod.POST, request);
		return responseMapper.mapResponse(ADTRequestTypes.CREATE_AUDIT_POINT, adtResp);
	}
}
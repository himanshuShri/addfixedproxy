package jacada.addFixedProxy.service.adt;

import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;

import jacada.addFixedProxy.dto.adt.ADTUniversalDTO;

/**
 * ADT requests are nearly all the same so this serves as a good base and is sufficient for most requests.
 **/
@Component
public class ADTRequestBuilder {

	private LinkedMultiValueMap<String, String> headers;
	private final String contentTypeHeader = "Content-Type";
	private final String json = "application/json";

	public HttpEntity<ADTUniversalDTO> buildRequest() {

		if(headers == null) {
			headers = new LinkedMultiValueMap<>();
			headers.add(contentTypeHeader, json);
		}

		LinkedMultiValueMap<String, String> hdrs = headers;
		HttpEntity<ADTUniversalDTO> request = new HttpEntity<>(hdrs);

		return request;
	}


	public HttpEntity<ADTUniversalDTO> buildRequest(ADTUniversalDTO dto) {

		if(headers == null) {
			headers = new LinkedMultiValueMap<>();
			headers.add(contentTypeHeader, json);
		}

		LinkedMultiValueMap<String, String> hdrs = headers;
		HttpEntity<ADTUniversalDTO> request = new HttpEntity<>(dto, hdrs);

		return request;
	}
}
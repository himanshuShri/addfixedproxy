package jacada.addFixedProxy.service.adt.dataFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import jacada.addFixedProxy.dto.adt.ADTUniversalDTO;
import jacada.addFixedProxy.dto.proxy.adt.ProxyADTRequestDTO;
import jacada.addFixedProxy.properties.ADTProperties;
import jacada.addFixedProxy.service.RestServiceDAO;
import jacada.addFixedProxy.service.adt.ADTRequestBuilder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class Privilege {

	@Autowired private ADTRequestBuilder reqBuilder;
	@Autowired private ADTProperties properties;
	@Autowired private RestServiceDAO dao;

	public String call(ProxyADTRequestDTO dto) {

		String url = dto.generateUrl(properties.getIsPrivilegedEP());
		HttpEntity<ADTUniversalDTO> request = reqBuilder.buildRequest();

		log.debug("Handling isPrivileged");
		return dao.call(url, HttpMethod.GET, request);
	}

	public PrivilegeVO callForVO(ProxyADTRequestDTO dto) {

		PrivilegeVO vo = new PrivilegeVO();
		vo.setPrivilege(dto.getPrivilege());
		vo.setSession(dto.getAdtSessionId());
		vo.dress(call(dto));
		return vo;
	}

	@Data
	public class PrivilegeVO {

		private String privilege;
		private String session;
		private boolean granted;

		public void dress(String svcRespStr) {
			DocumentContext svcRespDoc = JsonPath.parse(svcRespStr);
			granted = svcRespDoc.read("$.privileged");
		}
	}
}
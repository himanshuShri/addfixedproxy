package jacada.addFixedProxy.service.layer7;

import java.util.HashMap;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public enum Layer7RequestTypes {

	// ordinary BSL requests
	CONTACT_DETAILS						(false),
	CUSTOMER_PROFILE					(false),
	BILLING_ARRANGEMENT_DETAILS			(false),
	BILLING_ENTITIES					(false),
	UPDATE_ACCOUNT_DETAILS				(false),
	UPDATE_BILLING_ARRANGEMENT_DETAILS	(false),
	UPDATE_BILLING_CUSTOMER_DETAILS		(false),
	UPDATE_CONTACT						(false),
	RESERVE_TIMESLOTS					(false),
	APPOINTMENT							(false),
	CREATE_APPOINTMENT					(false),
	UPDATE_APPOINTMENT					(false),
	SUBMIT_SWAP							(false),
	CREATE_INTERACTION					(false),
	CREATE_TICKET						(false),
	ASSIGNED_PRODUCTS					(false),
	ASSIGNED_PRODUCT_DETAILS			(false),
	TICKET_DETAILS						(false),
	CUSTOMER_FILTERS					(false),
	REFRESH_ENTITLEMENTS				(true),

	// ISW requests are serialised separately, but parsed alongside BSL
	NETWORK_PATH_DETAILS				(false),
	CPE_DETAILS							(false);

	// SOAP requests are handled completely separately from BSL
	// ADDRESS SOAP
	private boolean parseAsXml;
	private Layer7RequestTypes(boolean parseXml) {
		parseAsXml 		= parseXml;
	}

	public boolean parseAsXml() {
		return parseAsXml;
	}

	// CLEAN by creating a parent interface, this static can be avoided
	static Map<String, Map<String, String>> outputPaths = new HashMap<> ();

	public void paths(Map<String, String> value) {
		log.trace("Registering output path key [{}] with [{}]",  this, value);
		outputPaths.put(this.name(), value);
	}

	// return a null safe copy which can be added to
	public static Map<String, String> paths(String key) {

		Map<String, String> specificPaths = outputPaths.get(key);
		return specificPaths == null?
				new HashMap<>():
				new HashMap<>(specificPaths);
	}
}
package jacada.addFixedProxy.service.layer7.soap;

import javax.net.ssl.KeyManager;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.ws.soap.SOAPBinding;

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.configuration.security.AuthorizationPolicy;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.ext.logging.LoggingFeature;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.spatial_eye.xy.soap.ISyncReply;

import jacada.addFixedProxy.properties.Layer7Properties;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class ClientConfig {

	@Autowired
	private Layer7Properties l7Props;

	@Bean
	public LoggingFeature loggingFeature() {

		LoggingFeature loggingFeature = new LoggingFeature();
		loggingFeature.setPrettyLogging(true);
		loggingFeature.setVerbose(true);

		return loggingFeature;
	}

	@Bean
	public ISyncReply addressProxy() {

		JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
		jaxWsProxyFactoryBean.setServiceClass(ISyncReply.class);
		jaxWsProxyFactoryBean.setAddress(l7Props.getAddressPortal());
		jaxWsProxyFactoryBean.getFeatures().add(loggingFeature());

		// make client send soap12 requests like soapui
		jaxWsProxyFactoryBean.setBindingId(SOAPBinding.SOAP12HTTP_BINDING);
		ISyncReply client = (ISyncReply) jaxWsProxyFactoryBean.create();

		log.trace("adding https handling to address proxy");
		configureSSLOnTheClient(client);
		return client;
	}

	private void configureSSLOnTheClient(Object client_) {

		log.trace("creating basic auth token");
		final AuthorizationPolicy basicAuthPolicy = new AuthorizationPolicy();
		basicAuthPolicy.setUserName(l7Props.getLayer7User());
		basicAuthPolicy.setPassword(l7Props.getLayer7Password());
		basicAuthPolicy.setAuthorizationType("Basic");

		log.trace("creating TLSClientParameters object");
		final TLSClientParameters tlsParams = new TLSClientParameters();
		tlsParams.setKeyManagers(new KeyManager[0]);
		tlsParams.setDisableCNCheck(true);

		log.trace("creating RubberStampTrustManager which accepts all");
		TrustManager[] trustManagers = new TrustManager[] { new RubberStampTrustManager() };
		tlsParams.setTrustManagers(trustManagers);

		log.trace("assembling client");
		final Client c = ClientProxy.getClient(client_);
		final HTTPConduit httpConduit = (HTTPConduit) c.getConduit();

		httpConduit.setAuthorization(basicAuthPolicy);
		httpConduit.setTlsClientParameters(tlsParams);
	}

	/**
	 * Even though we use basic authentication, the service will return a certificate in accordance
	 * with the https protocol and java will insist that this certificate is checked against the trust store.
	 * Here the implementation of that check is stubbed out to prevent exceptions.
	 */
	private static class RubberStampTrustManager implements X509TrustManager {

		@Override
		public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType)
			throws java.security.cert.CertificateException {
		}

		@Override
		public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)
			throws java.security.cert.CertificateException {
		}

		@Override
		public java.security.cert.X509Certificate[] getAcceptedIssuers() {
			return new java.security.cert.X509Certificate[0];
		}
	}
}
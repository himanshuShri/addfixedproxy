package jacada.addFixedProxy.service.layer7.soap;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;

import org.apache.cxf.message.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.spatial_eye.xy.soap.Address;
import com.spatial_eye.xy.soap.AddressType;
import com.spatial_eye.xy.soap.ArrayOfaddress;
import com.spatial_eye.xy.soap.FindAddress1;
import com.spatial_eye.xy.soap.FindAddress1RequestType;
import com.spatial_eye.xy.soap.FindAddress1Response;
import com.spatial_eye.xy.soap.FindAddress1ResponseType;
import com.spatial_eye.xy.soap.HeaderType;
import com.spatial_eye.xy.soap.ISyncReply;
import com.spatial_eye.xy.soap.ObjectFactory;
import com.spatial_eye.xy.soap.SearchParameterType;
import com.spatial_eye.xy.soap.SourceContextType;

import jacada.addFixedProxy.UtilBeans.DateHandler;
import jacada.addFixedProxy.dto.proxy.layer7.AddressDTO;
import jacada.addFixedProxy.dto.proxy.layer7.Layer7ProxySharedTypes.AddressDetails;
import jacada.addFixedProxy.properties.Layer7Properties;
import jacada.addFixedProxy.service.layer7.rest.Layer7HeadersHelper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Component
public class AddressClient {

	@Autowired private ISyncReply addressProxy;
	@Autowired private Layer7Properties layer7Props;
	@Autowired private Layer7HeadersHelper layer7Helper;
	@Autowired private DateHandler dateHandler;

	private FindAddress1Response call(AddressDTO dto) {

		ObjectFactory factory = new ObjectFactory();
		XMLGregorianCalendar now = dateHandler.gregorianNow();

		HeaderType faRequestHeader = factory.createHeaderType();
		FindAddress1RequestType faRequestBody = factory.createFindAddress1RequestType();

		// header fields
		SourceContextType sourceCtxt = factory.createSourceContextType();
		sourceCtxt.setApplication(layer7Props.getChannel());
		sourceCtxt.setHost(layer7Props.getHost());

		faRequestHeader.setBusinessTransactionID(dto.getBusinessTransactionId());
		faRequestHeader.setSentTimestamp(now);
		faRequestHeader.setSourceContext(sourceCtxt);
		faRequestHeader.setExternalCorrelationID(
				factory.createHeaderTypeExternalCorrelationID(layer7Helper.NEXT_MSG_ID())
		);

		// body fields
		AddressType addrType = factory.createAddressType();
		addrType.setExactMatch(true);

		AddressDetails addr = dto.getAddress();
		addrType.setPostalCode(addr.getZipCode());
		addrType.setHouseNumber(Integer.valueOf(addr.getHouseNumber()));
		addrType.setRoomNumber(factory.createAddressRoomNumber(addr.getRoomNumber()));
		addrType.setHouseNumberExtension(
				factory.createAddressHouseNumberExtension(addr.getHouseNumberExtension())
		);

		faRequestBody.setSearchParameter(SearchParameterType.ADDRESS);
		faRequestBody.setAddress(factory.createFindAddress1RequestTypeAddress(addrType));

		FindAddress1 findAddressRequest = factory.createFindAddress1();

		// build request
		findAddressRequest.setHeader(faRequestHeader);
		findAddressRequest.setBody(faRequestBody);

		return addressProxy.findAddress1(findAddressRequest);
	}

	public AddressSearchVO callforVO(AddressDTO dto) {
		return new AddressSearchVO(call(dto));
	}

	@Data
	@ToString(callSuper = true)
	@EqualsAndHashCode(callSuper = false)
	public class AddressSearchVO {

		private int responseCode 					= 0;
		private int resultCode 						= 0;
		private int errorCode 						= 0;
		private int matchCt 						= 0;
		private List<AddressDetailsVO> addresses 	= new ArrayList<>();

		public AddressSearchVO(FindAddress1Response svcRespObj) {

			FindAddress1ResponseType body = svcRespObj.getBody();
			responseCode = (Integer) ((BindingProvider) addressProxy).getResponseContext().get(Message.RESPONSE_CODE);
			resultCode = body.getResultCode();
			errorCode = body.getErrorCode();

			JAXBElement<ArrayOfaddress> j = body.getMatchingAddresses();
			if (j != null) {
				for(Address a: j.getValue().getAddress()) {
					addresses.add(new AddressDetailsVO(a));
				}
			}

			matchCt = addresses.size();
		}
	}

	@Data
	@ToString(callSuper = true)
	@EqualsAndHashCode(callSuper = false)
	public class AddressDetailsVO {

		private String zipCode;
		private int houseNumber;
		private String houseNumberExtension;
		private String roomNumber;
		private String street;
		private String city;

		public AddressDetailsVO(Address a) {

			houseNumber = a.getHouseNumber();
			street = a.getStreet();
			city = a.getCity();

			JAXBElement<String> j = null;
			j = a.getPostalCode();
			zipCode = j == null? "" : j.getValue();

			j = a.getHouseNumberExtension();
			houseNumberExtension = j == null? "" : j.getValue();

			j = a.getRoomNumber();
			roomNumber = j == null? "" : j.getValue();
		}
	}
}
package jacada.addFixedProxy.service.layer7.rest;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import jacada.addFixedProxy.dto.layer7.Layer7AppointmentDTO;
import jacada.addFixedProxy.dto.layer7.Layer7CreateAppointmentDTO;
import jacada.addFixedProxy.dto.layer7.Layer7CreateInteractionDTO;
import jacada.addFixedProxy.dto.layer7.Layer7CreateTicketDTO;
import jacada.addFixedProxy.dto.layer7.Layer7SOMOrderDTO;
import jacada.addFixedProxy.dto.layer7.Layer7SubmitSwapDTO;
import jacada.addFixedProxy.dto.layer7.Layer7UpdateAccountDetailsDTO;
import jacada.addFixedProxy.dto.layer7.Layer7UpdateAppointmentDTO;
import jacada.addFixedProxy.dto.layer7.Layer7UpdateBillingArrangementDetailsDTO;
import jacada.addFixedProxy.dto.layer7.Layer7UpdateBillingCustomerDetailsDTO;
import jacada.addFixedProxy.dto.layer7.Layer7UpdateContactDTO;
import jacada.addFixedProxy.dto.proxy.layer7.AddressDTO;
import jacada.addFixedProxy.dto.proxy.layer7.CPEDetailsDTO;
import jacada.addFixedProxy.dto.proxy.layer7.CreateAppointmentDTO;
import jacada.addFixedProxy.dto.proxy.layer7.CreateInteractionDTO;
import jacada.addFixedProxy.dto.proxy.layer7.CreateTicketDTO;
import jacada.addFixedProxy.dto.proxy.layer7.CreateTicketPhase2DTO;
import jacada.addFixedProxy.dto.proxy.layer7.GetRequestDTO;
import jacada.addFixedProxy.dto.proxy.layer7.GetRequestDTO.ID_TYPE;
import jacada.addFixedProxy.dto.proxy.layer7.NetworkPathDetailsDTO;
import jacada.addFixedProxy.dto.proxy.layer7.ReserveTimeslotsDTO;
import jacada.addFixedProxy.dto.proxy.layer7.SOMOrderDTO;
import jacada.addFixedProxy.dto.proxy.layer7.SubmitSwapDTO;
import jacada.addFixedProxy.dto.proxy.layer7.UpdateAccountDetailsDTO;
import jacada.addFixedProxy.dto.proxy.layer7.UpdateAppointmentDTO;
import jacada.addFixedProxy.dto.proxy.layer7.UpdateBillingArrangementDetailsDTO;
import jacada.addFixedProxy.dto.proxy.layer7.UpdateBillingCustomerDetailsDTO;
import jacada.addFixedProxy.dto.proxy.layer7.UpdateContactDTO;
import jacada.addFixedProxy.properties.Layer7Properties;
import jacada.addFixedProxy.service.RestServiceDAO;
import jacada.addFixedProxy.service.VendorResponseMapper;
import jacada.addFixedProxy.service.layer7.Layer7RequestTypes;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.AppointmentDetails;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.AppointmentDetails.AppointmentDetailsVO;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.AssignedProductDetails;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.AssignedProductDetails.AssignedProductDetailsVO;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.AssignedProducts;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.AssignedProducts.AssignedProductsVO;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.BillingArrangementDetails;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.BillingEntities;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.BillingEntities.BillingEntitiesVO;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.CPEDetails;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.ContactDetails;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.CustomerFilters;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.CustomerProfile;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.CustomerProfile.CustomerProfileVO;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.EngineerTimeslots;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.NetworkPathDetails;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.NetworkPathDetails.NetworkPathDetailsVO;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.StaticData;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.StaticData.StaticDataVO;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.TicketDetails;
import jacada.addFixedProxy.service.layer7.soap.AddressClient;
import jacada.addFixedProxy.service.layer7.soap.AddressClient.AddressSearchVO;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class Layer7Service {

	@Autowired private Layer7RequestBuilder<Layer7UpdateContactDTO> updateContactReqBuilder;
	@Autowired private Layer7RequestBuilder<Layer7UpdateAccountDetailsDTO> updateAccountDetailsReqBuilder;
	@Autowired private Layer7RequestBuilder<Layer7UpdateBillingCustomerDetailsDTO> updateBillingCustomerDetailsReqBuilder;
	@Autowired private Layer7RequestBuilder<Layer7CreateTicketDTO> createTicketReqBuilder;
	@Autowired private Layer7RequestBuilder<Layer7CreateInteractionDTO> createInteractionReqBuilder;
	@Autowired private Layer7RequestBuilder<Layer7CreateAppointmentDTO> createAppointmentReqBuilder;
	@Autowired private Layer7RequestBuilder<Layer7UpdateAppointmentDTO> updateAppointmentReqBuilder;
	@Autowired private Layer7RequestBuilder<Layer7UpdateBillingArrangementDetailsDTO> updateBillingArrangementReqBuilder;
	@Autowired private Layer7RequestBuilder<Layer7SubmitSwapDTO> submitSwapReqBuilder;
	@Autowired private Layer7XmlRequestBuilder<Layer7SOMOrderDTO> somOrderReqBuilder;
	@Autowired private VendorResponseMapper<Layer7Properties> amdocsResponseMapper;
	@Autowired private Layer7Properties properties;

	@Autowired private StaticData staticDataFactory;
	@Autowired private CustomerProfile customerProfileFactory;
	@Autowired private BillingArrangementDetails billingArrangementDetailsFactory;
	@Autowired private BillingEntities billingEntitiesFactory;
	@Autowired private AppointmentDetails appointmentDetailsFactory;
	@Autowired private AssignedProducts assignedProductsFactory;
	@Autowired private AssignedProductDetails assignedProductDetailsFactory;
	@Autowired private ContactDetails contactDetailsFactory;
	@Autowired private NetworkPathDetails networkPathDetailsFactory;
	@Autowired private TicketDetails ticketDetailsFactory;
	@Autowired private CPEDetails cpeDetailsFactory;
	@Autowired private EngineerTimeslots timeslotsFactory;
	@Autowired private CustomerFilters customerFiltersFactory;

	@Autowired private RestServiceDAO dao;
	@Autowired private AddressClient addressSoapClient;


	public AddressSearchVO address(AddressDTO dto) {

		return addressSoapClient.callforVO(dto);
	}

	public Map<String, Object> assignedProducts(GetRequestDTO dto) {

		String amdocsResp = assignedProductsFactory.call(dto);
		return amdocsResponseMapper.mapResponse(Layer7RequestTypes.ASSIGNED_PRODUCTS, amdocsResp);
	}

	public Map<String, Object> assignedProductDetails(GetRequestDTO dto) throws Exception {

		// decide how to proceed based on the id type
		String customerId = dto.id(ID_TYPE.CUSTOMER_ID);
		String amdocsResp = null;
		Map<String, Object> errMap = null;

		if(StringUtils.isEmpty(customerId)) {

			log.info("No customerId was found so calling assigned product details immediately");
			amdocsResp = assignedProductDetailsFactory.call(dto);
		} else {

			log.debug("CustomerId {} was found so calling assigned products first", customerId);

			//
			// now call assignedProducts to fill in the blanks
			//

			log.debug("Calling assigned products to complete the request parameters");

			AssignedProductsVO productsVO = assignedProductsFactory.callForVO(dto);

			errMap = productsVO.generateErrorMap();
			if(errMap != null) return errMap;

			amdocsResp = assignedProductDetailsFactory.with(productsVO).call(dto);
		}

		return amdocsResponseMapper.mapResponse(Layer7RequestTypes.ASSIGNED_PRODUCT_DETAILS, amdocsResp);
	}

	public Map<String, Object> contactDetails(GetRequestDTO dto) {

		String amdocsResp = contactDetailsFactory.call(dto);
		return amdocsResponseMapper.mapResponse(Layer7RequestTypes.CONTACT_DETAILS, amdocsResp);
	}

	public Map<String, Object> appointment(GetRequestDTO dto) {

		String amdocsResp = appointmentDetailsFactory.call(dto);
		return amdocsResponseMapper.mapResponse(Layer7RequestTypes.APPOINTMENT, amdocsResp);
	}

	public Map<String, Object> customerProfile(GetRequestDTO dto) {

		String amdocsResp = customerProfileFactory.call(dto);
		return amdocsResponseMapper.mapResponse(Layer7RequestTypes.CUSTOMER_PROFILE, amdocsResp);
	}

	/**
	 * This method artificially adds billingEntities data to
	 * the response. It can't be added dynamically as it comes
	 * not from the final response but from the previous one.
	 * @param dto
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> billingArrangementDetails(GetRequestDTO dto) throws Exception {

		log.debug("Handling billingArrangementDetails");
		Map<String, Object> errMap = null;

		//
		// now call billingEntities to fill in the blanks
		//
		log.info("Calling billing entities to complete the request parameters");

		BillingEntitiesVO entitiesVO = billingEntitiesFactory.callForVO(dto);

		errMap = entitiesVO.generateErrorMap();
		if(errMap != null) return errMap;

		String amdocsResp = billingArrangementDetailsFactory.with(entitiesVO).call(dto);
		Map<String, Object> resp = amdocsResponseMapper.mapResponse(Layer7RequestTypes.BILLING_ARRANGEMENT_DETAILS, amdocsResp);

		Map<String, Object> billingEntitiesData = new HashMap<>();
		resp.put("billingEntitiesData",					billingEntitiesData);

		billingEntitiesData.put("barName", 			entitiesVO.getBarName());
		billingEntitiesData.put("houseNumber",	entitiesVO.getHouseNumber());
		billingEntitiesData.put("flat", 				entitiesVO.getFlat());
		billingEntitiesData.put("addition",			entitiesVO.getAddition());

		return resp;
	}

	public Map<String, Object> billingEntities(GetRequestDTO dto) {

		String amdocsResp = billingEntitiesFactory.call(dto);
		return amdocsResponseMapper.mapResponse(Layer7RequestTypes.BILLING_ENTITIES, amdocsResp);
	}

	public Map<String, Object> networkPathDetails(NetworkPathDetailsDTO dto) {

		String amdocsResp = networkPathDetailsFactory.call(dto);
		return amdocsResponseMapper.mapResponse(Layer7RequestTypes.NETWORK_PATH_DETAILS, amdocsResp);
	}

	public Map<String, Object> customerFilters(GetRequestDTO dto) {

		String amdocsResp = customerFiltersFactory.call(dto);
		return amdocsResponseMapper.mapResponse(Layer7RequestTypes.CUSTOMER_FILTERS, amdocsResp);
	}

	public Map<String, Object> ticketDetails(GetRequestDTO dto) {

		String amdocsResp = ticketDetailsFactory.call(dto);
		return amdocsResponseMapper.mapResponse(Layer7RequestTypes.TICKET_DETAILS, amdocsResp);
	}

	public Map<String, Object> createInteraction(CreateInteractionDTO dto) {

		String url = dto.generateUrl(properties.getCreateInteractionEP());
		HttpEntity<Layer7CreateInteractionDTO> request =
				createInteractionReqBuilder.buildRequest(new Layer7CreateInteractionDTO(dto));

		log.debug("Handling createInteraction");
		String amdocsResp = dao.call(url, HttpMethod.POST, request);
		return amdocsResponseMapper.mapResponse(Layer7RequestTypes.CREATE_INTERACTION, amdocsResp);
	}

	public Map<String, Object> refreshEntitlements(SOMOrderDTO dto) throws Exception {

		String url = dto.generateUrl(properties.getRefreshEntitlementsEP());

		HttpEntity<String> request =
				somOrderReqBuilder.buildRequest(new Layer7SOMOrderDTO(dto));

		log.debug("Handling refreshEntitlements");
		String amdocsResp = dao.call(url, HttpMethod.POST, request);

		return amdocsResponseMapper.mapResponse(Layer7RequestTypes.REFRESH_ENTITLEMENTS, amdocsResp);
	}

	//TODO after PI10 remove this method along with DTO & controller
	public Map<String, Object> createTicket(CreateTicketDTO dto) {

		String url = dto.generateUrl(properties.getCreateTicketEP());
		HttpEntity<Layer7CreateTicketDTO> request =
				createTicketReqBuilder.buildRequest(new Layer7CreateTicketDTO(dto));

		log.debug("Handling createCase");
		String amdocsResp = dao.call(url, HttpMethod.POST, request);
		return amdocsResponseMapper.mapResponse(Layer7RequestTypes.CREATE_TICKET, amdocsResp);
	}

	public Map<String, Object> createTicket(CreateTicketPhase2DTO dto) {

		String url = dto.generateUrl(properties.getCreateTicketEP());
		HttpEntity<Layer7CreateTicketDTO> request =
				createTicketReqBuilder.buildRequest(new Layer7CreateTicketDTO(dto));

		log.debug("Handling createCase");
		String amdocsResp = dao.call(url, HttpMethod.POST, request);
		return amdocsResponseMapper.mapResponse(Layer7RequestTypes.CREATE_TICKET, amdocsResp);
	}

	public Map<String, Object> updateAccountDetails(UpdateAccountDetailsDTO dto) {

		String url = dto.generateUrl(properties.getUpdateAccountDetailsEP());
		HttpEntity<Layer7UpdateAccountDetailsDTO> request =
				updateAccountDetailsReqBuilder.buildRequest(new Layer7UpdateAccountDetailsDTO(dto));

		log.debug("Handling updateAccountDetails");
		String amdocsResp = dao.call(url, HttpMethod.POST, request);
		return amdocsResponseMapper.mapResponse(Layer7RequestTypes.UPDATE_ACCOUNT_DETAILS, amdocsResp);
	}

	public Map<String, Object> updateBillingArrangementDetails(UpdateBillingArrangementDetailsDTO dto) {

		String url = dto.generateUrl(properties.getUpdateBillingArrangementDetailsEP());
		HttpEntity<Layer7UpdateBillingArrangementDetailsDTO> request =
				updateBillingArrangementReqBuilder.buildRequest(new Layer7UpdateBillingArrangementDetailsDTO(dto));

		log.debug("Handling Layer7UpdateBillingArrangementDetails");
		String amdocsResp = dao.call(url, HttpMethod.POST, request);
		return amdocsResponseMapper.mapResponse(Layer7RequestTypes.UPDATE_BILLING_ARRANGEMENT_DETAILS, amdocsResp);
	}

	public Map<String, Object> updateBillingCustomerDetails(UpdateBillingCustomerDetailsDTO dto) {

		String url = dto.generateUrl(properties.getUpdateBillingCustomerDetailsEP());
		HttpEntity<Layer7UpdateBillingCustomerDetailsDTO> request =
				updateBillingCustomerDetailsReqBuilder.buildRequest(new Layer7UpdateBillingCustomerDetailsDTO(dto));

		log.debug("Handling updateBillingCustomerDetails");
		String amdocsResp = dao.call(url, HttpMethod.POST, request);
		return amdocsResponseMapper.mapResponse(Layer7RequestTypes.UPDATE_BILLING_CUSTOMER_DETAILS, amdocsResp);
	}

	public Map<String, Object> updateContact(UpdateContactDTO dto) {

		String url = dto.generateUrl(properties.getUpdateContactEP());
		HttpEntity<Layer7UpdateContactDTO> request =
				updateContactReqBuilder.buildRequest(new Layer7UpdateContactDTO(dto));

		log.debug("Handling updateContact", url, request);
		String amdocsResp = dao.call(url, HttpMethod.POST, request);

		return amdocsResponseMapper.mapResponse(Layer7RequestTypes.UPDATE_CONTACT, amdocsResp);
	}

	public Map<String, Object> submitSwap(SubmitSwapDTO dto) {

		String url = dto.generateUrl(properties.getSubmitSwapEP());
		HttpEntity<Layer7SubmitSwapDTO> request =
				submitSwapReqBuilder.buildRequest(new Layer7SubmitSwapDTO(dto));

		log.debug("Handling submitSwap ", url, request);
		String amdocsResp = dao.call(url, HttpMethod.POST, request);

		return amdocsResponseMapper.mapResponse(Layer7RequestTypes.SUBMIT_SWAP, amdocsResp);
	}

	public Map<String, Object> reserveTimeslots(ReserveTimeslotsDTO dto) throws Exception {

		log.debug("Handling reserveTimeslots");
		Map<String, Object> errMap = null;

		//
		// now call getCustomerProfile to fill in the blanks
		//
		log.info("Calling customer profile to complete the request parameters");

		CustomerProfileVO custProfVO =
				customerProfileFactory.callForVO(new GetRequestDTO(dto.getCustomerId(), ID_TYPE.CUSTOMER_ID));

		errMap = custProfVO.generateErrorMap();
		if(errMap != null) return errMap;

		String amdocsResp = timeslotsFactory.with(custProfVO).call(dto);
		return amdocsResponseMapper.mapResponse(Layer7RequestTypes.RESERVE_TIMESLOTS, amdocsResp);
	}

	public Map<String, Object> cpeDetails(CPEDetailsDTO dto) {

		String amdocsResp = cpeDetailsFactory.call(dto);
		return amdocsResponseMapper.mapResponse(Layer7RequestTypes.CPE_DETAILS, amdocsResp);
	}

	public Map<String, Object> createAppointment(CreateAppointmentDTO dto) {

		log.debug("Handling createAppointment");
		Map<String, Object> errMap = null;
		Layer7CreateAppointmentDTO apptDto = new Layer7CreateAppointmentDTO(dto);

		errMap = populateAppointmentDTOType(apptDto);
		if(errMap != null) return errMap;

		String url = dto.generateUrl(properties.getCreateAppointmentEP());
		swapCustomerId(apptDto);
		HttpEntity<Layer7CreateAppointmentDTO> request = createAppointmentReqBuilder.buildRequest(apptDto);

		String amdocsResp = dao.call(url, HttpMethod.POST, request);
		return amdocsResponseMapper.mapResponse(Layer7RequestTypes.CREATE_APPOINTMENT, amdocsResp);
	}

	public Map<String, Object> updateAppointment(UpdateAppointmentDTO dto) {

		log.debug("Handling updateAppointment");
		Map<String, Object> errMap = null;
		Layer7UpdateAppointmentDTO apptDto = new Layer7UpdateAppointmentDTO(dto);

		errMap = populateAppointmentDTOType(apptDto);
		if(errMap != null) return errMap;

		//
		// now look up the existing appointment to fill in the appointment blanks
		//
		log.info("Looking up existing appointment to complete the request parameters");
		AppointmentDetailsVO apptDetailsVO =
				appointmentDetailsFactory.callForVO(new GetRequestDTO(apptDto.getOldAppointmentId(), ID_TYPE.APPOINTMENT_ID));

		errMap = apptDetailsVO.generateErrorMap();
		if(errMap != null) return errMap;
		apptDto.consume(apptDetailsVO);

		String url = dto.generateUrl(properties.getUpdateAppointmentEP());
		swapCustomerId(apptDto);
		HttpEntity<Layer7UpdateAppointmentDTO> request = updateAppointmentReqBuilder.buildRequest(apptDto);

		String amdocsResp = dao.call(url, HttpMethod.PUT, request);
		return amdocsResponseMapper.mapResponse(Layer7RequestTypes.UPDATE_APPOINTMENT, amdocsResp);
	}

	private Map<String, Object> populateAppointmentDTOType(Layer7AppointmentDTO apptDto) {

		Map<String, Object> errMap = null;
		String customerId = apptDto.getCustomerId();

		//
		// first fetch any static items
		//
		log.info("Reading static data to complete the request parameters");
		StaticDataVO staticVO = staticDataFactory.callForVO(new GetRequestDTO());

		errMap = staticVO.generateErrorMap();
		if(errMap != null) return errMap;
		apptDto.consume(staticVO);

		//
		// now call getCustomerProfile to fill in the address & contact blanks
		//
		log.info("Calling customer profile to complete the request parameters");
		CustomerProfileVO custProfVO =
				customerProfileFactory.callForVO(new GetRequestDTO(customerId, ID_TYPE.CUSTOMER_ID));

		errMap = custProfVO.generateErrorMap();
		if(errMap != null) return errMap;
		apptDto.consume(custProfVO);

		//
		// now call assignedProducts & assignedProductDetails to fill in the CPE blanks
		//
		log.debug("Calling assigned products to complete the request parameters");

		GetRequestDTO productsDTO = new GetRequestDTO(customerId, ID_TYPE.CUSTOMER_ID);
		AssignedProductsVO productsVO = assignedProductsFactory.callForVO(productsDTO);

		errMap = productsVO.generateErrorMap();
		if(errMap != null) return errMap;

		// TODO consider removing the cast by adding callForVO to the base
		log.info("Calling assigned product details to complete the request parameters");
		AssignedProductDetailsVO productDetailsVO = ((AssignedProductDetails) assignedProductDetailsFactory.
				with(productsVO)).callForVO(productsDTO);

		errMap = productDetailsVO.generateErrorMap();
		if(errMap != null) return errMap;
		apptDto.consume(productDetailsVO);

		//
		// now call networkPathDetails to fill in the ISW blanks
		//
		log.info("Calling network path details to complete the request parameters");
		NetworkPathDetailsDTO networkDto = new NetworkPathDetailsDTO();
		networkDto.setBusinessTransactionId(apptDto.getBusinessTransactionId());
		networkDto.setHouseNumber(custProfVO.getHouseNumber());
		networkDto.setHouseLetter(custProfVO.getFlat());
		networkDto.setPostCode(custProfVO.getPostCode());

		NetworkPathDetailsVO networkPathVO =
				networkPathDetailsFactory.callForVO(networkDto);

		errMap = networkPathVO.generateErrorMap();
		if(errMap != null) return errMap;
		apptDto.consume(networkPathVO);

		apptDto.setCaseTitle(
			String.format("%s/%s/%s av",
					networkPathVO.summary(),
					apptDto.getCaseType(),
					apptDto.getAvailableStartDate().substring(5, 10)
			)
		);

		// by now this should always be null
		return errMap;
	}


	// this is a nasty switcheroo needed because SC is expecting to see the accountId in the customerId field
	// always leave this until just before the message is sent to avoid spoiling the data
	private void swapCustomerId(Layer7AppointmentDTO apptDto) {

		log.info("setting customerId [{}] to accountId [{}]", apptDto.getCustomerId(), apptDto.getAccountId());
		apptDto.setCustomerId(apptDto.getAccountId());
	}
}
package jacada.addFixedProxy.service.layer7.rest.dataFactory;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jacada.addFixedProxy.dto.layer7.Layer7AssignedProductsDTO;
import jacada.addFixedProxy.dto.layer7.Layer7SharedTypes;
import jacada.addFixedProxy.dto.proxy.layer7.GetRequestDTO;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.VODataPathsBean.AssignedProductVOPaths;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AssignedProducts extends DataFactory<GetRequestDTO, Layer7AssignedProductsDTO> {

	@Autowired AssignedProductVOPaths apData;

	@Override
	public String call(GetRequestDTO dto) {

		String url = dto.generateUrl(layer7Properties.getAssignedProductsEP());

		log.debug("Handling assignedProducts");
		return post(url, new Layer7AssignedProductsDTO(dto));
	}

	public AssignedProductsVO callForVO(GetRequestDTO dto) {

		return new AssignedProductsVO(call(dto));
	}

	@Data
	@ToString(callSuper = true)
	@EqualsAndHashCode(callSuper = false)
	public class AssignedProductsVO extends Layer7DataVO {

		private String assignedProductIds = "";
		private List<String> names;
		private List<String> dates;

		/**
		 ** Setup up a parser on the string return and read off the interesting fields
		 **
		 * @param svcRespStr
		 *          the AssignedProducts response
		 **/
		public AssignedProductsVO(String svcRespStr) {

			super(svcRespStr);

			List<String> assignedProductIdList = svcRespDoc.read(apData.getAssignedProductIds(), Layer7SharedTypes.StringArrTypeRef);
			int sz = assignedProductIdList.size();
			log.trace("{} assignedProductIds found, creating a comma separated list", sz);
			if(sz > 0) {
				for (int i=0; i<sz-1; i++) {
					assignedProductIds = assignedProductIds.
							concat(assignedProductIdList.get(i)).
							concat(",");
				}
				assignedProductIds = assignedProductIds.
						concat(assignedProductIdList.get(sz-1));
			}

			names = svcRespDoc.read(apData.getAssignedProductNames(), Layer7SharedTypes.StringArrTypeRef);
			dates = svcRespDoc.read(apData.getAssignedProductDates(), Layer7SharedTypes.StringArrTypeRef);
		}
	}
}
package jacada.addFixedProxy.service.layer7.rest;

import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.StringUtils;

import jacada.addFixedProxy.UtilBeans.DateHandler;
import jacada.addFixedProxy.dto.layer7.Layer7RequestDTO;
import jacada.addFixedProxy.dto.layer7.Layer7XmlRequestDTO;
import jacada.addFixedProxy.properties.Layer7Properties;

@Component
public class Layer7HeadersHelper {

	@Autowired private Layer7Properties 	properties;
	@Autowired private DateHandler 			dateHandler;

	private static int REQ_ID 				= 0;

	private final String authHdr			= "Authorization";
	private final String dealerHdr			= "DealerCode";
	private final String msgIdHdr			= "MessageID";
	private final String contentTypeHeader	= "Content-Type";
	private final String acceptHdr			= "Accept";
	private final String json				= "application/json";
	private final String xml				= "application/xml";
	private final String requestPrefix		= "ADT_FIXED";

	private String user;
	private String password;
	private LinkedMultiValueMap<String, String> headers;

	/**
	 * Standard amdocs header set
	 *
	 * @param dto
	 * @return the typed HttpEntity with the headers.
	 */
	public LinkedMultiValueMap<String, String> buildHeaders(Layer7RequestDTO dto) {

		var hdrs = buildHeadersBase();
		hdrs.add(contentTypeHeader, json);
		hdrs.add(acceptHdr, 		json);

		return hdrs;
	}
	public LinkedMultiValueMap<String, String> buildHeaders(Layer7XmlRequestDTO dto) {

		var hdrs = buildHeadersBase();
		hdrs.add(contentTypeHeader, xml);
		hdrs.add(acceptHdr, 		xml);

		return hdrs;
	}

	private LinkedMultiValueMap<String, String> buildHeadersBase() {

		if(headers == null) {
			headers 	= new LinkedMultiValueMap<>();
			user 		= properties.getLayer7User();
			password	= properties.getLayer7Password();

			String basic = basicAuth();
			if(StringUtils.hasText(basic)) {
				headers.add(authHdr, basic);
			}

			headers.add(dealerHdr, properties.getDealerCode());
		}

		LinkedMultiValueMap<String, String> hdrs = headers.clone();
		hdrs.add(msgIdHdr, NEXT_MSG_ID());

		return hdrs;
	}

	public String basicAuth() {

		return String.format("Basic %s", Base64.getEncoder().
						encodeToString(String.format("%s:%s", user, password).
						getBytes()));
	}

	public String NEXT_MSG_ID() {

		return String.format("%s:%s:%07d", requestPrefix, dateHandler.today(), NEXT_MSG_CT());
	}

	// only update the count here
	private int NEXT_MSG_CT() {

		return  ++REQ_ID;
	}
}
package jacada.addFixedProxy.service.layer7.rest.dataFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;

@Configuration
class VODataPathsBean {

	@Getter
  class CustomerProfileVOPaths {
		private final String customerType 	= "$.data[0].customerData.customerType";
		private final String houseNumber 	= "$.data[0].customerData.address.houseNumber";
		private final String flat 			= "$.data[0].customerData.address.flat";
		private final String street 		= "$.data[0].customerData.address.streetName";
		private final String city			= "$.data[0].customerData.address.city";
		private final String zipCode 		= "$.data[0].customerData.address.zipCode";

		private final String contactId 		= "$.data[0].contactData.contactId";
		private final String phone 			= "$.data[0].contactData.phone";
		private final String firstName		= "$.data[0].contactData.personData.firstName";
		private final String mobile 		= "$.data[0].contactData.alternativeContactMethod[?(@.method=='M')].value";
		private final String middleName		= "$.data[0].contactData.personData.middleName";
		private final String lastName		= "$.data[0].contactData.personData.lastName";
		private final String companyName	= "$.data[0].companyData.companyName";
		private final String accountId 		= "$.data[0].companyData.organizationId";
	}

	@Getter
	class AppointmentDetailsVOPaths {

		private final String statusCode 			= "statusCode";
		private final String appointmentDetails 	= "appointmentDetails";

		private final String appointmentId 			= "appointmentDetails.appointmentId";
		private final String availableStartDate 	= "appointmentDetails.availableStartDate";
		private final String availableFinishDate 	= "appointmentDetails.availableFinishDate";
		private final String status 				= "appointmentDetails.status";
		private final String caseId 				= "appointmentDetails.caseId";
		private final String caseTitle 				= "appointmentDetails.caseTitle";
		private final String caseType 				= "appointmentDetails.caseType";
		private final String houseFlatNumber 		= "appointmentDetails.houseFlatNumber";
		private final String houseFlatExt 			= "appointmentDetails.houseFlatExt";
		private final String streetName 			= "appointmentDetails.streetName";
		private final String postCode 				= "appointmentDetails.postCode";
		private final String city 					= "appointmentDetails.city";
		private final String contactName 			= "appointmentDetails.contactName";
		private final String contactPhoneNumber 	= "appointmentDetails.contactPhoneNumber";
		private final String customerName 			= "appointmentDetails.customerName";
		private final String cusMobileNumber 		= "appointmentDetails.cusMobileNumber";
		private final String alternatePhoneNo 		= "appointmentDetails.alternatePhoneNo";
		private final String products 				= "appointmentDetails.products";
		private final String installedProducts 		= "appointmentDetails.installedProducts";
		private final String cpeInfo 				= "appointmentDetails.cpeInfo";
		private final String workorderType			= "appointmentDetails.workorderType";
		private final String workorderPriority 		= "appointmentDetails.workorderPriority";
		private final String customerId 			= "appointmentDetails.customerId";
		private final String note 					= "appointmentDetails.note";
		private final String hub 					= "appointmentDetails.hub";
		private final String node 					= "appointmentDetails.node";
		private final String groupAmplifier 		= "appointmentDetails.groupAmplifier";
		private final String endAmplifier 			= "appointmentDetails.endAmplifier";
		private final String multitap 				= "appointmentDetails.multitap";
		private final String tapPosition 			= "appointmentDetails.tapPosition";
		private final String CMTS 					= "appointmentDetails.CMTS";
		private final String webcard 				= "appointmentDetails.webcard";
		private final String startNoEarlierThan 	= "appointmentDetails.startNoEarlierThan";
	}

	@Getter
	class NetworkPathDetailsVOPaths {

		private final String network			 	= "$.Body.NID.NetworkPath";
	}

	@Getter
	class AssignedProductDetailsVOPaths {

		private final String macAddress_TVPath
							= "$.data[0].getAssignedProductDetailsOutputData"
							+ "..childImplementedProducts[?(@.catalogProductCod == 'TV_HARDWARE')]"
							+ ".childImplementedProducts[?(@.catalogProductCod == 'STANDAARD_MEDIABOX_CI')]"
							+ ".implementedAttributes[?(@.catalogAttribute.catalogAttributeDetails.propertyNameX1=='STB_MAC_ADRES')].selectedValue";

		private final String macAddress_SharedPath
							= "$.data[0].getAssignedProductDetailsOutputData"
							+ "..childImplementedProducts[?(@.catalogProductCod == 'CPE')]"
							+ ".childImplementedProducts[?(@.catalogProductCod == 'EMTA')]"
							+ ".implementedAttributes[?(@.catalogAttribute.catalogAttributeDetails.propertyNameX1=='CM_MAC_ADRES')].selectedValue";

		private final String brand_TVPath
							= "$.data[0].getAssignedProductDetailsOutputData"
							+ "..childImplementedProducts[?(@.catalogProductCod == 'TV_HARDWARE')]"
							+ ".childImplementedProducts[?(@.catalogProductCod == 'STANDAARD_MEDIABOX_CI')]"
							+ ".implementedAttributes[?(@.catalogAttribute.catalogAttributeDetails.propertyNameX1=='MERK')].selectedValue";

		private final String brand_SharedPath
							= "$.data[0].getAssignedProductDetailsOutputData"
							+ "..childImplementedProducts[?(@.catalogProductCod == 'CPE')]"
							+ ".childImplementedProducts[?(@.catalogProductCod == 'EMTA')]"
							+ ".implementedAttributes[?(@.catalogAttribute.catalogAttributeDetails.propertyNameX1=='MERK')].selectedValue";
	}

	@Getter
	class AssignedProductVOPaths {

		private final String assignedProductIds 	= "$.data[0].searchAssignedProductOutputData.SearchAssignedProductData"
													+ "[?(@.assignedProductHeader.productCode in ['TV','FI','FV','GEDEELDE_HARDWARE'])].assignedProductId";
		private final String assignedProductNames 	= "$.data[0].searchAssignedProductOutputData.SearchAssignedProductData"
													+ "[?(@.assignedProductHeader.productCode in ['TV','FI','FV'])]"
													+ ".assignedProductHeader.simpleCatalogOfferName";
		private final String assignedProductDates 	= "$.data[0].searchAssignedProductOutputData.SearchAssignedProductData"
													+ "[?(@.assignedProductHeader.productCode in ['TV','FI','FV'])]"
													+ ".assignedProductHeader.effectiveSinceDate";
	}

	@Getter
	class BillingArrangementDetailsVOPaths {
		private final String contactIdPath 				= "$.data[0].billingArrangementDetails[0].contactData.contactId";
		private final String paymentMethodPath 			= "$.data[0].billingArrangementDetails[0].paymentMethod";
		private final String billingArrangementIdPath 	= "$.data[0].billingArrangementDetails[0].billingAccountIds.billingArragementId";
		private final String payMeansIdPath 			= "$.data[0].billingArrangementDetails[0].billingAccountIds.payMeansId";
		private final String barStatusPath 				= "$.data[0].billingArrangementDetails[0].barStatus";
		private final String cashPath 					= "$.data[0].billingArrangementDetails[0].cash";
		private final String bankAccountNamePath 		= "$.data[0].billingArrangementDetails[0].bankDetails.bankAccountName";
		private final String bankAccountNumberPath 		= "$.data[0].billingArrangementDetails[0].bankDetails.bankAccountNumber";
		private final String bankCodePath 				= "$.data[0].billingArrangementDetails[0].bankDetails.bankCode";
		private final String bankNamePath 				= "$.data[0].billingArrangementDetails[0].bankDetails.bankName";
		private final String branchCodePath 			= "$.data[0].billingArrangementDetails[0].bankDetails.branchCode";
		private final String invoiceAddressPath			= "$.data[0].billingArrangementDetails[0].contactData.contactId";
	}

	@Getter
	class BillingEntitiesVOPaths {
		private final String houseNumberPath 			= "$.data[0].financialAccountData[0].billingArrangementDataList[0].barAddress.houseNumber";
		private final String flatPath 					= "$.data[0].financialAccountData[0].billingArrangementDataList[0].barAddress.flat";
		private final String additionPath 				= "$.data[0].financialAccountData[0].billingArrangementDataList[0].barAddress.addition";
		private final String barIdPath					= "$.data[0].financialAccountData[0].billingArrangementDataList[0].barID";
		private final String barNamePath				= "$.data[0].financialAccountData[0].billingArrangementDataList[0].barName";
	}

	@Bean
	public CustomerProfileVOPaths customerProfileVOPaths() {
		return new CustomerProfileVOPaths();
	}

	@Bean
	public AppointmentDetailsVOPaths appointmentDetailsVOPaths() {
		return new AppointmentDetailsVOPaths();
	}

	@Bean
	public NetworkPathDetailsVOPaths networkPathDetailsVOPaths() {
		return new NetworkPathDetailsVOPaths();
	}

	@Bean
	public AssignedProductDetailsVOPaths assignedProductDetailsVOPaths() {
		return new AssignedProductDetailsVOPaths();
	}

	@Bean
	public AssignedProductVOPaths assignedProductVOPaths() {
		return new AssignedProductVOPaths();
	}

	@Bean
	public BillingArrangementDetailsVOPaths billingArrangementDetailsVOPaths() {
		return new BillingArrangementDetailsVOPaths();
	}

	@Bean
	public BillingEntitiesVOPaths billingEntitiesVOPaths() {
		return new BillingEntitiesVOPaths();
	}
}
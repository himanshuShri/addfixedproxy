package jacada.addFixedProxy.service.layer7.rest.dataFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jacada.addFixedProxy.dto.layer7.CPEDetailsBody;
import jacada.addFixedProxy.dto.layer7.ISWRequestDTO;
import jacada.addFixedProxy.dto.proxy.layer7.CPEDetailsDTO;
import jacada.addFixedProxy.service.layer7.rest.Layer7ISWHelper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CPEDetails extends DataFactory<CPEDetailsDTO, ISWRequestDTO> {

	@Autowired Layer7ISWHelper iswHelper;

	@Override
	public String call(CPEDetailsDTO dto) {

		String url 				= dto.generateUrl(layer7Properties.getCpeDetailsEP());
		ISWRequestDTO iswDto 	= iswHelper.buildISWBase(dto);
		iswDto.setBody(new CPEDetailsBody(dto));

		log.debug("Handling CPE Details");
		return post(url, iswDto);
	}
}
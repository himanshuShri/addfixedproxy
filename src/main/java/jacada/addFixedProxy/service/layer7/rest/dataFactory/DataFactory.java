package jacada.addFixedProxy.service.layer7.rest.dataFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;

import jacada.addFixedProxy.dto.layer7.Layer7RequestDTO;
import jacada.addFixedProxy.dto.proxy.layer7.ProxyLayer7RequestDTO;
import jacada.addFixedProxy.properties.Layer7Properties;
import jacada.addFixedProxy.service.RestServiceDAO;
import jacada.addFixedProxy.service.layer7.rest.Layer7RequestBuilder;

public abstract class DataFactory<T extends ProxyLayer7RequestDTO, P extends Layer7RequestDTO> extends ProxyLayer7RequestDTO {

	@Autowired private RestServiceDAO dao;
	@Autowired private Layer7RequestBuilder<P> layer7RequestBuilder;
	@Autowired protected Layer7Properties layer7Properties;

	public abstract String call(T vo) throws Exception;

	public String post(String url, P layer7Dto) {
		return access(url, HttpMethod.POST, layer7RequestBuilder.buildRequest(layer7Dto));
	}

	public String put(String url, P layer7Dto) {
		return access(url, HttpMethod.PUT, layer7RequestBuilder.buildRequest(layer7Dto));
	}

	public String get(String url, P layer7Dto) {
		return access(url, HttpMethod.GET, layer7RequestBuilder.buildRequest(layer7Dto));
	}

	private String access(String url, HttpMethod method, HttpEntity<P> entity) {
		return dao.call(url, method, entity);
	}
}
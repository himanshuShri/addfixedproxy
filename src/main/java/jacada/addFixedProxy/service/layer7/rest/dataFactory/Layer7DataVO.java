package jacada.addFixedProxy.service.layer7.rest.dataFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
@NoArgsConstructor
public class Layer7DataVO {

	public DocumentContext svcRespDoc;

	public boolean success;
	private int svcStatus;
	private List<Object> svcError;

	public Layer7DataVO(String svcRespStr) {
		svcRespDoc 	= JsonPath.parse(svcRespStr);
		svcStatus 	= svcRespDoc.read("status");
		svcError 	= svcRespDoc.read("error");
		success 	= svcStatus == 200 && svcError.isEmpty();
	}

	public Map<String, Object> generateErrorMap() {

		Map<String, Object> m = null;

		if (!isSuccess()) {
			log.error("Error in internal call, aborting REST service call");

			m = new HashMap<>();
			m.put("Internal error", svcError);
			m.put("Internal status", svcStatus);
		}

		return m;
	}
}
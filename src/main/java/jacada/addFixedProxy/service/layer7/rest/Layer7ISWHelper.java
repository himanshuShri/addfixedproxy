package jacada.addFixedProxy.service.layer7.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jacada.addFixedProxy.UtilBeans.DateHandler;
import jacada.addFixedProxy.dto.layer7.ISWRequestDTO;
import jacada.addFixedProxy.dto.layer7.ISWRequestDTO.ISWRequestHeader;
import jacada.addFixedProxy.dto.layer7.ISWRequestDTO.ISWRequestHeaderContext;
import jacada.addFixedProxy.dto.proxy.layer7.ProxyLayer7ISWRequestDTO;
import jacada.addFixedProxy.properties.Layer7Properties;

@Component
public class Layer7ISWHelper {

	@Autowired private Layer7Properties properties;
	@Autowired private DateHandler dateHandler;

	private ISWRequestHeaderContext context = null;

	/**
	 * Standard ISW request format:
	 *
	 *
		{
		  "Header": {
		    "BusinessTransactionID": "43567384",
		    "SentTimestamp": "2019-01-31T17:06:00+02:00",
		    "SourceContext": {
		      "Host": "hostname.vodafoneziggo.com",
		      "Application": "Sigma SOM"
		    }
		  },
		  "Body": <REQUEST SPECIFIC>
		  }
		}
	 *
	 * where only the body payload differs between services.
	 *
	 *
	 * @param dto
	 * @return the typed HttpEntity with the headers.
	 */
	public ISWRequestDTO buildISWBase(ProxyLayer7ISWRequestDTO incoming) {

		ISWRequestDTO dto = new ISWRequestDTO();
		if(context == null) {
			context = new ISWRequestDTO.ISWRequestHeaderContext();
			context.setHost(properties.getHost());
			context.setApplication(properties.getChannel());
		}

		ISWRequestHeader hdr = new ISWRequestDTO.ISWRequestHeader();
		hdr.setSentTimestamp(dateHandler.gregorianNow());
		hdr.setBusinessTransactionID(incoming.getBusinessTransactionId());
		hdr.setSourceContext(context);

		dto.setHeader(hdr);
		return dto;
	}
}
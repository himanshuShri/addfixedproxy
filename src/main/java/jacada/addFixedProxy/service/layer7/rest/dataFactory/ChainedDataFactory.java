package jacada.addFixedProxy.service.layer7.rest.dataFactory;

import jacada.addFixedProxy.dto.layer7.Layer7RequestDTO;
import jacada.addFixedProxy.dto.proxy.layer7.ProxyLayer7RequestDTO;

public abstract class ChainedDataFactory
	<T extends ProxyLayer7RequestDTO, P extends Layer7RequestDTO, V extends Layer7DataVO>
	extends	DataFactory<T, P> {

	V withVO;

	public ChainedDataFactory<T, P, V> with(V vo) {

		withVO = vo;
		return this;
	}
}
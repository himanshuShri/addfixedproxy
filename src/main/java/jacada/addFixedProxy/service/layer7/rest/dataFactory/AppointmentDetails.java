package jacada.addFixedProxy.service.layer7.rest.dataFactory;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jayway.jsonpath.JsonPath;

import jacada.addFixedProxy.dto.layer7.Layer7EmptyRequestDTO;
import jacada.addFixedProxy.dto.layer7.Layer7SharedTypes;
import jacada.addFixedProxy.dto.proxy.layer7.GetRequestDTO;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.VODataPathsBean.AppointmentDetailsVOPaths;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AppointmentDetails extends DataFactory<GetRequestDTO, Layer7EmptyRequestDTO> {

	@Autowired AppointmentDetailsVOPaths apptData;

	@Override
	public String call(GetRequestDTO dto) {

		String url = dto.generateUrl(layer7Properties.getAppointmentEP());

		log.debug("Handling appointmentDetails");
		return get(url, new Layer7EmptyRequestDTO());
	}

	public AppointmentDetailsVO callForVO(GetRequestDTO dto) {

		return new AppointmentDetailsVO(call(dto));
	}

	@Data
	@ToString(callSuper = true)
	@EqualsAndHashCode(callSuper = false)
	public class AppointmentDetailsVO extends Layer7DataVO {

		private String appointmentId;
		private String availableStartDate;
		private String availableFinishDate;
		private String status;
		private String caseId;
		private String caseTitle;
		private String caseType;
		private String houseFlatNumber;
		private String houseFlatExt;
		private String streetName;
		private String postCode;
		private String city;
		private String contactName;
		private String contactPhoneNumber;
		private String customerName;
		private String cusMobileNumber;
		private String alternatePhoneNo;
		private Layer7SharedTypes.ProductWrapper products;
		private Layer7SharedTypes.InstalledProductWrapper installedProducts;
		private Layer7SharedTypes.CPEInfoWrapper cpeInfo;
		private String workorderType;
		private String workorderPriority;
		private String customerId;
		private String note;
		private String hub;
		private String node;
		private String groupAmplifier;
		private String endAmplifier;
		private String multitap;
		private String tapPosition;
		private String CMTS;
		private String webcard;
		private String startNoEarlierThan;

		/**
		 ** Setup up a parser on the string return and read off the interesting fields
		 **
		 * @param svcRespStr
		 *          the AssignedProductDetails response
		 **/
		public AppointmentDetailsVO(String svcRespStr) {

			svcRespDoc 					= JsonPath.parse(svcRespStr);

			String statusCode			= svcRespDoc.read(apptData.getStatusCode());
			Map<String, Object> apptDet = svcRespDoc.read(apptData.getAppointmentDetails());
			success						= false;

			if (apptDet != null && !apptDet.isEmpty()) {
				success						= Integer.parseInt(statusCode) == 200;

				appointmentId 				= svcRespDoc.read(apptData.getAppointmentId());
				availableStartDate 			= svcRespDoc.read(apptData.getAvailableStartDate());
				availableFinishDate 		= svcRespDoc.read(apptData.getAvailableFinishDate());
				status 						= svcRespDoc.read(apptData.getStatus());
				caseId 						= svcRespDoc.read(apptData.getCaseId());
				caseTitle 					= svcRespDoc.read(apptData.getCaseTitle());
				caseType 					= svcRespDoc.read(apptData.getCaseType());
				houseFlatNumber 			= svcRespDoc.read(apptData.getHouseFlatNumber());
				houseFlatExt 				= svcRespDoc.read(apptData.getHouseFlatExt());
				streetName 					= svcRespDoc.read(apptData.getStreetName());
				postCode 					= svcRespDoc.read(apptData.getPostCode());
				city 						= svcRespDoc.read(apptData.getCity());
				contactName 				= svcRespDoc.read(apptData.getContactName());
				contactPhoneNumber 			= svcRespDoc.read(apptData.getContactPhoneNumber());
				customerName 				= svcRespDoc.read(apptData.getCustomerName());
				cusMobileNumber 			= svcRespDoc.read(apptData.getCusMobileNumber());
				alternatePhoneNo 			= svcRespDoc.read(apptData.getAlternatePhoneNo());
				products 					= svcRespDoc.read(apptData.getProducts(),
																Layer7SharedTypes.ProductWrapper.class);
				installedProducts 			= svcRespDoc.read(apptData.getInstalledProducts(),
																Layer7SharedTypes.InstalledProductWrapper.class);
				cpeInfo 					= svcRespDoc.read(apptData.getCpeInfo(),
																Layer7SharedTypes.CPEInfoWrapper.class);
				workorderType 				= svcRespDoc.read(apptData.getWorkorderType());
				workorderPriority 			= svcRespDoc.read(apptData.getWorkorderPriority());
				customerId 					= svcRespDoc.read(apptData.getCustomerId());
				note 						= svcRespDoc.read(apptData.getNote());
				hub 						= svcRespDoc.read(apptData.getHub());
				node 						= svcRespDoc.read(apptData.getNode());
				groupAmplifier 				= svcRespDoc.read(apptData.getGroupAmplifier());
				endAmplifier 				= svcRespDoc.read(apptData.getEndAmplifier());
				multitap 					= svcRespDoc.read(apptData.getMultitap());
				tapPosition 				= svcRespDoc.read(apptData.getTapPosition());
				CMTS 						= svcRespDoc.read(apptData.getCMTS());
				webcard 					= svcRespDoc.read(apptData.getWebcard());
				startNoEarlierThan 			= svcRespDoc.read(apptData.getStartNoEarlierThan());
			}
		}
	}
}
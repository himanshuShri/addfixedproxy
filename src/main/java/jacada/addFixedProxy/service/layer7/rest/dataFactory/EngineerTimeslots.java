package jacada.addFixedProxy.service.layer7.rest.dataFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jacada.addFixedProxy.UtilBeans.DateHandler;
import jacada.addFixedProxy.dto.layer7.Layer7ReserveTimeslotsDTO;
import jacada.addFixedProxy.dto.proxy.layer7.ReserveTimeslotsDTO;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.CustomerProfile.CustomerProfileVO;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EngineerTimeslots extends DataFactory<ReserveTimeslotsDTO, Layer7ReserveTimeslotsDTO> {

	@Autowired private DateHandler dates;
	private CustomerProfileVO customerProfile;

	@Override
	public String call(ReserveTimeslotsDTO dto) throws Exception {

		String url = dto.generateUrl(layer7Properties.getReserveTimeslotsEP());

		// create a skeleton request from what was passed over
		Layer7ReserveTimeslotsDTO slotsDto = new Layer7ReserveTimeslotsDTO(dto);

		// add in any static fields
		slotsDto.setDesireStartDate(dates.inNDays(dto.getDaysToOffset()));

		slotsDto.setHouseFlatNumber(customerProfile.getHouseNumber());
		slotsDto.setPostCode(customerProfile.getPostCode());
		slotsDto.setStreetName(customerProfile.getStreet());
		slotsDto.setCity(customerProfile.getCity());

		// set this to null now to prevent accidental re-use
		customerProfile = null;

		log.debug("Handling Engineer Timeslots");
		return post(url, slotsDto);
	}

	public EngineerTimeslots with(CustomerProfileVO custProfVO) {

		customerProfile = custProfVO;
		return this;
	}
}
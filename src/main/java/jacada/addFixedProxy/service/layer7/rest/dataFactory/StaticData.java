package jacada.addFixedProxy.service.layer7.rest.dataFactory;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jacada.addFixedProxy.UtilBeans;
import jacada.addFixedProxy.dto.proxy.layer7.GetRequestDTO;
import jacada.addFixedProxy.properties.Layer7Properties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Service
public class StaticData {

	@Autowired private Layer7Properties properties;
	@Autowired private UtilBeans.DateHandler dates;
	private static StaticDataVO instance = null;

	public StaticDataVO callForVO(GetRequestDTO dummy) {

		instance = instance == null? new StaticDataVO() : instance;
		return instance;
	}

	@Data
	@ToString(callSuper = true)
	@EqualsAndHashCode(callSuper = false)
	public class StaticDataVO extends Layer7DataVO {

		private String cxlStatus	= "Cancelled";
		private String channel 		= properties.getChannel();
		private String today		= dates.today();

		@Override
		// there's no http service call here
		public Map<String, Object> generateErrorMap() {
			return null;
		}
	}
}
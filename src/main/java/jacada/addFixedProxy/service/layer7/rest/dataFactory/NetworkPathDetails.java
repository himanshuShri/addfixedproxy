package jacada.addFixedProxy.service.layer7.rest.dataFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.jayway.jsonpath.JsonPath;

import jacada.addFixedProxy.dto.layer7.ISWRequestDTO;
import jacada.addFixedProxy.dto.layer7.NetworkPathDetailsBody;
import jacada.addFixedProxy.dto.proxy.layer7.NetworkPathDetailsDTO;
import jacada.addFixedProxy.service.layer7.rest.Layer7ISWHelper;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.VODataPathsBean.NetworkPathDetailsVOPaths;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
public class NetworkPathDetails extends DataFactory<NetworkPathDetailsDTO, ISWRequestDTO> {

	@Autowired NetworkPathDetailsVOPaths npdData;
	@Autowired Layer7ISWHelper	iswHelper;

	@Override
	public String call(NetworkPathDetailsDTO dto) {

		String url 				= dto.generateUrl(layer7Properties.getNetworkPathDetailsEP());
		ISWRequestDTO iswDto	= iswHelper.buildISWBase(dto);
		iswDto.setBody(new NetworkPathDetailsBody(dto));

		log.debug("Handling networkPathDetails");
		return post(url, iswDto);
	}

	public NetworkPathDetailsVO callForVO(NetworkPathDetailsDTO dto) {

		return new NetworkPathDetailsVO(call(dto));
	}

	@Data
	@ToString(callSuper = true)
	@EqualsAndHashCode(callSuper = false)
	public class NetworkPathDetailsVO extends Layer7DataVO {

		private String hub;
		private String node;
		private String grpAmplifier;
		private String endAmplifier;
		private String multiTap;
		private String tapPosn;

		/**
		 ** Setup up a parser on the string return and read off the interesting fields
		 **
		 **
		 ** Notes from Jan, Feb '19
		 **
				Hub	--> NID.NetworkPath until 1st dot
				sample fUPC networkPath: AD00.0000020.1A.001.01.5
				sample fUPC Hub: AD00
				sample fZiggo networkPath: 350.004.00.01.0.06
				sample fZiggo Hub: 350"
				Node --> "NID.NetworkPath between 1st and 2nd dot
				sample fUPC networkPath: AD00.0000020.1A.001.01.5
				sample fUPC Node: 0000020
				sample fZiggo networkPath: 350.004.00.01.0.06
				sample fZiggo Node: 004"
				Group amplifier	--> "NID.NetworkPath between 2nd and 3rd dot
				sample fUPC networkPath: AD00.0000020.1A.001.01.5
				sample fUPC Group Amplifier: 1A
				sample fZiggo networkPath: 350.004.00.01.0.06
				sample fZiggo Group Amplifier: 00"
				End amplifier	--> "NID.NetworkPath between 3rd and 4th dot
				sample fUPC networkPath: AD00.0000020.1A.001.01.5
				sample fUPC End Amplifier: 001
				sample fZiggo networkPath: 350.004.00.01.0.06
				sample fZiggo End Amplifier: 01"
				Multitap	--> "Multitap.MTRef
				or
				NID.NetworkPath between 4th and 5ft dot
				sample fUPC networkPath: AD00.0000020.1A.001.01.5
				sample fUPC Multitap: 01
				sample fZiggo networkPath: 350.004.00.01.0.06
				sample fZiggo Multitap: 0"
				Tap position	--> "Multitap.Tap
				or
				NID.NetworkPath after 5ft dot
				sample fUPC networkPath: AD00.0000020.1A.001.01.5
				sample fUPC Tap: 5
				sample fZiggo networkPath: 350.004.00.01.0.06
				sample fZiggo Tap: 06"
		 **
		 **
		 ** @param svcRespStr
		 **          the NetworkPathDetails response
		 */
		public NetworkPathDetailsVO(String svcRespStr) {

			svcRespDoc = JsonPath.parse(svcRespStr);

			String networkpath = svcRespDoc.read(npdData.getNetwork());
			boolean ok = !StringUtils.isEmpty(networkpath);
			setSuccess(ok);

			log.debug("Parsing network path [{}]", networkpath);

			if(!ok) {
				log.error("All useful data is missing from this reponse, this operation will most likely fail now!");
				return;
			}

			String[] comp = networkpath.split("\\.");

			hub 			= comp[0];
			node 			= comp[1];
			grpAmplifier	= comp[2];
			endAmplifier	= comp[3];
			if(comp.length == 5) {
				log.debug("network path only has 5 components, splitting the last one ({}) into 2", comp[4]);
				multiTap 	= comp[4].substring(0, 1);
				tapPosn 	= comp[4].substring(1);
			} else {
				multiTap 	= comp[4];
				tapPosn 	= comp[5];
			}
		}

		public String summary() {
			return String.format("%s,%s,%s,%s,%s,%s",
					hub, node, grpAmplifier, endAmplifier, multiTap, tapPosn);
		}
	}
}
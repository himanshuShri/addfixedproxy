package jacada.addFixedProxy.service.layer7.rest.dataFactory;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jacada.addFixedProxy.dto.layer7.Layer7AssignedProductDetailsDTO;
import jacada.addFixedProxy.dto.layer7.Layer7SharedTypes;
import jacada.addFixedProxy.dto.proxy.layer7.GetRequestDTO;
import jacada.addFixedProxy.dto.proxy.layer7.GetRequestDTO.ID_TYPE;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.AssignedProducts.AssignedProductsVO;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.VODataPathsBean.AssignedProductDetailsVOPaths;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AssignedProductDetails extends ChainedDataFactory
	<GetRequestDTO, Layer7AssignedProductDetailsDTO, AssignedProductsVO> {

	@Autowired AssignedProductDetailsVOPaths apdData;

	@Override
	public String call(GetRequestDTO dto) {

		String url = dto.generateUrl(layer7Properties.getAssignedProductDetailsEP());
		String assProdIds = withVO == null ?
				dto.id(ID_TYPE.ASSIGNED_PRODUCT_ID) :
					withVO.getAssignedProductIds();

		// create a skeleton request from what was passed over
		GetRequestDTO assProdDetailsDto = new GetRequestDTO(
				assProdIds, ID_TYPE.ASSIGNED_PRODUCT_ID);

		// set this to null now to prevent accidental re-use
		withVO = null;

		log.debug("Handling assignedProductDetails");
		return post(url, new Layer7AssignedProductDetailsDTO(assProdDetailsDto));
	}

	public AssignedProductDetailsVO callForVO(GetRequestDTO dto) {

		return new AssignedProductDetailsVO(call(dto));
	}

	@Data
	@ToString(callSuper = true)
	@EqualsAndHashCode(callSuper = false)
	public class AssignedProductDetailsVO extends Layer7DataVO {

		private List<String> tvMacs, 	sharedMacs;
		private List<String> tvBrands,	sharedBrands;

		/**
		 ** Setup up a parser on the string return and read off the interesting fields
		 **
		 * @param svcRespStr
		 *          the AssignedProductDetails response
		 **/
		public AssignedProductDetailsVO(String svcRespStr) {

			super(svcRespStr);

			tvMacs			= svcRespDoc.read(apdData.getMacAddress_TVPath(),	 	Layer7SharedTypes.StringArrTypeRef);
			tvBrands		= svcRespDoc.read(apdData.getBrand_TVPath(), 			Layer7SharedTypes.StringArrTypeRef);
			sharedMacs		= svcRespDoc.read(apdData.getMacAddress_SharedPath(),	Layer7SharedTypes.StringArrTypeRef);
			sharedBrands	= svcRespDoc.read(apdData.getBrand_SharedPath(),		Layer7SharedTypes.StringArrTypeRef);
		}
	}
}
package jacada.addFixedProxy.service.layer7.rest.dataFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jacada.addFixedProxy.dto.layer7.Layer7BillingArrangementDetailsDTO;
import jacada.addFixedProxy.dto.layer7.Layer7SharedTypes.BillingAddress;
import jacada.addFixedProxy.dto.proxy.layer7.GetRequestDTO;
import jacada.addFixedProxy.dto.proxy.layer7.GetRequestDTO.ID_TYPE;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.BillingEntities.BillingEntitiesVO;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.VODataPathsBean.BillingArrangementDetailsVOPaths;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BillingArrangementDetails extends ChainedDataFactory
	<GetRequestDTO, Layer7BillingArrangementDetailsDTO, BillingEntitiesVO> {

	@Autowired BillingArrangementDetailsVOPaths barData;

	@Override
	public String call(GetRequestDTO dto) {

		String url = dto.generateUrl(layer7Properties.getBillingArrangementDetailsEP());

		// create a skeleton request from what was passed over
		GetRequestDTO baDetailsDto = new GetRequestDTO(withVO.getBarId(), ID_TYPE.BILLING_ARRANGEMENT_ID);

		// set this to null now to prevent accidental re-use
		withVO = null;

		log.debug("Handling billingArrangementDetails");
		return post(url, new Layer7BillingArrangementDetailsDTO(baDetailsDto));
	}

	public BillingArrangementDetailsVO callForVO(GetRequestDTO dto) {

		return new BillingArrangementDetailsVO(call(dto));
	}

	@Data
	@ToString(callSuper = true)
	@EqualsAndHashCode(callSuper = false)
	public class BillingArrangementDetailsVO extends Layer7DataVO {

		private String contactId;
		private String paymentMethod;
		private String billingArrangementId;
		private String payMeansId;
		private String barStatus;
		private boolean cash;
		private String bankAccountName;
		private String bankAccountNumber;
		private String bankCode;
		private String bankName;
		private String branchCode;
		private BillingAddress invoiceAddress;

		/**
		 ** Setup up a parser on the string return and read off the interesting fields
		 **
		 * @param svcRespStr
		 *          the AssignedProductDetails response
		 **/
		public BillingArrangementDetailsVO(String svcRespStr) {

			super(svcRespStr);

			contactId 				= svcRespDoc.read(barData.getContactIdPath());
			paymentMethod			= svcRespDoc.read(barData.getPaymentMethodPath());
			billingArrangementId	= svcRespDoc.read(barData.getBillingArrangementIdPath());
			payMeansId 				= svcRespDoc.read(barData.getPayMeansIdPath());
			barStatus 				= svcRespDoc.read(barData.getBarStatusPath());
			cash 					= svcRespDoc.read(barData.getCashPath());
			bankAccountName 		= svcRespDoc.read(barData.getBankAccountNamePath());

			bankAccountNumber 		= svcRespDoc.read(barData.getBankAccountNumberPath());
			bankCode 				= svcRespDoc.read(barData.getBankCodePath());
			bankName 				= svcRespDoc.read(barData.getBankNamePath());
			branchCode 				= svcRespDoc.read(barData.getBranchCodePath());
			invoiceAddress 			= svcRespDoc.read(barData.getInvoiceAddressPath(), BillingAddress.class);
		}
	}
}
package jacada.addFixedProxy.service.layer7.rest.dataFactory;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jacada.addFixedProxy.dto.layer7.Layer7EmptyRequestDTO;
import jacada.addFixedProxy.dto.layer7.Layer7SharedTypes;
import jacada.addFixedProxy.dto.proxy.layer7.GetRequestDTO;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.VODataPathsBean.CustomerProfileVOPaths;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CustomerProfile extends DataFactory<GetRequestDTO, Layer7EmptyRequestDTO> {

	@Autowired CustomerProfileVOPaths cpData;

	private String residentialCustomerType = "I";

	@Override
	public String call(GetRequestDTO dto) {

 		String url = dto.generateUrl(layer7Properties.getCustomerProfileEP());

		log.debug("Handling customerProfile");
		return get(url, new Layer7EmptyRequestDTO());
	}

	public CustomerProfileVO callForVO(GetRequestDTO dto) {

		return new CustomerProfileVO(call(dto));
	}

	@Data
	@ToString(callSuper = true)
	@EqualsAndHashCode(callSuper = false)
	public class CustomerProfileVO extends Layer7DataVO {

		private String houseNumber;
		private String postCode;
		private String street;
		private String city;
		private String flat;
		private String customerType;
		private boolean residential;

		private String accountId;
		private String contactId;
		private String phone;

		private List<String> mobiles;
		private String mobile;

		private String firstName;
		private String middleName;
		private String lastName;
		private String fullName;
		private String companyName;

		/**
		 ** Setup up a parser on the string return and read off the interesting fields
		 **
		 * @param svcRespStr
		 *          the CustomerProfile response
		 **/
		public CustomerProfileVO(String svcRespStr) {

			super(svcRespStr);

			houseNumber 	= svcRespDoc.read(cpData.getHouseNumber(), String.class);
			postCode 		= svcRespDoc.read(cpData.getZipCode());
			street 			= svcRespDoc.read(cpData.getStreet());
			city 			= svcRespDoc.read(cpData.getCity());
			flat 			= svcRespDoc.read(cpData.getFlat());
			customerType 	= svcRespDoc.read(cpData.getCustomerType());

			accountId 		= svcRespDoc.read(cpData.getAccountId(), String.class);
			contactId 		= svcRespDoc.read(cpData.getContactId(), String.class);
			phone 			= svcRespDoc.read(cpData.getPhone());

			List<String> mobiles
							= svcRespDoc.read(cpData.getMobile(), Layer7SharedTypes.StringArrTypeRef);
			mobile 			= mobiles.size() > 0 ? mobiles.get(0) : "";

			firstName 		= svcRespDoc.read(cpData.getFirstName());
			middleName 		= svcRespDoc.read(cpData.getMiddleName());
			lastName 		= svcRespDoc.read(cpData.getLastName());

			residential 	= residentialCustomerType.equals(customerType);
			companyName 	= svcRespDoc.read(cpData.getCompanyName());
			fullName 		= residential ? String.format("%s %s %s", firstName, middleName, lastName) : companyName;
		}
	}
}
package jacada.addFixedProxy.service.layer7.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;

import jacada.addFixedProxy.dto.layer7.Layer7XmlRequestDTO;

/**
 * A genericised class, each instance of which
 * builds requests of a specific type
 **/
@Component
public class Layer7XmlRequestBuilder<T extends Layer7XmlRequestDTO> {

	@Autowired private Layer7HeadersHelper hdrs;

	public HttpEntity<String> buildRequest(T dto) {

		LinkedMultiValueMap<String, String> headers = hdrs.buildHeaders(dto);
		HttpEntity<String> request = new HttpEntity<>(dto.body(), headers);

		return request;
	}
}
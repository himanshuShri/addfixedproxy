package jacada.addFixedProxy.service.layer7.rest.dataFactory;

import org.springframework.stereotype.Service;

import jacada.addFixedProxy.dto.layer7.Layer7EmptyRequestDTO;
import jacada.addFixedProxy.dto.proxy.layer7.GetRequestDTO;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ContactDetails extends DataFactory<GetRequestDTO, Layer7EmptyRequestDTO> {

	@Override
	public String call(GetRequestDTO dto) {

		String url = dto.generateUrl(layer7Properties.getContactDetailsEP());

		log.debug("Handling contactDetails");
		return post(url, new Layer7EmptyRequestDTO());
	}
}
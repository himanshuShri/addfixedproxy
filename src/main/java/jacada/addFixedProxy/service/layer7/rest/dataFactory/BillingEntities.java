package jacada.addFixedProxy.service.layer7.rest.dataFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jacada.addFixedProxy.dto.layer7.Layer7BillingEntitiesDTO;
import jacada.addFixedProxy.dto.proxy.layer7.GetRequestDTO;
import jacada.addFixedProxy.service.layer7.rest.dataFactory.VODataPathsBean.BillingEntitiesVOPaths;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BillingEntities extends DataFactory<GetRequestDTO, Layer7BillingEntitiesDTO> {

	@Autowired BillingEntitiesVOPaths beData;

	@Override
	public String call(GetRequestDTO dto) {

		String url = dto.generateUrl(layer7Properties.getBillingEntitiesEP());

		log.debug("Handling billingEntities");
		return post(url, new Layer7BillingEntitiesDTO());
	}

	public BillingEntitiesVO callForVO(GetRequestDTO dto) {

		return new BillingEntitiesVO(call(dto));
	}

	@Data
	@ToString(callSuper = true)
	@EqualsAndHashCode(callSuper = false)
	public class BillingEntitiesVO extends Layer7DataVO {

		private String 	barId;
		private String 	barName;
		private Integer houseNumber;
		private String 	flat, addition;

		/**
		 ** Setup up a parser on the string return and read off the interesting fields
		 **
		 * @param svcRespStr
		 *          the CustomerProfile response
		 **/
		public BillingEntitiesVO(String svcRespStr) {

			super(svcRespStr);

			barId 	= svcRespDoc.read(beData.getBarIdPath(), String.class);
			barName = svcRespDoc.read(beData.getBarNamePath());

			houseNumber = svcRespDoc.read(beData.getHouseNumberPath());
			flat 		= svcRespDoc.read(beData.getFlatPath());
			addition 	= svcRespDoc.read(beData.getAdditionPath());
		}
	}
}
package jacada.addFixedProxy.service.layer7.rest.dataFactory;

import org.springframework.stereotype.Service;

import jacada.addFixedProxy.dto.layer7.Layer7EmptyRequestDTO;
import jacada.addFixedProxy.dto.proxy.layer7.GetRequestDTO;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TicketDetails extends DataFactory<GetRequestDTO, Layer7EmptyRequestDTO> {

	@Override
	public String call(GetRequestDTO dto) {

		String url = dto.generateUrl(layer7Properties.getTicketDetailsEP());

		log.debug("Handling ticketDetails");
		return get(url, new Layer7EmptyRequestDTO());
	}
}
package jacada.addFixedProxy.service.layer7.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;

import jacada.addFixedProxy.dto.layer7.Layer7RequestDTO;

/**
 * A genericised class, each instance of which
 * builds requests of a specific type
 **/
@Component
public class Layer7RequestBuilder<T extends Layer7RequestDTO> {

	@Autowired private Layer7HeadersHelper hdrs;

	public HttpEntity<T> buildRequest(T dto) {

		LinkedMultiValueMap<String, String> headers = hdrs.buildHeaders(dto);
		HttpEntity<T> request = new HttpEntity<>(dto, headers);

		return request;
	}
}
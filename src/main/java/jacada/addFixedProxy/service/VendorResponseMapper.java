package jacada.addFixedProxy.service;

import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import jacada.addFixedProxy.dto.SimpleXMLNamespaceContext;
import jacada.addFixedProxy.service.adt.ADTRequestTypes;
import jacada.addFixedProxy.service.layer7.Layer7RequestTypes;
import lombok.extern.slf4j.Slf4j;

/**
 * Appropriately typed instances of this type are created in the
 * ResponseMapperFactory bean and then autowired into the service classes by
 * spring.
 *
 * @author ndoan
 * @param <P> the properties instance
 **/
@Slf4j
public class VendorResponseMapper<P extends ServiceProperties> {

	private DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
	private NamespaceContext somNs = new SimpleXMLNamespaceContext("som", "http://som.sigma.com");
	private XPathFactory xPathFactory;
	private DocumentBuilder builder;
	private P properties;

	public VendorResponseMapper(P props) throws ParserConfigurationException {

		docFactory.setNamespaceAware(true);
		builder = docFactory.newDocumentBuilder();
		xPathFactory = XPathFactory.newInstance();
		properties = props;
	}

	/**
	 * Find a format specific parser for the response and lookup the returnable fields.
	 * Using String as an input not only makes the logic provider agnositic, it also means that
	 * the response doesn't need to be marshalled by jackson only to be serialised again here.
	 *
	 * @return a map of requested fields to their object representations in the service response
	 **/
	public Map<String, Object> mapResponse(Layer7RequestTypes requestType, String svcResp) {

		log.debug("attempting to map {} response, using xml = {}", requestType.name(), requestType.parseAsXml());
		return requestType.parseAsXml() ?
				mapXMLResponse(requestType.name(), svcResp) :
				mapJSONResponse(requestType.name(), svcResp);
	}

	/**
	 * Use a JSON parser and lookup the returnable fields.
	 * Using String as an input not only makes the logic provider agnositic, it also means that
	 * the response doesn't need to be marshalled by jackson only to be serialised again here.
	 *
	 * @return a map of requested fields to their object representations in the service response
	 **/
	public Map<String, Object> mapResponse(ADTRequestTypes requestType, String svcResp) {

		return mapJSONResponse(requestType.name(), svcResp);
	}

	private Map<String, Object> mapJSONResponse(String requestType, String svcResp) {

		// parse the JSON and retrieve the fields
		DocumentContext doc = JsonPath.parse(svcResp);
		Map<String, String> paths = properties.outputPaths(requestType);

		// everything normalised from here on, pick the fields out of the document
		Map<String, Object> output = new HashMap<>();
		for (String p : paths.keySet()) {

			Object v = doc.read(paths.get(p));
			log.info("Resolved [{}] - {} to [{}]", p, paths.get(p), v);
			output.put(p, v);
		}

		return output;
	}

	private Map<String, Object> mapXMLResponse(String requestType, String svcResp) {

		InputSource is = new InputSource(new StringReader(svcResp));
		Document xmlDoc = null;
		try {
			xmlDoc = builder.parse(is);
		} catch (Exception e) {
			log.error("Could not parse response [{}]", svcResp);
		}

		// parse the XML and retrieve the fields
		Map<String, String> paths = properties.outputPaths(requestType);

		// everything normalised from here on, pick the fields out of the document
		Map<String, Object> output = new HashMap<>();
		for (String p : paths.keySet()) {

			XPath xPath = xPathFactory.newXPath();
			xPath.setNamespaceContext(somNs);

			String token = paths.get(p);
			try {
				String v = xPath.evaluate(token, xmlDoc);
				log.info("Resolved [{}] - {} to [{}]", p, paths.get(p), v);
				output.put(p, v);
			} catch (Exception ex) {
				log.error("Token {} unresolvable, skipping", token);
			}
		}

		return output;
	}
}
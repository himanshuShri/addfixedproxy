package jacada.addFixedProxy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RestServiceDAO {

	@Autowired private ObjectMapper objMapper;
	private RestTemplate restTemplate;

	public RestServiceDAO(RestTemplateBuilder restTemplateBuilder) {
		restTemplate = restTemplateBuilder.build();
	}

	// this is the only method that actually talks to amdocs
	// the choice between json & xml serialisation is encoded into the entity's header
	public String call(String url, HttpMethod method, HttpEntity<?> request) {

		log.debug("Calling ({}) {}", method.name(), url);
		String stringResp = null;

		try {
			if (MediaType.APPLICATION_JSON.equals( request.getHeaders().getContentType()))
				log.debug("=== Request JSON ==>\n\n\t {}\n", objMapper.writeValueAsString(request));
			else
				log.debug("=== Request XML ==>\n\n\t {}\n", request);

			stringResp = restTemplate.exchange(url, method, request, String.class).getBody();
		} catch (JsonProcessingException e) {
			log.error("Could not marshall request {}", request);
		} catch (RestClientResponseException e) {
			log.error("!!Caught HTTP REST service error from {}", url);
			stringResp = e.getResponseBodyAsString();
			log.error("!!Error!! {}", stringResp);
		}

		log.debug("<== Response ===\n\n\t {}\n", stringResp);
		return stringResp;
	}
}
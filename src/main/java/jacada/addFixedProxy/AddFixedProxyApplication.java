package jacada.addFixedProxy;

import java.util.EnumSet;
import java.util.Set;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.spi.json.JacksonJsonProvider;
import com.jayway.jsonpath.spi.json.JsonProvider;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;
import com.jayway.jsonpath.spi.mapper.MappingProvider;

import jacada.addFixedProxy.properties.ADTProperties;
import jacada.addFixedProxy.properties.AdminProperties;
import jacada.addFixedProxy.properties.InteractProperties;
import jacada.addFixedProxy.properties.Layer7Properties;
import jacada.addFixedProxy.properties.MongoProperties;
import jacada.addFixedProxy.properties.VanDyckProperties;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
@EnableScheduling
@EnableConfigurationProperties({
	AdminProperties.class,
	InteractProperties.class,
	MongoProperties.class,
	ADTProperties.class,
	Layer7Properties.class,
	VanDyckProperties.class
})

public class AddFixedProxyApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(AddFixedProxyApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(AddFixedProxyApplication.class, args);
	}

	public AddFixedProxyApplication() {

		log.info("configuring JSON Parser to use Jackson providers");
		// use jackson rather than Json-smart
		Configuration.setDefaults(new Configuration.Defaults() {

			/**
			 *  configure jayway to ignore a single trailing comma at the end of a set or array

			    private  final ObjectMapper myObjectMapper = new ObjectMapper();
			    private  final ObjectReader myObjectReader = myObjectMapper.reader().forType(Object.class).withFeatures(
			    		JsonParser.Feature.ALLOW_TRAILING_COMMA
			    );
				private final JsonProvider jsonProvider = new JacksonJsonProvider(myObjectMapper, myObjectReader);

			**/

			/**
			 * this is the default strict behaviour where trailing commas are errors
			**/
			private final JsonProvider jsonProvider 		= new JacksonJsonProvider();
			private final MappingProvider mappingProvider 	= new JacksonMappingProvider();

			@Override
			public JsonProvider jsonProvider() {
				return jsonProvider;
			}

			@Override
			public MappingProvider mappingProvider() {
				return mappingProvider;
			}

			@Override
			public Set<Option> options() {
				return EnumSet.of(Option.DEFAULT_PATH_LEAF_TO_NULL, Option.SUPPRESS_EXCEPTIONS);
			}
		});
	}

}
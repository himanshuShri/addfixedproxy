package jacada.addFixedProxy.mongo;

import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;

import jacada.addFixedProxy.properties.MongoProperties;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class MongoInstance {

	@Autowired private MongoProperties	properties;
	@Autowired private MongoTemplate 	mongo;
	@Autowired private ObjectMapper		objMapper;

	// the underlying collection names here are configured
	// TICKET_TYPE_1 etc are the tables
	// FILT_TICKET_TYPE_1 etc are the filtered views
	// ALL_TICKET_TYPE_1 etc are the unfiltered views
	// DISPLAY_MAPPING maps display values back to refIds
	public enum COLLECTIONS {
		EXCLUSIONS,
		TICKET_TYPE_1,
		TICKET_TYPE_2,
		TICKET_TYPE_3,
		FLEXIBLE_ATTRIBUTES,
		FILT_TICKET_TYPE_1,
		FILT_TICKET_TYPE_2,
		FILT_TICKET_TYPE_3,
		FILT_FLEXIBLE_ATTRIBUTES,
		ALL_TICKET_TYPE_1,
		ALL_TICKET_TYPE_2,
		ALL_TICKET_TYPE_3,
		ALL_FLEXIBLE_ATTRIBUTES,
		DISPLAY_MAPPING
	}

	public MongoCollection<Document> collection(COLLECTIONS collection) {

		var c_name = properties.getCollections().get(
				collection.toString()
		);
		log.trace("retuning {} collection", c_name);
		return mongo.getCollection(c_name);
	}

	public FindIterable<Document> findAll(COLLECTIONS collection) {

		log.debug("looking up all records");
		return find(collection, new Document());
	}

	public FindIterable<Document> find(COLLECTIONS collection, Document filter) {

		log.debug("looking up filtered records");
		return collection(collection).find(filter);
	}

	public void insertAll(COLLECTIONS collection, List<Document> docs) {

		log.debug("inserting [{}] records", docs.size());

		if(docs.size() > 0) {
			collection(collection).insertMany(docs);
		} else {
			log.warn("no documents to add");
		}
	}

	public void insertOne(COLLECTIONS collection, String json) throws Exception {

		log.debug("inserting a record");

		Map<String, Object> insert
		  = objMapper.readValue(json, new TypeReference<Map<String,Object>>(){});

		Document doc = new Document();
		for(String key: insert.keySet()) {
			doc.put(key, insert.get(key));
		}

		collection(collection).insertOne(doc);
	}

	public long updateOne(COLLECTIONS collection, String id, String json) throws Exception {

		log.debug("updating fields for {{}]", id);

		Map<String, Object> update
		  = objMapper.readValue(json, new TypeReference<Map<String,Object>>(){});

		Document doc = new Document();
		for(String key: update.keySet()) {
			doc.put(key, update.get(key));
		}

		UpdateResult res = collection(collection).
				replaceOne(Filters.eq("_id", new ObjectId(id)), doc);

		return res.getModifiedCount();
	}


	public long deleteOne(COLLECTIONS collection, String id) {

		log.warn("!!! deleting record [{}] !!!", id);
		DeleteResult res = collection(collection).
				deleteOne(Filters.eq("_id", new ObjectId(id)));

		return res.getDeletedCount();
	}

	public long deleteAll(COLLECTIONS collection) {

		log.warn("!!! deleting all records !!!");
		DeleteResult res = collection(collection).deleteMany(Filters.where("true"));

		return res.getDeletedCount();
	}
}
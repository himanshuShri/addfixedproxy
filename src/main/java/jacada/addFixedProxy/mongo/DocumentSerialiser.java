package jacada.addFixedProxy.mongo;

import java.io.IOException;

import org.bson.Document;
import org.bson.types.ObjectId;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class DocumentSerialiser extends JsonSerializer<Document> {

	@Override
	public void serialize(Document doc, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
			throws IOException, JsonProcessingException {

		jsonGenerator.writeStartObject();

		for (String key : doc.keySet()) {
			jsonGenerator.writeObjectField(key, simplifyObjectId(doc.get(key)));
		}

		jsonGenerator.writeEndObject();
	}

	private Object simplifyObjectId(Object val) {

		return val == null ? "" : val instanceof ObjectId ?
				((ObjectId) val).toHexString() : val;
	}
}
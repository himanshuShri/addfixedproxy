package jacada.addFixedProxy.mongo;

import java.io.IOException;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.boot.jackson.JsonComponent;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

@JsonComponent
public class MongoDocumentSerialiser extends JsonSerializer<Document> {

	@Override
	public void serialize(Document doc, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
		throws IOException, JsonProcessingException {

		jsonGenerator.writeStartObject();

		for (String key : doc.keySet()) {
			jsonGenerator.writeStringField(key, pretty(doc.get(key)));
		}

		jsonGenerator.writeEndObject();
	}

	// simplify the key serialisation
	private String pretty(Object val) {

		return val == null ? "" :
				val instanceof ObjectId ?
						((ObjectId) val).toHexString() :
							val.toString();
	}
}
package jacada.addFixedProxy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.bson.Document;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import jacada.addFixedProxy.dto.layer7.Layer7ResponseDTO;
import jacada.addFixedProxy.mongo.DocumentSerialiser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * General utilities class containing the useful bean types for autowiring.
 * POJO style static utility methods are in the {@link Utils} class
 * @author ndoan
 *
 **/
@Slf4j
@Configuration
public class UtilBeans {

	public static final DateFormat STD_DATE_FORMATTER 				= new SimpleDateFormat("YYYY-MM-dd");
	public static final DateFormat US_DATETIME_FORMATTER			= new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
	public static final DateFormat ISO_T_DATE_FORMATTER 			= new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss");
	public static final DateTimeFormatter LOCAL_DATE_FORMATTER 		= DateTimeFormatter.ofPattern("YYYY-MM-dd");
	public static final DateTimeFormatter LOCAL_DATETIME_FORMATTER 	= DateTimeFormatter.ofPattern("YYYY-MM-dd'T'HH:mm:ss");

	// Bean definitions
	@Bean
	public ObjectMapper JsonObjectMapper() {

		ObjectMapper mapper = new ObjectMapper();

		// this wasn't necessary in earlier springboot
		// where @JsonCompopnent did the registration
		SimpleModule module = new SimpleModule();
		module.addSerializer(Document.class, new DocumentSerialiser());
		mapper.registerModule(module);

		return mapper;
	}

	@Bean
	public DateHandler dateHander() throws Exception {
		return new DateHandler();
	}

	@Bean
	public Checkers checkers() {
		return new Checkers();
	}

	@Bean
	public ExpiringComplexMap fifteenMinuteMap() {
		return new ExpiringComplexMap(15*60);
	}

	// types underlying the beans
	public class ExpiringComplexMap {

		private Map<String, CacheItem> map = null;
		private final int secondsToLive;

		/** @param the number of seconds to survive in the map */
		public ExpiringComplexMap(int liveForSecs) {
			map = new HashMap<String, CacheItem>();
			secondsToLive = liveForSecs;
		}

		/** @return deep copy of the underlying map */
		public Map<String, CacheItem> map() {

			return map.
					entrySet().stream().
					collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().clone()));
		}

		public CacheItem add(String id) {

			var now = LocalDateTime.now();
			var addedAt = String.format("%02d:%02d", now.getHour(), now.getMinute());

			var item = new CacheItem(now.plusSeconds(secondsToLive), addedAt);
			return map.put(id, item);
		}

		public CacheItem remove(String id) {
			return map.remove(id);
		}

		public CacheItem contains(String id) {
			return map.get(id);
		}

		/** check every minute and evict any stale keys */
		@Scheduled(fixedRate = 60*1000)
		private void trim() {

			var now = LocalDateTime.now();
			log.trace("==> trimming expiring complex map at {}", now);

			map = map.entrySet().stream().
				filter(e -> now.isBefore(e.getValue().getExpiry())).
				collect(
					Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)
				);

			log.trace("map is now ({})\n{}<==", map.size(), map);
		}

		@Data
		@AllArgsConstructor
		@NoArgsConstructor
		public class CacheItem {
			private LocalDateTime expiry;
			private String addedAt;

			@Override
			public CacheItem clone() {

				CacheItem clone = new CacheItem();
				clone.expiry 	= expiry;
				clone.addedAt	= addedAt;

				return clone;
			}
		}
	}

	public class DateHandler {

		private DateFormat formatter = STD_DATE_FORMATTER;
		private DatatypeFactory dataTypeFactory = null;

		public DateHandler() throws DatatypeConfigurationException {
			dataTypeFactory = DatatypeFactory.newInstance();
		}

		public void setFormatter(DateFormat fmt) {
			formatter = fmt;
		}

		public String format(Long l) {

			Date d = new Date(l.longValue());
			return formatter.format(d);
		}

		public String format(String s) {

			Long l = (StringUtils.hasText(s)) ? Long.valueOf(s) : 0L;
			return format(l);
		}

		public String format(Object o) {

			String s = (o == null) ? "" : String.valueOf(o);
			return format(s);
		}

		public XMLGregorianCalendar gregorianNow() {

			GregorianCalendar c = new GregorianCalendar();
			c.setTime(new Date());
			return dataTypeFactory.newXMLGregorianCalendar(c);
		}

		public String today() {

			Date now = new Date();
			return formatter.format(now);
		}

		public String inNDays(int n) {

			LocalDateTime now = LocalDateTime.now();
			return now.plusDays(n).format(LOCAL_DATETIME_FORMATTER);
		}
	}

	public class Checkers {

		// can't add null to a map so, replace with a ""
		// this logic can be extended to provide type-appropriate defaults
		public Object blankForEmpty(Object value) {

			return (value == null) ? "" : value;
		}

		public String stringBlankForEmpty(Object value) {

			return (value == null) ? "": String.valueOf(value);
		}

		// replace with a 0L
		public Object zerForEmpty(Object value) {

			return (value == null) ? 0L: value;
		}

		// remove the NPE risk from for maps
		public <T> Map<String, T> nullSafe(Map<String, T> map) {
	    return map == null ? Collections.<String, T>emptyMap() : map;
		}

		// remove the NPE risk from for loops
		public <T> List<T> nullSafe(List<T> iterable) {
	    return iterable == null ? Collections.<T>emptyList() : iterable;
		}

		public List<Map<String, Object>> dumpError(Layer7ResponseDTO resp) {

			List<Map<String, Object>> errs = nullSafe(resp.getError());
			log.error(String.format("[%d] errors", resp.getError().size()));

			for (Map<String, Object> err : errs) {
				log.error("Error set follows");
				for (String k : err.keySet()) {
					log.error(String.format("Error [%s] - [%s]", k, err.get(k)));
				}
			}

			return errs;
		}
	}
}
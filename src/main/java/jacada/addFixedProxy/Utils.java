package jacada.addFixedProxy;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;

/**
 * General utilities class containing public static methods only.
 * @author ndoan
 **/

@Slf4j
@Configuration
public class Utils {

	/**
	 * Load a resource file
	 * @param resource relative to the resources folder
	 * @return a single, unbroken String read in from the resource
	 * @throws URISyntaxException
	 * @throws IOException
	 **/
	public static String RESOURCE_STRING(String resource) throws IOException, URISyntaxException {

		log.debug("Loading resource from {}", resource);

		URL url = Utils.class.getResource(resource);
	  	return new String(Files.readAllBytes(Paths.get(url.toURI())));
	}

	public static String formatTibcoDateTime(String tibcoDT) {

		return tibcoDT.substring(0, 10) +" " +tibcoDT.substring(11,16);
	}
}
package jacada.addFixedProxy.properties;

import java.util.Map;

import javax.validation.constraints.NotEmpty;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import lombok.Data;

@ConfigurationProperties(prefix = "proxy.mongo")
@Validated
@Data
public class MongoProperties {

	@NotEmpty(message="a mongo collection must be specified")
	private Map<String, String> collections;
}
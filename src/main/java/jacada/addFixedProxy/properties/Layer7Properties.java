package jacada.addFixedProxy.properties;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotEmpty;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import jacada.addFixedProxy.service.ServiceProperties;
import jacada.addFixedProxy.service.layer7.Layer7RequestTypes;
import lombok.Data;

@ConfigurationProperties(prefix = "proxy.service.layer7")
@Validated
@Data
public class Layer7Properties implements ServiceProperties {

	@NotEmpty private String layer7Password;
	@NotEmpty private String layer7User;
	@NotEmpty private String dealerCode;
	@NotEmpty private String channel;

	@NotEmpty private String keyStorePath;
	@NotEmpty private String keyStorePassword;

	@NotEmpty private String host;
	@NotEmpty private String port;
	@NotEmpty private String environment;
	@NotEmpty private String baseUrl;
	@NotEmpty private String bslStem;
	@NotEmpty private String tibcoStem;
	@NotEmpty private String somStem;
	@NotEmpty private String ISWStem;
	@NotEmpty private String soapStem;
	@NotEmpty private String addressPortal;

	@NotEmpty private String contactDetailsEP;
	@NotEmpty private String customerProfileEP;
	@NotEmpty private String customerFiltersEP;
	@NotEmpty private String billingArrangementDetailsEP;
	@NotEmpty private String billingEntitiesEP;
	@NotEmpty private String updateAccountDetailsEP;
	@NotEmpty private String updateBillingCustomerDetailsEP;
	@NotEmpty private String updateBillingArrangementDetailsEP;
	@NotEmpty private String submitSwapEP;
	@NotEmpty private String updateContactEP;
	@NotEmpty private String createTicketEP;
	@NotEmpty private String createInteractionEP;
	@NotEmpty private String appointmentEP;
	@NotEmpty private String createAppointmentEP;
	@NotEmpty private String updateAppointmentEP;
	@NotEmpty private String reserveTimeslotsEP;
	@NotEmpty private String assignedProductsEP;
	@NotEmpty private String assignedProductDetailsEP;
	@NotEmpty private String networkPathDetailsEP;
	@NotEmpty private String ticketDetailsEP;
	@NotEmpty private String cpeDetailsEP;
	@NotEmpty private String refreshEntitlementsEP;

	@NotEmpty private Map<String, String> commonOutputPaths;
	@NotEmpty private Map<String, String> contactDetailsOutputPaths;
	@NotEmpty private Map<String, String> customerProfileOutputPaths;
	@NotEmpty private Map<String, String> customerFiltersOutputPaths;
	@NotEmpty private Map<String, String> billingArrangementDetailsOutputPaths;
	@NotEmpty private Map<String, String> billingEntitiesOutputPaths;
	@NotEmpty private Map<String, String> updateAccountDetailsOutputPaths;
	@NotEmpty private Map<String, String> updateBillingCustomerDetailsOutputPaths;
	@NotEmpty private Map<String, String> updateBillingArrangementDetailsOutputPaths;
	@NotEmpty private Map<String, String> submitSwapOutputPaths;
	@NotEmpty private Map<String, String> updateContactOutputPaths;
	@NotEmpty private Map<String, String> createTicketOutputPaths;
	@NotEmpty private Map<String, String> createInteractionOutputPaths;
	@NotEmpty private Map<String, String> appointmentOutputPaths;
	@NotEmpty private Map<String, String> createAppointmentOutputPaths;
	@NotEmpty private Map<String, String> updateAppointmentOutputPaths;
	@NotEmpty private Map<String, String> reserveTimeslotsOutputPaths;
	@NotEmpty private Map<String, String> assignedProductsOutputPaths;
	@NotEmpty private Map<String, String> assignedProductDetailsOutputPaths;
	@NotEmpty private Map<String, String> networkPathDetailsOutputPaths;
	@NotEmpty private Map<String, String> ticketDetailsOutputPaths;
	@NotEmpty private Map<String, String> cpeDetailsOutputPaths;
	@NotEmpty private Map<String, String> refreshEntitlementsOutputPaths;

	@PostConstruct
	private void registerPaths() {
		Layer7RequestTypes.CONTACT_DETAILS.paths					(contactDetailsOutputPaths);
		Layer7RequestTypes.CUSTOMER_PROFILE.paths					(customerProfileOutputPaths);
		Layer7RequestTypes.CUSTOMER_FILTERS.paths					(customerFiltersOutputPaths);
		Layer7RequestTypes.BILLING_ARRANGEMENT_DETAILS.paths		(billingArrangementDetailsOutputPaths);
		Layer7RequestTypes.BILLING_ENTITIES.paths					(billingEntitiesOutputPaths);
		Layer7RequestTypes.UPDATE_ACCOUNT_DETAILS.paths				(updateAccountDetailsOutputPaths);
		Layer7RequestTypes.UPDATE_BILLING_ARRANGEMENT_DETAILS.paths	(updateBillingArrangementDetailsOutputPaths);
		Layer7RequestTypes.UPDATE_BILLING_CUSTOMER_DETAILS.paths	(updateBillingCustomerDetailsOutputPaths);
		Layer7RequestTypes.UPDATE_CONTACT.paths						(updateContactOutputPaths);
		Layer7RequestTypes.CREATE_TICKET.paths						(createTicketOutputPaths);
		Layer7RequestTypes.CREATE_INTERACTION.paths					(createInteractionOutputPaths);
		Layer7RequestTypes.APPOINTMENT.paths						(appointmentOutputPaths);
		Layer7RequestTypes.CREATE_APPOINTMENT.paths					(createAppointmentOutputPaths);
		Layer7RequestTypes.UPDATE_APPOINTMENT.paths					(updateAppointmentOutputPaths);
		Layer7RequestTypes.SUBMIT_SWAP.paths						(submitSwapOutputPaths);
		Layer7RequestTypes.RESERVE_TIMESLOTS.paths					(reserveTimeslotsOutputPaths);
		Layer7RequestTypes.ASSIGNED_PRODUCTS.paths					(assignedProductsOutputPaths);
		Layer7RequestTypes.ASSIGNED_PRODUCT_DETAILS.paths			(assignedProductDetailsOutputPaths);
		Layer7RequestTypes.NETWORK_PATH_DETAILS.paths				(networkPathDetailsOutputPaths);
		Layer7RequestTypes.TICKET_DETAILS.paths						(ticketDetailsOutputPaths);
		Layer7RequestTypes.CPE_DETAILS.paths						(cpeDetailsOutputPaths);
		Layer7RequestTypes.REFRESH_ENTITLEMENTS.paths				(refreshEntitlementsOutputPaths);
	}

	@Override
	// return the common patterns spliced with any request specific ones
	public Map<String, String> outputPaths(String requestType) {

		// specific ones override the general
		Map<String, String> map = new HashMap<>() ;
		map.putAll(commonOutputPaths);
		map.putAll(Layer7RequestTypes.paths(requestType));

		return map;
	}
}
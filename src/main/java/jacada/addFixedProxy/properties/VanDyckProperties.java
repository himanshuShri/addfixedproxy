package jacada.addFixedProxy.properties;

import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotEmpty;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import lombok.Data;

@ConfigurationProperties(prefix = "proxy.service.vandyck")
@Validated
@Data
public class VanDyckProperties {

	@NotEmpty	private List<Case> supportedCases;

	// returns the first match on the file name
	public Case findCase(String id) {

		Case cas = null;
		for(Case c : supportedCases) {
			if(id.equals(c.file)) {
				cas = c;
				break;
			}
		}
		return cas;
	}

	@Data
	public static class Case {
    private String id;
    private String title;
    private String file;
    private Map<String, String> properties;
	}
}
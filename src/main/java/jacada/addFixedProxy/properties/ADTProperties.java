package jacada.addFixedProxy.properties;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotEmpty;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import jacada.addFixedProxy.service.ServiceProperties;
import jacada.addFixedProxy.service.adt.ADTRequestTypes;
import lombok.Data;

@ConfigurationProperties(prefix = "proxy.service.adt")
@Validated
@Data
public class ADTProperties implements ServiceProperties {

	@NotEmpty private String stem;
	@NotEmpty private String closeTabEP;
	@NotEmpty private String showTabEP;
	@NotEmpty private String addImportantActionEP;
	@NotEmpty private String dataUpdatedEP;
	@NotEmpty private String isPrivilegedEP;
	@NotEmpty private String cancelAppointmentEP;
	@NotEmpty private String createAuditPointEP;
	@NotEmpty private Map<String, String> commonOutputPaths;
	@NotEmpty private Map<String, String> isPrivilegedOutputPaths;

	@PostConstruct
	private void registerPaths() {
		ADTRequestTypes.paths(ADTRequestTypes.IS_PRIVILEGED.name(), isPrivilegedOutputPaths);
	}

	@Override
	// return just the common patterns
	public Map<String, String> outputPaths(String requestType) {

		// specific ones override the general
		Map<String, String> map = new HashMap<>() ;
		map.putAll(commonOutputPaths);
		map.putAll(ADTRequestTypes.paths(requestType));

		return map;
	}
}
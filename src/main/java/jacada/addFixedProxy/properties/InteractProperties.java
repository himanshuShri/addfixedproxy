package jacada.addFixedProxy.properties;

import javax.validation.constraints.NotEmpty;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import lombok.Data;

@ConfigurationProperties(prefix = "proxy.service.interact")
@Validated
@Data
public class InteractProperties {

	@NotEmpty private String appKey;
	@NotEmpty private String accountId;
	@NotEmpty private String environment;
	@NotEmpty private String shortUrlPrefix;
	@NotEmpty private String showTabs;
	@NotEmpty private String showSearch;
	@NotEmpty private String createCaseInteraction;
	@NotEmpty private String createCaseUrl;
}
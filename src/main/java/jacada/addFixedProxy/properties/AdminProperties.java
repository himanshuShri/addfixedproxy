package jacada.addFixedProxy.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import lombok.Data;

@ConfigurationProperties(prefix = "proxy.service.admin")
@Validated
@Data
public class AdminProperties {

	private char incomingFileDelimeter;
}
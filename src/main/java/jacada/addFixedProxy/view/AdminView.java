package jacada.addFixedProxy.view;

import org.springframework.stereotype.Component;

import jacada.addFixedProxy.Utils;

@Component
public class AdminView {

	private final String PAGE_TPL = "/html/admin.html";

	public String buildPage() throws Exception {
      return Utils.RESOURCE_STRING(PAGE_TPL);
    }
}
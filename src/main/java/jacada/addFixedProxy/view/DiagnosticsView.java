package jacada.addFixedProxy.view;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.samskivert.mustache.Mustache;
import com.samskivert.mustache.Template;

import jacada.addFixedProxy.Utils;

@Component
public class DiagnosticsView {

	private final String DEFAULT_TPL 	= "/html/templates/diagnostics.html";
	private final String EMPTY_MSG		= "No data was returned";
	private Template dataTableTpl 		= null;


	public DiagnosticsView() throws Exception {
		dataTableTpl = Mustache.compiler().compile(loadDefaultTable());
	}

	public String hashTable(Map<String, Object> hash) {

		return hash.size() > 0?
				dataTableTpl.execute(hash.entrySet()):
				EMPTY_MSG;
	}

	public String buildInfoTable(List<String> bi) {

		Map<String, Object> hash = new HashMap<>();
		String[] kvList = null;

		for (String s: bi) {
			if(!s.startsWith("#")) {
				kvList = s.split("=");
				hash.put(kvList[0], kvList[1]);
			}
		}

		return hashTable(hash);
	}

	private String loadDefaultTable() throws Exception {
	  	return Utils.RESOURCE_STRING(DEFAULT_TPL);
	}
}
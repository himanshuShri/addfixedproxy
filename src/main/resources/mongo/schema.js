/** 
  Load all of this file into the database with:

  mongo <host>:<port>/addFixedProxy schema.js
**/

// these will create the collection or update it if already there 
// also note that the index will be used if only the first component matches (as with RDBMS)

print("\nStarting...\n");
print("* Creating indexes");
print("**exclusions\n");
db.exclusions.createIndex( {ticketType1:1, ticketType2:1, ticketType3:1});
print("**ticketType1\n");
db.ticketType1.createIndex({ticketType1:1, ticketType2:1, ticketType3:1});
print("**ticketType2\n");
db.ticketType2.createIndex({ticketType1:1, ticketType2:1, ticketType3:1});
print("**ticketType3\n");
db.ticketType3.createIndex({ticketType1:1, ticketType2:1, ticketType3:1});
print("**flexibleAttributes\n");
db.flexibleAttributes.createIndex({Ticket_type1:1, Ticket_type2:1, Ticket_type3:1});
print("* Created indexes\n");

/**
Things to bear in mind here:
  Mongo is a document rather than row based store so it can only operate on sets which means we consequently
  have to do things a bit differently to the way an RDBMS would. The syntax is expressive anyway but it's
  laid out here with a lot of white space and comments so we can see what we're doing.
  Although this might look bloated & unwieldy to us, we don't have to process it so keep it well formatted.
  If you're going to edit this you'll need a good json editor (not eclipse) which supports folding and comments.
  Don't allow 2 or more consecutive blank lines as that is the mongo shell escape sequence.

In terms of the approach:
  Each query represents at least a 2 table join, but only 2 tables can be joined at a time.
  Moreover, you have to use the equivalent of "join on..." (lookup) followed by "where..." (redact) clauses 
  as these cannot be combined.
  Start with the smallest table and keep the working set as small as possible, avoiding repeated operations.

  At each level the query plans follow this approach:
    build the children for the next step:
      lookup - join to the exclusions table to capture potential disqualifying rules
      redact - remove any exclusions that don't meet all criteria
    in the parent document:
      redact - eliminate the documents with exclusions remaining
      unwind - flatten the children out 
    finally:
      project - present the data in the final visible form
**/

print("\n* Creating views");
/** LEVEL 0 **/
print("** Dropping filteredFlexibleAttributes view");
db.filteredFlexibleAttributes.drop()
db.createView("filteredFlexibleAttributes", "flexibleAttributes", [

  // relabel & sort the output
  {
    $project: {
  	  "_id": 0,
  	  ticketType1: "$Ticket_type_1",
  	  ticketType2: "$Ticket_type_2",
  	  ticketType3: "$Ticket_type_3",
  	  attributeId: "$Attribute_ID",
  	  type:  "$Type",
  	  description:  "$Description",
  	  "default":  "$Default Value",
  	  mandatory:  "$Mandatory",
  	  validValues:  "$Valid Values",
  	  display:  "$Display Label",
  	  regex:  "$Regex",
  	  regexMessage:  "$Regex message",
  	  subtypeFlag:  "$Subtype flag"
    }
  },

  {
    $sort:{
      ticketType1: 1,
      ticketType2: 1,
      ticketType3: 1,
      display: 1
    }
  }

]);
print("** Created filteredFlexibleAttributes view\n");

/** LEVEL 1 **/
print("** Dropping filteredTicketType1 view");
db.filteredTicketType1.drop()
db.createView("filteredTicketType1", "ticketType1", [

  // join to the exclusions
  {
    $lookup: {
      from: "exclusions",
      let: { tt1: "$ref_id" },
      pipeline: [

        // join clause - no filtering allowed here
        // find any related exclusions
        {
          $match: {
            $expr: {
              $or: [{
                $eq: [ "$ticketType1", "$$tt1" ]
              },{
                $eq: [ "$ticketType1", "*" ]
              }]
            }
          }
        },

        // redact non-qualifying exclusions
        {
          $redact: {
            $cond: {
              if: {
                $or: [{
                  $ne: [ "$ticketType2", "*" ]
                },{
                  $ne: [ "$ticketType3", "*" ]
                }]
              }, 
              then: "$$PRUNE", 
              else: "$$DESCEND"
            }
          }
        },
      ],
      as: "excl"
    }
  },

  // now remove any rows with remaining exclusions
  {
    $redact: {
      $cond: {
        if: {
          $gt: [ { $size: "$excl" }, 0 ]
        }, 
        then: "$$PRUNE", 
        else: "$$DESCEND"
      }
    }
  },

  // relabel & sort the output
  {
    $project: {
	  "_id": 0,
	  ticketType1: "$ref_id",
	  display: "$Display label"
    }
  },

  {
    $sort:{
      display: 1
    }
  }

]);
print("** Created filteredTicketType1 view\n");

print("** Dropping allTicketType1 view");
db.allTicketType1.drop()
db.createView("allTicketType1", "ticketType1", [
  {
	$project: {
	  "_id": 0,
	  ticketType1: "$ref_id",
	  ticketType2: "",
	  ticketType3: "",
	  display: "$Display label"
	}
  },

  {
    $sort:{
      display: 1
    }
  }

]);
print("** Created allTicketType1 view\n");


/** LEVEL 2 **/
print("** Dropping filteredTicketType2 view");
db.filteredTicketType2.drop()
db.createView("filteredTicketType2", "ticketType1", [

  // join to the exclusions
  {
    $lookup: {
      from: "exclusions",
      let: { tt1: "$ref_id" },
      pipeline: [

        // join clause - no filtering allowed here
        // find any related exclusions
        {
          $match: {
            $expr: {
              $or: [{
                $eq: [ "$ticketType1", "$$tt1" ]
              },{
                $eq: [ "$ticketType1", "*" ]
              }]
            }
          }
        },

        // redact non-qualifying exclusions
        {
          $redact: {
            $cond: {
              if: {
                $or: [{
                  $ne: [ "$ticketType2", "*" ]
                },{
                  $ne: [ "$ticketType3", "*" ]
                }]
              }, 
              then: "$$PRUNE", 
              else: "$$DESCEND"
            }
          }
        },
      ],
      as: "excl"
    }
  },

  // now remove any rows with remaining exclusions
  {
    $redact: {
      $cond: {
        if: {
          $gt: [ { $size: "$excl" }, 0 ]
        }, 
        then: "$$PRUNE", 
        else: "$$DESCEND"
      }
    }
  },

  // join to level 2
  {
    $lookup: {
      from: "ticketType2",
      let: { tt1: "$ref_id" },
      pipeline: [

        // join clause - no filtering allowed here
        {
          $match: {
            $expr: {
              $eq: [ "$parent_refid", "$$tt1" ]
            }
          }
        },

        // now join to the exclusions and eliminate
        // any explicit exclusions at level 2
        {
          $lookup: {
            from: "exclusions",
            let: { tt2: "$ref_id" },
            pipeline: [

              // join clause - no filtering allowed here
              // find any related exclusions
             {
                $match: {
                  $expr: {
                    $or: [{
                      $eq: [ "$ticketType2", "$$tt2" ]
                    },{
                      $eq: [ "$ticketType2", "*" ]
                    }]
                  }
                }
              },

              // redact non-qualifying exclusions
              {
                $redact: {
                  $cond: {
                    if: {
                      $or: [{
                        $and: [{
                          $ne: [ "$ticketType1", "*" ]
                        },{
                          $ne: [ "$ticketType1", "$$tt1" ]
                        }]
                      }, {
                        $ne: [ "$ticketType3", "*" ]
                      }]
                    }, 
                    then: "$$PRUNE", 
                    else: "$$DESCEND"
                  }
                }
              }
            ],
            as: "excl"
          }
        },

        // now remove any rows with remaining exclusions
        {
          $redact: {
            $cond: {
              if: {
                $gt: [ { $size: "$excl" }, 0 ]
              }, 
              then: "$$PRUNE", 
              else: "$$DESCEND"
            }
          }
        },
      ],
      as: "tt2"
    },
  },

  // collapse level 2 into level 1
  {
    $unwind: "$tt2"
  },

  // relabel & sort the output
  {
    $project: {
      "_id": 0,
      ticketType1: "$ref_id",
      ticketType2: "$tt2.ref_id",
      display: "$tt2.Display label"
    }
  },

  {
    $sort:{
      ticketType1: 1,
      display: 1
    }
  }

]);
print("** Created filteredTicketType2 view\n");

print("** Dropping allTicketType2 view");
db.allTicketType2.drop()
db.createView("allTicketType2", "ticketType2", [
  {
	$project: {
	  "_id": 0,
	  ticketType1: "$parent_refid",
	  ticketType2: "$ref_id",
	  ticketType3: "",
	  display: "$Display label",
	}
  },

  {
    $sort:{
      display: 1
    }
  }

]);
print("** Created allTicketType2 view\n");

/** LEVEL 3 **/
print("** Dropping filteredTicketType3 view");
db.filteredTicketType3.drop()
db.createView("filteredTicketType3", "ticketType1", [

  // join to the exclusions
  {
    $lookup: {
      from: "exclusions",
      let: { tt1: "$ref_id" },
      pipeline: [

        // join clause - no filtering allowed here
        // find any related exclusions
        {
          $match: {
            $expr: {
              $or: [{
                $eq: [ "$ticketType1", "$$tt1" ]
              },{
                $eq: [ "$ticketType1", "*" ]
              }]
            }
          }
        },

        // redact non-qualifying exclusions
        {
          $redact: {
            $cond: {
              if: {
                $or: [{
                  $ne: [ "$ticketType2", "*" ]
                },{
                  $ne: [ "$ticketType3", "*" ]
                }]
              }, 
              then: "$$PRUNE", 
              else: "$$DESCEND"
            }
          }
        },
      ],
      as: "excl"
    }
  },

  // now remove any rows with remaining exclusions
  {
    $redact: {
      $cond: {
        if: {
          $gt: [ { $size: "$excl" }, 0 ]
        }, 
        then: "$$PRUNE", 
        else: "$$DESCEND"
      }
    }
  },

  // join to level 2
  {
    $lookup: {
      from: "ticketType2",
      let: { tt1: "$ref_id" },
      pipeline: [

        // join clause - no filtering allowed here
        {
          $match: {
            $expr: {
              $eq: [ "$parent_refid", "$$tt1" ]
            }
          }
        },

        // now join to the exclusions and eliminate
        // any explicit exclusions at level 2
        {
          $lookup: {
            from: "exclusions",
            let: { tt2: "$ref_id" },
            pipeline: [

              // join clause - no filtering allowed here
              // find any related exclusions
              {
                $match: {
                  $expr: {
                    $or: [{
                      $eq: [ "$ticketType2", "$$tt2" ]
                    },{
                      $eq: [ "$ticketType2", "*" ]
                    }]
                  }
                }
              },

              // redact non-qualifying exclusions
              {
                $redact: {
                  $cond: {
                    if: {
                      $or: [{
                        $and: [{
                          $ne: [ "$ticketType1", "*" ]
                        },{
                          $ne: [ "$ticketType1", "$$tt1" ]
                        }]
                      }, {
                        $ne: [ "$ticketType3", "*" ]
                      }]
                    }, 
                    then: "$$PRUNE", 
                    else: "$$DESCEND"
                  }
                }
              }
            ],
            as: "excl"
          }
        },

        // now remove any rows with remaining exclusions
        {
          $redact: {
            $cond: {
              if: {
                $gt: [ { $size: "$excl" }, 0 ]
              }, 
              then: "$$PRUNE", 
              else: "$$DESCEND"
            }
          }
        },

        // join to  level 3
        {
          $lookup: {
            from: "ticketType3",
            let: { tt2: "$ref_id" },
            pipeline: [

              // join clause - no filtering allowed here
              {
                $match: {
                  $expr: {
                    $eq: [ "$parent_refid", "$$tt2" ]
                  }
                }
              },

              // now join to the exclusions and eliminate
              // any explicit exclusions at level 3
              {
                $lookup: {
                  from: "exclusions",
                  let: { tt3: "$ref_id" },
                  pipeline: [

                    // join clause - no filtering allowed here
                    // find any related exclusions
                    {
                        $match: {
                          $expr: {
                            $or: [{
                              $eq: [ "$ticketType3", "$$tt3" ]
                            },{
                              $eq: [ "$ticketType3", "*" ]
                            }]
                          }
                        }
                      },

                    // redact non-qualifying exclusions
                    {
                      $redact: {
                        $cond: {
                          if: {
                            $or: [{
                              $and: [{
                                $ne: [ "$ticketType1", "*" ]
                              },{
                                $ne: [ "$ticketType1", "$$tt1" ]
                              }]
                            },{
                              $and: [{
                                $ne: [ "$ticketType2", "*" ]
                              },{
                                $ne: [ "$ticketType2", "$$tt2" ]
                              }]
                            }]
                          }, 
                          then: "$$PRUNE", 
                          else: "$$DESCEND"
                        }
                      }
                    }
                 ],
                  as: "excl"
                }
              },

              // now remove any rows with remaining exclusions
              {
                $redact: {
                  $cond: {
                    if: {
                      $gt: [ { $size: "$excl" }, 0 ]
                    }, 
                    then: "$$PRUNE", 
                    else: "$$DESCEND"
                  }
                }
              }
            ],
            as: "tt3"
          }
        },

        // collapse level 3 into level 2
        {
          $unwind: "$tt3"
        }
      ],
      as: "tt2"
    },
  },

  // collapse level 2 into level 1
  {
    $unwind: "$tt2"
  },

  // relabel and sort the data
  {
    $project: {
      "_id": 0,
      ticketType1: "$ref_id",
      ticketType2: "$tt2.ref_id",
      ticketType3: "$tt2.tt3.ref_id",
      display: "$tt2.tt3.Display label"
    }
  },

  {
    $sort:{
      ticketType1: 1,
      ticketType2: 1,
      display: 1
    }
  }

]);
print("** Created filteredTicketType3 view\n");

print("** Dropping allTicketType3 view");
db.allTicketType3.drop()
db.createView("allTicketType3", "ticketType3", [
  {
	$project: {
	  "_id": 0,
	  ticketType1: "",
	  ticketType2: "$parent_refid",
	  ticketType3: "$ref_id",
	  display: "$Display label"
	}
  },

  {
    $sort:{
      display: 1
    }
  }

]);
print("** Created allTicketType3 view\n");

print("** Dropping displayMapping view");
db.displayMapping.drop()
db.createView("displayMapping","ticketType1", [

  {
    $lookup: {
      from: "ticketType2",
      let: { tt1: "$ref_id" },
      pipeline: [

        {
          $match: {
            $expr: {
              $eq: [ "$parent_refid", "$$tt1" ]
            }
          }
        }

      ],
      as: "tt2"
    }
  },

  {
    $unwind: "$tt2"
  },

  {
    $lookup: {
      from: "ticketType3",
      let: { tt2: "$tt2.ref_id" },
      pipeline: [

        {
          $match: {
            $expr: {
              $eq: [ "$parent_refid", "$$tt2" ]
            }
          }
        }

      ],
      as: "tt3"
    }
  },

  {
    $unwind: "$tt3"
  },

  {
    $project: {
      "_id": 0,
      ticketType1: "$ref_id",     display_ticketType1: "$Display label",
      ticketType2: "$tt2.ref_id", display_ticketType2: "$tt2.Display label",
      ticketType3: "$tt3.ref_id", display_ticketType3: "$tt3.Display label"
    }
  },

])
print("** Created displayMapping view\n");

print("* Created views\n");

// check everything is present & correct
// first check the base collections
print("\n* Checking components");
var cols = db.getCollectionNames();
var chkViews = true;
["ticketType1", "ticketType2", "ticketType3", "flexibleAttributes"].forEach(function(col) {

  print("** Checking collection: " +col);
  if(cols.includes(col)) {
    var ct = db[col].count();
    print("\t collection exists and contains " +ct +" documents\n");
  } else {
    chkViews = false;
    print("\t !!! Error - collection " +col +" has not been created !!!\n");
  }
});

if(!chkViews) {
  print("\n!!! Error - there are problems with base collections. Please fix those first and then re-run this script.");
  print("\t Here is the current list of collections:\n\t\t" +cols +"\nExiting...\n");
  quit();
}

// now check the views
[
  "filteredTicketType1", "allTicketType1", 
  "filteredTicketType2", "allTicketType2", 
  "filteredTicketType3", "allTicketType3", 
  "filteredFlexibleAttributes", "displayMapping"
].forEach(function(col) {

	print("\n* Checking view: " +col);
	if(cols.includes(col)) {
		var ct = db[col].count();
		print("\t View exists and contains " +ct +" documents");
		if(ct == 0) {
	      print("\t ! Warn - document count of 0 indicates that either no data is loaded or an error has occured.");
	      print("\t ! You can ignore this if this no data is present in the base collection.");
	    }
	} else {
		print("\n\t !!! Error - view " +col +" has not been created !!!");
		print("\n\t !!! Error - these are the views: " +cols +" !!!");
	}		
});

print("\nDone");